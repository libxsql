#!/bin/bash

PACKAGE=xsql
VER=`cat debian/$PACKAGE/DEBIAN/control | grep Version | awk '{print $2}'`
LIBDIR=debian/$PACKAGE/usr/lib
DOCDIR=debian/$PACKAGE/usr/share/doc/$PACKAGE
mkdir -p $LIBDIR
mkdir -p $DOCDIR
cp libxsql.so.$VER $LIBDIR
ln -s libxsql.so.$VER $LIBDIR/libxsql.so
cp copyright $DOCDIR
cp changelog.Debian.gz $DOCDIR
cd debian/$PACKAGE
md5deep -r -l usr > DEBIAN/md5sums
cd ..
fakeroot dpkg-deb --build $PACKAGE
mv $PACKAGE.deb ../"$PACKAGE"_`cat $PACKAGE/DEBIAN/control | grep Version | awk '{print $2}'`_`cat $PACKAGE/DEBIAN/control | grep Architecture | awk '{print $2}'`.deb
rm -fr $PACKAGE/usr/*
rmdir $PACKAGE/usr
rm $PACKAGE/DEBIAN/md5sums
cd ..

PACKAGE=xsql-dev
VER=`cat debian/$PACKAGE/DEBIAN/control | grep Version | awk '{print $2}'`
LIBDIR=debian/$PACKAGE/usr/lib
INCDIR=debian/$PACKAGE/usr/include/libxsql
DOCDIR=debian/$PACKAGE/usr/share/doc/$PACKAGE
mkdir -p $LIBDIR/pkgconfig
mkdir -p $DOCDIR
mkdir -p $INCDIR
cp libxsql.pc $LIBDIR/pkgconfig
cp include/libxsql/* $INCDIR
cp copyright $DOCDIR
cp changelog.Debian.gz $DOCDIR
cd debian/$PACKAGE
md5deep -r -l usr > DEBIAN/md5sums
cd ..
fakeroot dpkg-deb --build $PACKAGE
mv $PACKAGE.deb ../"$PACKAGE"_`cat $PACKAGE/DEBIAN/control | grep Version | awk '{print $2}'`_`cat $PACKAGE/DEBIAN/control | grep Architecture | awk '{print $2}'`.deb
rm -fr $PACKAGE/usr/*
rmdir $PACKAGE/usr
rm $PACKAGE/DEBIAN/md5sums
cd ..

PACKAGE=xsql-firebird
VER=`cat debian/$PACKAGE/DEBIAN/control | grep Version | awk '{print $2}'`
LIBDIR=debian/$PACKAGE/usr/lib
DOCDIR=debian/$PACKAGE/usr/share/doc/$PACKAGE
mkdir -p $LIBDIR
mkdir -p $DOCDIR
cp libxsql_gds.so.$VER $LIBDIR
ln -s libxsql_gds.so.$VER $LIBDIR/libxsql_gds.so
cp copyright $DOCDIR
cp changelog.Debian.gz $DOCDIR
cd debian/$PACKAGE
md5deep -r -l usr > DEBIAN/md5sums
cd ..
fakeroot dpkg-deb --build $PACKAGE
mv $PACKAGE.deb ../"$PACKAGE"_`cat $PACKAGE/DEBIAN/control | grep Version | awk '{print $2}'`_`cat $PACKAGE/DEBIAN/control | grep Architecture | awk '{print $2}'`.deb
rm -fr $PACKAGE/usr/*
rmdir $PACKAGE/usr
rm $PACKAGE/DEBIAN/md5sums
cd ..

PACKAGE=xsql-mdb
VER=`cat debian/$PACKAGE/DEBIAN/control | grep Version | awk '{print $2}'`
LIBDIR=debian/$PACKAGE/usr/lib
DOCDIR=debian/$PACKAGE/usr/share/doc/$PACKAGE
mkdir -p $LIBDIR
mkdir -p $DOCDIR
cp libxsql_mdb.so.$VER $LIBDIR
ln -s libxsql_mdb.so.$VER $LIBDIR/libxsql_mdb.so
cp copyright $DOCDIR
cp changelog.Debian.gz $DOCDIR
cd debian/$PACKAGE
md5deep -r -l usr > DEBIAN/md5sums
cd ..
fakeroot dpkg-deb --build $PACKAGE
mv $PACKAGE.deb ../"$PACKAGE"_`cat $PACKAGE/DEBIAN/control | grep Version | awk '{print $2}'`_`cat $PACKAGE/DEBIAN/control | grep Architecture | awk '{print $2}'`.deb
rm -fr $PACKAGE/usr/*
rmdir $PACKAGE/usr
rm $PACKAGE/DEBIAN/md5sums
cd ..

PACKAGE=xsql-mysql
VER=`cat debian/$PACKAGE/DEBIAN/control | grep Version | awk '{print $2}'`
LIBDIR=debian/$PACKAGE/usr/lib
DOCDIR=debian/$PACKAGE/usr/share/doc/$PACKAGE
mkdir -p $LIBDIR
mkdir -p $DOCDIR
cp libxsql_mysql.so.$VER $LIBDIR
ln -s libxsql_mysql.so.$VER $LIBDIR/libxsql_mysql.so
cp copyright $DOCDIR
cp changelog.Debian.gz $DOCDIR
cd debian/$PACKAGE
md5deep -r -l usr > DEBIAN/md5sums
cd ..
fakeroot dpkg-deb --build $PACKAGE
mv $PACKAGE.deb ../"$PACKAGE"_`cat $PACKAGE/DEBIAN/control | grep Version | awk '{print $2}'`_`cat $PACKAGE/DEBIAN/control | grep Architecture | awk '{print $2}'`.deb
rm -fr $PACKAGE/usr/*
rmdir $PACKAGE/usr
rm $PACKAGE/DEBIAN/md5sums
cd ..

PACKAGE=xsql-postgres
VER=`cat debian/$PACKAGE/DEBIAN/control | grep Version | awk '{print $2}'`
LIBDIR=debian/$PACKAGE/usr/lib
DOCDIR=debian/$PACKAGE/usr/share/doc/$PACKAGE
mkdir -p $LIBDIR
mkdir -p $DOCDIR
cp libxsql_pg.so.$VER $LIBDIR
ln -s libxsql_pg.so.$VER $LIBDIR/libxsql_pg.so
cp copyright $DOCDIR
cp changelog.Debian.gz $DOCDIR
cd debian/$PACKAGE
md5deep -r -l usr > DEBIAN/md5sums
cd ..
fakeroot dpkg-deb --build $PACKAGE
mv $PACKAGE.deb ../"$PACKAGE"_`cat $PACKAGE/DEBIAN/control | grep Version | awk '{print $2}'`_`cat $PACKAGE/DEBIAN/control | grep Architecture | awk '{print $2}'`.deb
rm -fr $PACKAGE/usr/*
rmdir $PACKAGE/usr
rm $PACKAGE/DEBIAN/md5sums
cd ..

PACKAGE=xsql-sqlite
VER=`cat debian/$PACKAGE/DEBIAN/control | grep Version | awk '{print $2}'`
LIBDIR=debian/$PACKAGE/usr/lib
DOCDIR=debian/$PACKAGE/usr/share/doc/$PACKAGE
mkdir -p $LIBDIR
mkdir -p $DOCDIR
cp libxsql_sqlite.so.$VER $LIBDIR
ln -s libxsql_sqlite.so.$VER $LIBDIR/libxsql_sqlite.so
cp copyright $DOCDIR
cp changelog.Debian.gz $DOCDIR
cd debian/$PACKAGE
md5deep -r -l usr > DEBIAN/md5sums
cd ..
fakeroot dpkg-deb --build $PACKAGE
mv $PACKAGE.deb ../"$PACKAGE"_`cat $PACKAGE/DEBIAN/control | grep Version | awk '{print $2}'`_`cat $PACKAGE/DEBIAN/control | grep Architecture | awk '{print $2}'`.deb
rm -fr $PACKAGE/usr/*
rmdir $PACKAGE/usr
rm $PACKAGE/DEBIAN/md5sums
cd ..

PACKAGE=xsql-xlsx
VER=`cat debian/$PACKAGE/DEBIAN/control | grep Version | awk '{print $2}'`
LIBDIR=debian/$PACKAGE/usr/lib
DOCDIR=debian/$PACKAGE/usr/share/doc/$PACKAGE
mkdir -p $LIBDIR
mkdir -p $DOCDIR
cp libxsql_xlsx.so.$VER $LIBDIR
ln -s libxsql_xlsx.so.$VER $LIBDIR/libxsql_xlsx.so
cp copyright $DOCDIR
cp changelog.Debian.gz $DOCDIR
cd debian/$PACKAGE
md5deep -r -l usr > DEBIAN/md5sums
cd ..
fakeroot dpkg-deb --build $PACKAGE
mv $PACKAGE.deb ../"$PACKAGE"_`cat $PACKAGE/DEBIAN/control | grep Version | awk '{print $2}'`_`cat $PACKAGE/DEBIAN/control | grep Architecture | awk '{print $2}'`.deb
rm -fr $PACKAGE/usr/*
rmdir $PACKAGE/usr
rm $PACKAGE/DEBIAN/md5sums
cd ..

PACKAGE=xsql-util
VER=`cat debian/$PACKAGE/DEBIAN/control | grep Version | awk '{print $2}'`
BINDIR=debian/$PACKAGE/usr/bin
DOCDIR=debian/$PACKAGE/usr/share/doc/$PACKAGE
mkdir -p $BINDIR
mkdir -p $DOCDIR
cp xsql_config $BINDIR
cp ixsql $BINDIR
cp copyright $DOCDIR
cp changelog.Debian.gz $DOCDIR
cd debian/$PACKAGE
md5deep -r -l usr > DEBIAN/md5sums
cd ..
fakeroot dpkg-deb --build $PACKAGE
mv $PACKAGE.deb ../"$PACKAGE"_`cat $PACKAGE/DEBIAN/control | grep Version | awk '{print $2}'`_`cat $PACKAGE/DEBIAN/control | grep Architecture | awk '{print $2}'`.deb
rm -fr $PACKAGE/usr/*
rmdir $PACKAGE/usr
rm $PACKAGE/DEBIAN/md5sums
cd ..
