#include <string.h>
#include <sqlite3.h>
#include <libex/str.h>
#include "var.h"
#include "db.h"

#define NUMERIC_BUFSIZE 32
typedef struct {
    char *result;
} sqlite_t;

const char * get_provider_name () {
    return "sqlite";
}

char *get_placeholder (int n) {
    return strdup("?");
}

xsql_conn_t *dbopen (xsql_dbprov_t *provider, const char *name, size_t name_len, const char *conninfo, size_t conninfo_len) {
    sqlite3 *conn;
    xsql_conn_t *ret = calloc(1, sizeof(xsql_conn_t));
    ret->name = mkstr(name, name_len, 8);
    if (SQLITE_OK == sqlite3_open(conninfo, &conn)) {
        ret->provider = provider;
        ret->hdb = (xsql_dbconn_t*)conn;
    } else
        ret->msg = strdup(sqlite3_errmsg(conn));
    return ret;
}

xsql_stmt_t *sqlprepare (xsql_conn_t *conn, xsql_stmt_t *curstmt, const char *cmd, size_t cmd_len, size_t param_count) {
    xsql_stmt_t *ret;
    if (curstmt) {
        ret = curstmt;
        sqlite3_reset((sqlite3_stmt*)ret->hstmt);
        ret->nrecs = 0;
    } else {
        ret = calloc(1, sizeof(xsql_stmt_t));
        ret->data = calloc(1, sizeof(sqlite_t));
        sqlite3_stmt *stmt;
        const char *tail;
        if (SQLITE_OK != sqlite3_prepare((sqlite3*)conn->hdb, cmd, cmd_len, &stmt, &tail))
            ret->msg = strdup(sqlite3_errmsg((sqlite3*)conn->hdb));
        ret->hstmt = (xsql_dbstmt_t*)stmt;
        ret->conn = conn;
    }
    return ret;
}

static void create_field_attrs (xsql_stmt_t *stmt) {
    stmt->fldnams = malloc(stmt->nflds * sizeof(str_t));
    for (int i = 0; i < stmt->nflds; ++i) {
        const char *fldnam = sqlite3_column_name((sqlite3_stmt*)stmt->hstmt, i);
        if (fldnam)
            stmt->fldnams[i] = mkstr(fldnam, strlen(fldnam), 8);
        else
            stmt->fldnams[i] = mkstr(CONST_STR_LEN("?column?"), 8);
        switch (sqlite3_column_type((sqlite3_stmt*)stmt->hstmt, i)) {
            case SQLITE_INTEGER: stmt->types[i] = TYP_INT; break;
            case SQLITE_FLOAT: stmt->types[i] = TYP_FLOAT; break;
            default: stmt->types[i] = TYP_STRING; break;
        }
    }
}

static void fill_params (xsql_stmt_t *stmt, list_t *params) {
    int param_no = 1;
    list_item_t *li = params->head;
    do {
        xsql_var_t *var = (xsql_var_t*)li->ptr;
        switch (var->typ) {
            case TYP_NONE: sqlite3_bind_null((sqlite3_stmt*)stmt->hstmt, param_no); break;
            case TYP_INT: sqlite3_bind_int64((sqlite3_stmt*)stmt->hstmt, param_no, var->raw.i_val); break;
            case TYP_FLOAT: sqlite3_bind_double((sqlite3_stmt*)stmt->hstmt, param_no, var->raw.f_val); break;
            case TYP_STRING: sqlite3_bind_text((sqlite3_stmt*)stmt->hstmt, param_no, var->raw.s_val->ptr, var->raw.s_val->len, SQLITE_TRANSIENT); break;
            default: break;
        }
        li = li->next;
        ++param_no;
    } while (li != params->head);
}

static void get_row_data (xsql_stmt_t *stmt) {
    sqlite3_stmt *hstmt = (sqlite3_stmt*)stmt->hstmt;
    sqlite_t *data = (sqlite_t*)stmt->data;
    for (int i = 0; i < stmt->nflds; ++i) {
        char *ptr;
        switch (sqlite3_column_type(hstmt, i)) {
            case SQLITE_INTEGER:
                ptr = data->result + i * NUMERIC_BUFSIZE;
                snprintf(ptr, NUMERIC_BUFSIZE, LONG_FMT, sqlite3_column_int64(hstmt, i));
                stmt->result[i] = ptr;
                break;
            case SQLITE_FLOAT:
                ptr = data->result + i * NUMERIC_BUFSIZE;
                snprintf(ptr, NUMERIC_BUFSIZE, "%f", sqlite3_column_double(hstmt, i));
                stmt->result[i] = ptr;
                break;
            case SQLITE_BLOB:
                stmt->result[i] = (char*)sqlite3_column_blob(hstmt, i);
                break;
            case SQLITE_NULL:
                stmt->result[i] = NULL;
                break;
            case SQLITE_TEXT:
                stmt->result[i] = (char*)sqlite3_column_text(hstmt, i);
                break;
        }
    }
    stmt->nrecs++;
}

int fetch (xsql_stmt_t *stmt) {
    sqlite3_stmt *hstmt = (sqlite3_stmt*)stmt->hstmt;
    switch (sqlite3_step(hstmt)) {
        case SQLITE_ROW:
            get_row_data(stmt);
            stmt->ret = XSQL_OK;
            return XSQL_OK;
        case SQLITE_DONE:
            sqlite3_reset((sqlite3_stmt*)stmt->hstmt);
            stmt->ret = XSQL_FIN;
            return XSQL_FIN;
        default:
            sqlite3_reset((sqlite3_stmt*)stmt->hstmt);
            stmt->msg = strdup(sqlite3_errmsg((sqlite3*)stmt->conn->hdb));
            stmt->ret = XSQL_ERROR;
            return XSQL_ERROR;
    }
}

int sqlexec (xsql_stmt_t *stmt, list_t *params) {
    if (params->len > 0)
        fill_params(stmt, params);
    switch (sqlite3_step((sqlite3_stmt*)stmt->hstmt)) {
        case SQLITE_ROW:
            if (!stmt->fldnams) {
                stmt->nflds = sqlite3_column_count((sqlite3_stmt*)stmt->hstmt);
                stmt->result = malloc(stmt->nflds * sizeof(char*));
                stmt->types = malloc(stmt->nflds * sizeof(xsql_type_t));
                ((sqlite_t*)stmt->data)->result = malloc(stmt->nflds * NUMERIC_BUFSIZE);
                create_field_attrs(stmt);
                get_row_data(stmt);
            }
            return stmt->ret;
        case SQLITE_DONE:
            if (!stmt->fldnams && (stmt->nflds = sqlite3_column_count((sqlite3_stmt*)stmt->hstmt)))
                create_field_attrs(stmt);
            stmt->ret = XSQL_FIN;
            return XSQL_FIN;
        default:
            stmt->msg = strdup(sqlite3_errmsg((sqlite3*)stmt->conn->hdb));
            stmt->ret = XSQL_ERROR;
            return XSQL_ERROR;
    }
}

int sqlexec_direct (xsql_conn_t *conn, xsql_stmt_t **curstmt, const char *cmd, size_t cmd_len, list_t *params) {
    xsql_stmt_t *stmt = *curstmt = sqlprepare(conn, *curstmt, cmd, cmd_len, params->len);
    if (!stmt->msg)
        return sqlexec(stmt, params);
    stmt->ret = XSQL_ERROR;
    return XSQL_ERROR;
}

void sqlclose (xsql_stmt_t *stmt) {
    if (stmt->result) {
        free(stmt->result);
        free(((sqlite_t*)stmt->data)->result);
    }
    if (stmt->fldnams) {
        for (int i = 0; i < stmt->nflds; ++i)
            free(stmt->fldnams[i]);
        free(stmt->fldnams);
    }
    if (stmt->msg)
        free(stmt->msg);
    if (stmt->types)
        free(stmt->types);
    if (stmt->hstmt)
        sqlite3_finalize((sqlite3_stmt*)stmt->hstmt);
    free(stmt->data);
    free(stmt);
}

void dbclose (xsql_conn_t *conn) {
    free(conn->name);
    if (conn->hdb)
        sqlite3_close((sqlite3*)conn->hdb);
    if (conn->msg)
        free(conn->msg);
    free(conn);
}
