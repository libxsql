#include "xsql.h"

int xsql_gen_runtime_error (xsql_t *scr, const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    if (scr->err) free(scr->err);
    scr->err = malloc(XSQL_ERR_BUF_SIZE);
    vsnprintf(scr->err, XSQL_ERR_BUF_SIZE-2, fmt, ap);
    va_end(ap);
    return XSQL_ERROR;
}

xsql_ev_t *xsql_eval_add_var (xsql_type_t var_typ, xsql_raw_t *raw, xsql_var_free_h on_free) {
    xsql_ev_t *ev = calloc(1, sizeof(xsql_ev_t));
    ev->typ = EV_VAL;
    ev->var = xsql_var_init(var_typ, raw, on_free);
    ev->var_ptr = &ev->var;
    return ev;
}

xsql_ev_t *xsql_eval_add_var_link (xsql_var_t *var) {
    xsql_ev_t *ev = calloc(1, sizeof(xsql_ev_t));
    ev->typ = EV_VAL;
    ev->var_ptr = var;
    ev->var.typ = var->typ;
    return ev;
}

xsql_ev_t *xsql_eval_add_inst (xsql_inst_t inst, xsql_ev_t *left, xsql_ev_t *right) {
    xsql_ev_t *ev = calloc(1, sizeof(xsql_ev_t));
    ev->typ = EV_INST;
    ev->var_ptr = &ev->var;
    if (0 == xsql_get_inst(left->var_ptr->typ, inst, right->var_ptr->typ, &ev->inst, &ev->var.typ)) {
        ev->left = left;
        ev->right = right;
        return ev;
    }
    free(ev);
    return NULL;
}

xsql_ev_t *xsql_eval_add_uinst (xsql_inst_t inst, xsql_ev_t *right) {
    xsql_ev_t *ev = calloc(1, sizeof(xsql_ev_t));
    ev->typ = EV_UINST;
    if (0 == xsql_get_uinst(right->var.typ, inst, &ev->uinst, &ev->var.typ)) {
        ev->right = right;
        return ev;
    }
    free(ev);
    return NULL;
}

xsql_var_t xsql_eval_exec (xsql_ev_t *ev, int is_keep_consts) {
    if (ev->typ == EV_VAL) {
        xsql_var_t ret = *ev->var_ptr;
        ret.on_free = NULL;
        return ret;
    } else
    if (ev->typ == EV_INST) {
        xsql_var_t lval = xsql_eval_exec(ev->left, 1);
        xsql_var_t rval = xsql_eval_exec(ev->right, 1);
        xsql_var_t ret = ev->inst(lval, rval);
        if (ev->var.on_free)
            ev->var.on_free(&ev->var);
        ev->var = ret;
        if (ev->right->typ != EV_VAL && rval.on_free) {
            rval.on_free(&rval);
            rval.on_free = NULL;
            ev->right->var.on_free = NULL; // R1
        }
        return ev->var;
    } else {
        xsql_var_t rval = xsql_eval_exec(ev->right, is_keep_consts);
        return ev->uinst(rval);
    }
}

xsql_ev_t *xsql_eval_clone (xsql_script_t *scr, xsql_ev_t *ev) {
    if (!ev) return NULL;
    xsql_ev_t *ret = calloc(1, sizeof(xsql_ev_t));
    ret->typ = ev->typ;
    switch (ev->typ) {
        case EV_VAL:
            if (ev->var_ptr == &ev->var) {
                ret->var = ev->var;
                if (ev->var.on_free)
                    ret->var.raw.s_val = strclone(ev->var.raw.s_val);
                ret->var_ptr = &ret->var;
            } else {
                if (!(ev->var_ptr = xsql_get_var(scr, ev->var_ptr->name->ptr, ev->var_ptr->name->len))) {
                    xsql_gen_runtime_error(scr->scr_main, "%s not found", ev->var_ptr->name->ptr);
                    xsql_eval_free(ret);
                    return NULL;
                }
            }
            break;
        case EV_INST:
            ret->left = xsql_eval_clone(scr, ev->left);
            ret->right = xsql_eval_clone(scr, ev->right);
            ret->inst = ev->inst;
            break;
        default:
            ret->right = xsql_eval_clone(scr, ev->right);
            ret->uinst = ev->uinst;
            break;
    }
    return ret;
}

void xsql_eval_free (xsql_ev_t *ev) {
    if (!ev) return;
    if (ev->left) xsql_eval_free(ev->left);
    if (ev->right) xsql_eval_free(ev->right);
    if (ev->var.on_free) {
        ev->var.on_free(&ev->var);
        ev->var.on_free = NULL;
    }
    free(ev);
}

void xsql_op_exec (xsql_op_t *x) {
    while (x)
        x = x->exec(x);
}

xsql_op_t *xsql_op_no_clone (xsql_op_t *x) { return NULL; }

xsql_op_t *xsql_op_clone (xsql_script_t *dst, xsql_op_t *x) {
    xsql_op_t *ret = x->clone(dst, x), **op = &ret->next;
    while ((x = x->next) && (x = x->clone(dst, x))) {
        *op = x;
        op = &(*op)->next;
    }
    return ret;
}

void xsql_op_free (xsql_op_t *x) {
    xsql_op_t *c = NULL;
    while (x) {
        xsql_op_t *y = x->next;
        if (x->destroy && x->destroy != xsql_connect_destroy)
            x->destroy(x);
        else {
            x->next = c;
            c = x;
        }
        x = y;
    }
    while (c) {
        xsql_op_t *y = c->next;
        if (c->destroy)
            c->destroy(c);
        c = y;
    }
}

static int exec_args (xsql_t *scr, list_t *args) {
    if (args->len > scr->input_param_list->len)
        return xsql_gen_runtime_error(scr, "too many arguments for script %s", scr->name->ptr);
    else
    if (args->len < scr->input_param_list->len)
        return xsql_gen_runtime_error(scr, "too few arguments for script %s", scr->name->ptr);
    lst_clear(scr->param_list);
    list_item_t *li_a = args->head, *li_p = scr->input_param_list->head;
    if (li_a) {
        xsql_source_t txt;
        memset(&txt, 0, sizeof(xsql_source_t));
        do {
            strptr_t token;
            str_t *arg = (str_t*)li_a->ptr;
            txt.text = txt.text_ptr = arg->ptr;
            xsql_param_t *param = (xsql_param_t*)li_p->ptr;
            xsql_ev_t *ev = xsql_parse_eval(&txt, (xsql_script_t*)scr, &token);
            if (!ev)
                break;
            lst_adde(scr->param_list, ev);
            if (!(ev->var.typ == TYP_NONE || ev->var.typ == param->var->typ)) {
                char x_str [32], y_str [32];
                strcpy(x_str, xsql_typstr(param->var->typ));
                strcpy(y_str, xsql_typstr(ev->var.typ));
                xsql_gen_runtime_error(scr, "expected %s but argument if of type %s", x_str, y_str);
                break;
            }
            xsql_var_t ve = xsql_eval_exec(ev, 0);
            param->var->raw = ve.raw;
            li_a = li_a->next;
            li_p = li_p->next;
        } while (li_a != args->head);
        if (txt.err) {
            scr->err = txt.err;
            return XSQL_ERROR;
        }
    }
    return XSQL_OK;
}

void xsql_add_arg (list_t *arg_list, const char *arg) {
    str_t *str = mkstr(arg, strlen(arg), 8);
    if (';' != str->ptr[str->len-1])
        strnadd(&str, CONST_STR_LEN(";"));
}

void xsql_add_arg_ex (xsql_t *scr, list_t *arg_list, const char *arg, xsql_type_t expected_typ) {
    str_t *str;
    if (arg) {
        size_t arg_len = strlen(arg);
        if (TYP_STRING == expected_typ) {
            str = stralloc(arg_len, 8);
            strnadd(&str, CONST_STR_LEN("\""));
            strnadd(&str, arg, arg_len);
            strnadd(&str, CONST_STR_LEN("\""));
        } else
            str = mkstr(arg, arg_len, 8);
    } else
        str = mkstr(CONST_STR_LEN("NULL"), 8);
    if (';' != str->ptr[str->len-1])
        strnadd(&str, CONST_STR_LEN(";"));
    lst_adde(arg_list, str);
}

int xsql_script_exec (xsql_t *scr, list_t *args) {
    int ret;
    if (XSQL_OK != (ret = exec_args(scr, args)))
        return ret;
    if (scr->op)
        xsql_op_exec(scr->op);
    return scr->scr_main->err ? XSQL_ERROR : XSQL_OK;
}
#if 0
static void agg_reset (xsql_t *scr) {
    for (int i = 0; i < scr->agg_len; ++i) {
        xsql_agg_t *agg = &scr->aggs[i];
        memset(&agg->agg1.raw, 0, sizeof(xsql_raw_t));
        memset(&agg->agg2.raw, 0, sizeof(xsql_raw_t));
        memset(&agg->agg3.raw, 0, sizeof(xsql_raw_t));
    }
    scr->agg_idx = 0;
}
#endif
// -= assign =-
xsql_op_t *xsql_assign_exec (xsql_op_t *x) {
    xsql_assign_t *op = (xsql_assign_t*)x;
    xsql_var_t ret = xsql_eval_exec(op->ev, 0), *var = op->var;
    var->raw = ret.raw;
    var->typ = ret.typ;
    var->on_free = ret.on_free;
    return op->next;
}

void xsql_assign_destroy (xsql_op_t *x) {
    xsql_assign_t *op = (xsql_assign_t*)x;
    xsql_eval_free(op->ev);
    free(op);
}

// -= print =-
xsql_op_t *xsql_print_exec (xsql_op_t *x) {
    x->scr->scr_main->agg_idx = 0;
    xsql_print_t *op = (xsql_print_t*)x;
    xsql_script_t *scr = op->scr;
    xsql_t *scr_main = scr->scr_main;
    xsql_fmt_t *fmt = scr_main->fmt;
    if (!fmt) fmt = scr_main->fmtstd;
    if (!fmt) return op->next;
    xsql_fmtprov_t *fmtprov = fmt->prov;
    fmtprov->fmtstart_outvar(fmt);
    list_t *l = op->ev_list;
    list_item_t *a = op->ev_list->head;
    size_t index = 0;
    do {
        xsql_ev_t *ev = (xsql_ev_t*)a->ptr;
        xsql_var_t v = xsql_eval_exec(ev, 1);
        fmtprov->fmtoutvar(fmt, &v, index++);
        a = a->next;
    } while (a != l->head);
    fmtprov->fmtend_outvar(fmt);
    return op->next;
}

void xsql_print_destroy (xsql_op_t *x) {
    xsql_print_t *op = (xsql_print_t*)x;
    lst_free(op->ev_list);
    free(op);
}

// -= if =-
xsql_op_t *xsql_if_exec (xsql_op_t *x) {
    xsql_if_t *op = (xsql_if_t*)x;
    xsql_t *scr_main = op->scr->scr_main;
    xsql_var_t var = xsql_eval_exec(op->ev, 1);
    if (var.raw.b_val)
        xsql_op_exec(op->op_true);
    else
        if (op->op_false) xsql_op_exec(op->op_false);
    return scr_main->err ? NULL : op->next;
}

void xsql_if_destroy (xsql_op_t *x) {
    xsql_if_t *op = (xsql_if_t*)x;
    xsql_op_free(op->op_true);
    xsql_op_free(op->op_false);
    xsql_eval_free(op->ev);
    free(op);
}

// -= case =-
xsql_op_t *xsql_case_exec (xsql_op_t *x) {
    xsql_case_t *op = (xsql_case_t*)x;
    xsql_t *scr_main = op->scr->scr_main;
    list_item_t *li = op->items->head;
    if (li) {
        do {
            xsql_case_item_t *ci = (xsql_case_item_t*)li->ptr;
            xsql_var_t var = xsql_eval_exec(ci->ev, 1);
            if (var.raw.b_val) {
                xsql_op_exec(ci->body);
                break;
            }
            li = li->next;
        } while (li != op->items->head);
    }
    return scr_main->err ? NULL : op->next;
}

void xsql_case_destroy (xsql_op_t *x) {
    xsql_case_t *op = (xsql_case_t*)x;
    lst_free(op->items);
    free(op);
}

// -= for =-
static int is_in_iter (xsql_ev_t *ev) {
    xsql_var_t var = xsql_eval_exec(ev, 1);
    return var.raw.b_val;
}

xsql_op_t *xsql_for_exec (xsql_op_t *x) {
    xsql_for_t *op = (xsql_for_t*)x;
    xsql_t *scr_main = x->scr->scr_main;
    xsql_var_t var = xsql_eval_exec(op->ev_init, 0), *var_iter = op->var_iter;
    var_iter->raw = var.raw;
    var_iter->typ = var.typ;
    var_iter->on_free = var.on_free;
    while (is_in_iter(op->ev_end)) {
        xsql_op_exec(op->op_body);
        var = xsql_eval_exec(op->ev_step, 0);
        var_iter->raw = var.raw;
        var_iter->typ = var.typ;
        var_iter->on_free = var.on_free;
    }
    return scr_main->err ? NULL : op->next;
}

void xsql_for_destroy (xsql_op_t *x) {
    xsql_for_t *op = (xsql_for_t*)x;
    xsql_op_free(op->op_body);
    xsql_eval_free(op->ev_end);
    xsql_eval_free(op->ev_step);
    xsql_eval_free(op->ev_init);
    free(op);
}

// -= while =-
xsql_op_t *xsql_while_exec (xsql_op_t *x) {
    xsql_while_t *op = (xsql_while_t*)x;
    while (is_in_iter(op->ev))
        xsql_op_exec(op->op_body);
    return op->next;
}

void xsql_while_destroy (xsql_op_t* x) {
    xsql_while_t *op = (xsql_while_t*)x;
    xsql_op_free(op->op_body);
    xsql_eval_free(op->ev);
    free(op);
}

// -= connect =-
xsql_op_t *xsql_connect_exec (xsql_op_t *x) {
    xsql_connect_t *op = (xsql_connect_t*)x;
    xsql_t *scr_main = x->scr->scr_main;
    xsql_dbprov_t *prov = op->provider;
    strptr_t conninfo = CONST_STR_INIT_NULL;
    if (op->conn) {
        xsql_gen_runtime_error(scr_main, "connection \"%s\" is exists", op->name->ptr);
        return NULL;
    }
    if (op->conninfo) {
        conninfo.ptr = op->conninfo->ptr;
        conninfo.len = op->conninfo->len;
    } else {
        conninfo.ptr = op->v_conninfo->raw.s_val->ptr;
        conninfo.len = op->v_conninfo->raw.s_val->len;
    }
    xsql_conn_t *conn = prov->dbopen(op->provider, op->name->ptr, op->name->len, conninfo.ptr, conninfo.len);
    if (conn->msg) {
        scr_main->err = strdup(conn->msg);
        prov->dbclose(conn);
        return NULL;
    }
    scr_main->conn = conn;
    scr_main->cur_prov = op->provider;
    op->conn = conn;
    return op->next;
}

void xsql_connect_destroy (xsql_op_t *x) {
    xsql_connect_t *op = (xsql_connect_t*)x;
    free(op->name);
    free(op->provider_name);
    if (op->conninfo)
        free(op->conninfo);
    if (op->conn)
        op->conn->provider->dbclose(op->conn);
    free(op);
}

// -= exec query =-
xsql_op_t *xsql_sqlexec_exec (xsql_op_t *x) {
    xsql_sqlexec_t *op = (xsql_sqlexec_t*)x;
    xsql_t *scr_main = x->scr->scr_main;
    xsql_conn_t *conn = scr_main->conn;
    if (!conn) {
        xsql_gen_runtime_error(scr_main, "connection expected");
        return NULL;
    }
    xsql_dbprov_t *prov = conn->provider;
    int rc;
    if (op->recycling) {
        op->stmt = prov->prepare(conn, op->stmt, op->query->ptr, op->query->len, op->params->len);
        if (op->stmt->msg)
            goto err;
        rc = prov->sqlexec(op->stmt, op->params);
    } else
        rc = prov->sqlexec_direct(conn, &op->stmt, op->query->ptr, op->query->len, op->params);
    xsql_fmt_t *fmt = scr_main->fmt;
    if (!fmt) fmt = scr_main->fmtstd;
    switch (rc) {
        case XSQL_OK:
            if (fmt) {
                int styles [] = { IDFMT_ODD, IDFMT_EVEN }, id_style = 1;
                xsql_fmtprov_t *fmtprov = fmt->prov;
                if (fmtprov->fmtsetstyle)
                    fmtprov->fmtsetstyle(fmt, IDFMT_FLDNAM);
                fmtprov->fmtstart_outrec(fmt, op->stmt);
                if (fmtprov->fmtsetstyle)
                    fmtprov->fmtsetstyle(fmt, styles[!id_style]);
                fmtprov->fmtoutrec(fmt, op->stmt);
                while (XSQL_OK == (rc = prov->fetch(op->stmt))) {
                    if (fmtprov->fmtsetstyle)
                        fmtprov->fmtsetstyle(fmt, styles[!id_style]);
                    fmtprov->fmtoutrec(fmt, op->stmt);
                }
                fmtprov->fmtend_outrec(fmt, op->stmt);
                if (fmtprov->fmtsetstyle)
                    fmtprov->fmtsetstyle(fmt, IDFMT_NORMAL);
                if (XSQL_ERROR == rc) goto err;
            }
            break;
        case XSQL_FIN:
            break;
        case XSQL_ERROR: goto err;
    }
    if (!op->recycling) {
        prov->sqlclose(op->stmt);
        op->stmt = NULL;
        op->recycling = 1;
    }
    return op->next;
err:
    scr_main->err = strdup(op->stmt->msg);
    prov->sqlclose(op->stmt);
    op->stmt = NULL;
    return NULL;
}

void xsql_sqlexec_destroy (xsql_op_t *x) {
    xsql_sqlexec_t *op = (xsql_sqlexec_t*)x;
    xsql_t *scr_main = x->scr->scr_main;
    if (op->stmt) {
        xsql_conn_t *conn = scr_main->conn;
        xsql_dbprov_t *prov = conn->provider;
        prov->sqlclose(op->stmt);
    }
    lst_free(op->params);
    if (op->query)
        free(op->query);
    free(op);
}

// -= plot =-
xsql_op_t *xsql_plot_exec (xsql_op_t *x) {
    xsql_op_t *next = x->next;
    char *msg = NULL, cmd [256], *value;
    str_t *output = NULL;
    FILE *file = NULL;
    xsql_plot_t *op = (xsql_plot_t*)x;
    xsql_script_t *scr = op->scr;
    xsql_t *scr_main = scr->scr_main;
    xsql_conn_t *conn;
    xsql_dbprov_t *dbprov = NULL;
    int rc;
    xsql_fmt_t *fmt;
    xsql_fmt_plot_h plot;
    if (!(fmt = scr_main->fmt) || !(plot = fmt->prov->fmtplot))
        return op->next;
    if (!(conn = scr_main->conn)) {
        msg = "connection expected";
        goto done;
    }
    output = mktempnam(CONST_STR_LEN("/tmp"), CONST_STR_LEN("image_"));
    if (0 == strcmp(op->format, "png"))
        strnadd(&output, CONST_STR_LEN(".png"));
    else
    if (0 == strcmp(op->format, "postscript"))
        strnadd(&output, CONST_STR_LEN(".ps"));
    snprintf(cmd, sizeof(cmd), "gnuplot -e \"set term %s; set output '%s'; plot '-' with %s\"", op->format, output->ptr, op->style);
    if (!(file = popen(cmd, "w"))) {
        msg = strerror(errno);
        goto done;
    }
    dbprov = conn->provider;
    if (op->recycling) {
        op->stmt = dbprov->prepare(conn, op->stmt, op->query->ptr, op->query->len, op->params->len);
        if (op->stmt->msg) {
            msg = op->stmt->msg;
            goto done;
        }
        rc = dbprov->sqlexec(op->stmt, op->params);
    } else {
        rc = dbprov->sqlexec_direct(conn, &op->stmt, op->query->ptr, op->query->len, op->params);
        op->recycling = 1;
    }
    switch (rc) {
        case XSQL_OK:
            for (int i = 0; i < op->stmt->nflds; ++i) {
                value = op->stmt->result[i];
                if (value)
                    fwrite(value, strlen(value), 1, file);
                else
                    fwrite(CONST_STR_LEN(NULLSTR), 1, file);
                fwrite(CONST_STR_LEN("\t"), 1, file);
            }
            fwrite(CONST_STR_LEN("\n"), 1, file);
            while (XSQL_OK == (rc = dbprov->fetch(op->stmt))) {
                for (int i = 0; i < op->stmt->nflds; ++i) {
                    value = op->stmt->result[i];
                    if (value)
                        fwrite(value, strlen(value), 1, file);
                    else
                        fwrite(CONST_STR_LEN(NULLSTR), 1, file);
                    fwrite(CONST_STR_LEN("\t"), 1, file);
                }
                fwrite(CONST_STR_LEN("\n"), 1, file);
            }
            if (XSQL_ERROR == rc) {
                msg = op->stmt->msg;
                goto done;
            }
            fwrite(CONST_STR_LEN("e"), 1, file);
        case XSQL_FIN:
            break;
        case XSQL_ERROR:
            msg = op->stmt->msg;
            goto done;
    }
    pclose(file);
    file = NULL;
    plot(fmt, output->ptr, output->len);
    unlink(output->ptr);
done:
    if (msg) {
        xsql_gen_runtime_error(scr_main, msg);
        if (op->stmt) {
            dbprov->sqlclose(op->stmt);
            op->stmt = NULL;
        }
        next = NULL;
    }
    if (output) free(output);
    if (file) pclose(file);
    return next;
}

void xsql_plot_destroy (xsql_op_t *x) {
    xsql_plot_t *op = (xsql_plot_t*)x;
    xsql_t *scr_main = x->scr->scr_main;
    xsql_conn_t *conn = scr_main->conn;
    if (conn && op->stmt)
        conn->provider->sqlclose(op->stmt);
    if (op->params)
        lst_free(op->params);
    if (op->query)
        free(op->query);
    free(op);
}

// -= store =-
xsql_op_t *xsql_store_exec (xsql_op_t *x) {
    xsql_store_t *op = (xsql_store_t*)x;
    xsql_t *scr_main = x->scr->scr_main;
    xsql_fmtprov_t *fmtprov = op->fmtprov;
    xsql_fmt_t *fmt;
    if (!fmtprov) {
        scr_main->err = strdup("writer or formatter not defined");
        return NULL;
    }
    if (!op->fname && op->v_fname)
        op->fname = xsql_getpath(op->v_fname->raw.s_val->ptr, op->v_fname->raw.s_val->len);
    if (op->fname) {
        if (op->name)
            fmt = scr_main->fmt = fmtprov->fmtopen(fmtprov, op->fname->ptr, op->fname->len, op->name->ptr, op->name->len, XSQL_WRITE);
        else
            fmt = scr_main->fmt = fmtprov->fmtopen(fmtprov, op->fname->ptr, op->fname->len, CONST_STR_NULL, XSQL_WRITE);
    } else {
        if (op->name)
            fmt = scr_main->fmt = fmtprov->fmtopen(fmtprov, CONST_STR_NULL, op->name->ptr, op->name->len, XSQL_WRITE);
        else
            fmt = scr_main->fmt = fmtprov->fmtopen(fmtprov, CONST_STR_NULL, CONST_STR_NULL, XSQL_WRITE);
    }
    if (!scr_main->fmt) {
        if (0 != errno)
            scr_main->err = strdup(strerror(errno));
        return NULL;
    }
    xsql_op_exec(op->op_body);
    keyval_t *k = malloc(sizeof(keyval_t));
    k->key = mkstr(op->name->ptr, op->name->len, op->name->chunk_size);
    k->val = mkstr(fmt->fname->ptr, fmt->fname->len, fmt->fname->chunk_size);
    if (fmtprov->fmtend)
        fmtprov->fmtend(fmt);
    fmtprov->fmtclose(scr_main->fmt);
    scr_main->fmt = NULL;
    lst_adde(scr_main->files, (void*)k);
    return op->next;
}

void xsql_store_destroy (xsql_op_t *x) {
    xsql_store_t *op = (xsql_store_t*)x;
    xsql_op_free(op->op_body);
    if (op->name) free(op->name);
    if (op->fname) free(op->fname);
    free(op);
}

// -= load =-
static int reset_names (strptr_t **names, int *names_len, xsql_fmt_read_t *x) {
    strptr_t *ns = *names;
    if (ns) {
        for (int i = 0; i < *names_len; ++i)
            free(ns[i].ptr);
        if (x->len > *names_len) {
            strptr_t *p = realloc(ns, sizeof(strptr_t) * x->len);
            if (!p)
                return -1;
            *names = ns = p;
        } else
        if (0 == x->len) {
            free(*names);
            *names = NULL;
        }
    } else {
        *names = ns = malloc(sizeof(strptr_t) * x->len);
        if (!ns)
            return -1;
    }
    if ((*names_len = x->len) > 0) {
        for (int i = 0; i < x->len; ++i) {
            ns[i].ptr = strndup(x->ptr[i].ptr, x->ptr[i].len);
            ns[i].len = x->ptr[i].len;
        }
    }
    return 0;
}

static void clear_names (strptr_t **names, int *names_len) {
    strptr_t *ns = *names;
    if (ns) {
        for (int i = 0; i < *names_len; ++i)
            free(ns[i].ptr);
        free(ns);
        *names = NULL;
    }
    *names_len = 0;
}

xsql_op_t *xsql_load_exec (xsql_op_t *x) {
    int fin = 0, rc = 0;
    xsql_load_t *op = (xsql_load_t*)x;
    xsql_fmtprov_t *fmtprov = op->fmtprov;
    xsql_fmt_read_t data = { .typ = 0, .ptr = NULL, .len = 0 };
    strptr_t *names = NULL;
    int names_len = 0;
    if (!op->fname && op->v_fname)
        op->fname = xsql_getpath(op->v_fname->raw.s_val->ptr, op->v_fname->raw.s_val->len);
    xsql_fmt_t *fmt = fmtprov->fmtopen(fmtprov, op->fname->ptr, op->fname->len, CONST_STR_NULL, XSQL_READ);
    if (!fmt) {
        if (0 != errno)
            x->scr->scr_main->err = strdup(strerror(errno));
        return NULL;
    }
    while (!fin && 0 == (rc = fmtprov->fmtread(fmt, &data))) {
        switch (data.typ) {
            case FMT_BEGIN_DATA:
                reset_names(&names, &names_len, &data);
                break;
            case FMT_RECORD:
                if (names) {
                    for (int i = 0; i < names_len; ++i) {
                        xsql_var_t *v = xsql_get_var(x->scr, names[i].ptr, names[i].len);
                        if (v) {
                            if (data.ptr[i].ptr) {
                                if (v->raw.s_val)
                                    strput(&v->raw.s_val, data.ptr[i].ptr, data.ptr[i].len, STR_REDUCE);
                                else
                                    v->raw.s_val = mkstr(data.ptr[i].ptr, data.ptr[i].len, 32);
                                v->typ = TYP_STRING;
                            } else {
                                if (v->raw.s_val) {
                                    free(v->raw.s_val);
                                    v->raw.s_val = NULL;
                                }
                                v->typ = TYP_NONE;
                            }
                        } else {
                            char *s = strndup(names[i].ptr, names[i].len);
                            xsql_gen_runtime_error(x->scr->scr_main, "%s not found", s);
                            free(s);
                            if (data.ptr) {
                                for (int i = 0; i < data.len; ++i)
                                    free(data.ptr[i].ptr);
                                free(data.ptr);
                            }
                            fmtprov->fmtclose(fmt);
                            return NULL;
                        }
                    }
                    xsql_op_exec(op->op_body);
                }
                break;
            case FMT_END_DATA:
                clear_names(&names, &names_len);
                break;
            case FMT_END:
                fin = 1;
                break;
            case FMT_BEGIN:
            case FMT_NODATA:
                break;
        }
    }
    fmtprov->fmtclose(fmt);
    clear_names(&names, &names_len);
    if (-1 == rc) {
        xsql_gen_runtime_error(x->scr->scr_main, "file \"%s\" is corrupted", op->fname->ptr);
        return NULL;
    }
    return op->next;
}

void xsql_load_destroy (xsql_op_t *x) {
    xsql_load_t *op = (xsql_load_t*)x;
    xsql_op_free(op->op_body);
    if (op->fname) free(op->fname);
    if (op->var_list) lst_free(op->var_list);
    free(op);
}

// -= loop =-
static void set_result (xsql_stmt_t *stmt, list_t *var_list, str_t **svals) {
    int i = 0;
    list_item_t *li = var_list->head;
    do {
        char *sval = stmt->result[i];
        xsql_var_t *var = (xsql_var_t*)li->ptr;
        if (sval) {
            if (svals[i])
                strput(&svals[i], sval, strlen(sval), STR_REDUCE);
            else
                svals[i] = mkstr(sval, strlen(sval), XSQL_STR_CHUNK_SIZE);
            var->raw.s_val = svals[i];
        } else {
            if (svals[i]) free(svals[i]);
            var->raw.s_val = NULL;
        }
        var->typ = TYP_STRING;
        var->on_free = NULL;
        li = li->next;
        ++i;
    } while (li != var_list->head);
}

xsql_op_t *xsql_loop_exec (xsql_op_t *x) {
    xsql_loop_t *op = (xsql_loop_t*)x;
    xsql_t *scr_main = x->scr->scr_main;
    xsql_conn_t *conn = scr_main->conn;
    if (!conn) {
        xsql_gen_runtime_error(scr_main, "connection expected");
        return NULL;
    }
    xsql_dbprov_t *prov = conn->provider;
    int rc;
    if (op->recycling) {
        op->stmt = prov->prepare(conn, op->stmt, op->query->ptr, op->query->len, op->params->len);
        if (op->stmt->msg)
            goto err;
        rc = prov->sqlexec(op->stmt, op->params);
    } else
        rc = prov->sqlexec_direct(conn, &op->stmt, op->query->ptr, op->query->len, op->params);
    xsql_fmt_t *fmt = scr_main->fmt;
    if (!fmt) fmt = scr_main->fmtstd;
    switch (rc) {
        case XSQL_OK:
            if (op->stmt->nflds != op->var_list->len) {
                xsql_gen_runtime_error(scr_main, "variable count error");
                prov->sqlclose(op->stmt);
                op->stmt = NULL;
                return NULL;
            }
            set_result(op->stmt, op->var_list, op->svals);
            xsql_op_exec(op->op_body);
            while (XSQL_OK == (rc = prov->fetch(op->stmt))) {
                set_result(op->stmt, op->var_list, op->svals);
                xsql_op_exec(op->op_body);
            }
            if (XSQL_ERROR == rc) goto err;
            break;
        case XSQL_FIN:
            break;
        case XSQL_ERROR: goto err;
    }
    if (!op->recycling) {
        prov->sqlclose(op->stmt);
        op->stmt = NULL;
        op->recycling = 1;
    }
    return op->next;
    err:
    scr_main->err = strdup(op->stmt->msg);
    prov->sqlclose(op->stmt);
    op->stmt = NULL;
    return NULL;
}

void xsql_loop_destroy (xsql_op_t *x) {
    xsql_loop_t *op = (xsql_loop_t*)x;
    xsql_t *scr_main = x->scr->scr_main;
    if (op->stmt) {
        xsql_conn_t *conn = scr_main->conn;
        xsql_dbprov_t *prov = conn->provider;
        prov->sqlclose(op->stmt);
    }
    lst_free(op->params);
    if (op->query)
        free(op->query);
    if (op->svals) {
        for (size_t i = 0; i < op->svals_len; ++i) {
            str_t *s = op->svals[i];
            if (s) free(s);
        }
        free(op->svals);
    }
    if (op->var_list)
        lst_free(op->var_list);
    xsql_op_free(op->op_body);
    free(op);
}

// -= new tab =-
xsql_op_t *xsql_newtab_exec (xsql_op_t *x) {
    xsql_newtab_t *op = (xsql_newtab_t*)x;
    xsql_t *scr_main = x->scr->scr_main;
    xsql_fmt_t *fmt = scr_main->fmt;
    if (fmt) {
        xsql_fmtprov_t *fmtprov = fmt->prov;
        if (fmtprov->fmtnewtab) {
            if (op->name)
                fmtprov->fmtnewtab(fmt, op->name->ptr, op->name->len);
            else
                fmtprov->fmtnewtab(fmt, CONST_STR_NULL);
        }
    }
    return op->next;
}

void xsql_newtab_destroy (xsql_op_t *x) {
    xsql_newtab_t *op = (xsql_newtab_t*)x;
    if (op->name) free(op->name);
    free(op);
}

// -= use connection =-
xsql_op_t *xsql_use_exec (xsql_op_t *x) {
    xsql_use_t *op = (xsql_use_t*)x;
    xsql_t *scr_main = x->scr->scr_main;
    scr_main->cur_prov = op->provider;
    scr_main->conn = op->op_conn->conn;
    return op->next;
}

void xsql_use_destroy (xsql_op_t *x) {
    xsql_use_t *op = (xsql_use_t*)x;
    if (op->connection_name)
        free(op->connection_name);
    free(op);
}
