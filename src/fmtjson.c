#include "fmt.h"

typedef struct {
    str_t *name;
    str_t *fname;
    xsql_fmtprov_t *prov;
    int flags;
    int fd;
    str_t *str;
    int is_first;
} xsql_json_t;

static const char *json_fmtname () {
    return "json";
}

static xsql_fmt_t *json_open (xsql_fmtprov_t *prov, const char *fname, size_t fname_len, const char *name, size_t name_len, int flags) {
    xsql_json_t *ret = calloc(1, sizeof(xsql_json_t));
    ret->name = mkstr(name, name_len, 8);
    ret->fname = prov->fmtwrtname(fname, fname_len);
    ret->prov = prov;
    ret->flags = flags;
    if (-1 == prov->fmtwrtopen((xsql_fmt_t*)ret)) {
        if (ret->fname) free(ret->fname);
        free(ret);
        return NULL;
    }
    if (XSQL_WRITE == ret->flags)
        ret->prov->fmtwrtstr((xsql_fmt_t*)ret, CONST_STR_LEN("["), 0);
    return (xsql_fmt_t*)ret;
}

static void add_result (xsql_json_t *json, xsql_stmt_t *stmt, int is_comma, str_t **str) {
    char *s = stmt->result[0];
    (*str)->len = 0;
    if (is_comma)
        strnadd(str, CONST_STR_LEN(","));
    if (s) {
        strnadd(str, CONST_STR_LEN("[\""));
        strnadd(str, s, strlen(s));
        strnadd(str, CONST_STR_LEN("\""));
    } else
        strnadd(str, CONST_STR_LEN("[null"));
    for (int i = 1; i < stmt->nflds; ++i) {
        s = stmt->result[i];
        if (s) {
            strnadd(str, CONST_STR_LEN(",\""));
            strnadd(str, s, strlen(s));
            strnadd(str, CONST_STR_LEN("\""));
        } else
            strnadd(str, CONST_STR_LEN(",null"));
    }
    strnadd(str, CONST_STR_LEN("]"));
}

static void json_start_outrec (xsql_fmt_t *fmt, xsql_stmt_t *stmt) {
    xsql_json_t *json = (xsql_json_t*)fmt;
    xsql_fmtprov_t *prov = fmt->prov;
    str_t *fldnam = stmt->fldnams[0];
    if (json->str) {
        json->str->len = 0;
        strnadd(&json->str, CONST_STR_LEN(","));
    } else
        json->str = stralloc(512, 256);
//    strnadd(&json->str, CONST_STR_LEN("{\"result\":{\"header\":[\""));
    strnadd(&json->str, CONST_STR_LEN("{\"header\":[\""));
    strnadd(&json->str, fldnam->ptr, fldnam->len);
    strnadd(&json->str, CONST_STR_LEN("\""));
    for (int i = 1; i < stmt->nflds; ++i) {
        fldnam = stmt->fldnams[i];
        strnadd(&json->str, CONST_STR_LEN(",\""));
        strnadd(&json->str, fldnam->ptr, fldnam->len);
        strnadd(&json->str, CONST_STR_LEN("\""));
    }
    strnadd(&json->str, CONST_STR_LEN("],\"data\":["));
    prov->fmtwrtstr(fmt, json->str->ptr, json->str->len, 0);
//    add_result(json, stmt, 0, &json->str);
//    prov->fmtwrtstr(fmt, json->str->ptr, json->str->len, 0);
}

static void json_outrec (xsql_fmt_t *fmt, xsql_stmt_t *stmt) {
    xsql_json_t *json = (xsql_json_t*)fmt;
    add_result(json, stmt, 1, &json->str);
    fmt->prov->fmtwrtstr(fmt, json->str->ptr, json->str->len, 0);
}

static void json_end_outrec (xsql_fmt_t *fmt, xsql_stmt_t *stmt) {
    //fmt->prov->fmtwrtstr(fmt, CONST_STR_LEN("]}}"), 0);
    fmt->prov->fmtwrtstr(fmt, CONST_STR_LEN("]}"), 0);
}

static void json_start_outvar (xsql_fmt_t *fmt) {
    xsql_json_t *json = (xsql_json_t*)fmt;
    if (json->str) {
        json->str->len = 0;
        strnadd(&json->str, CONST_STR_LEN(","));
    } else
        json->str = stralloc(512, 256);
    strnadd(&json->str, CONST_STR_LEN("["));
    json->is_first = 1;
}

static void json_outvar (xsql_fmt_t *fmt, xsql_var_t *var, size_t index) {
    xsql_json_t *json = (xsql_json_t*)fmt;
    strptr_t str;
    if (!json->is_first)
        strnadd(&json->str, CONST_STR_LEN(","));
    json->is_first = 0;
    if (var->typ == TYP_STRING) {
        str.ptr = var->raw.s_val->ptr;
        str.len = var->raw.s_val->len;
    } else {
        const char *s = xsql_varstr(var);
        str.ptr = (char*)s;
        str.len = strlen(s);
    }
    strnadd(&json->str, CONST_STR_LEN("\""));
    strnadd(&json->str, str.ptr, str.len);
    strnadd(&json->str, CONST_STR_LEN("\""));
//    fmt->prov->fmtwrtstr(fmt, json->str->ptr, json->str->len, 0);
//    json->str->len = 0;
}

static void json_end_outvar (xsql_fmt_t *fmt) {
    xsql_json_t *json = (xsql_json_t*)fmt;
    strnadd(&json->str, CONST_STR_LEN("]"));
//    fmt->prov->fmtwrtstr(fmt, CONST_STR_LEN("]"), 0);
    fmt->prov->fmtwrtstr(fmt, json->str->ptr, json->str->len, 0);
    json->str->len = 0;
}

static void json_plot (xsql_fmt_t *fmt, const char *fname, size_t fname_len) {
    str_t *buf = load_all_file(fname, 8, 0);
    if (!buf) return;
    xsql_json_t *json = (xsql_json_t*)fmt;
    if (json->str) {
        json->str->len = 0;
        strnadd(&json->str, CONST_STR_LEN(","));
    } else
        json->str = stralloc(buf->len > 512 ? buf->len : 512, 256);
    strnadd(&json->str, CONST_STR_LEN("{\"image\":\""));
    fmt->prov->fmtwrtstr(fmt, json->str->ptr, json->str->len, 0);
    str_t *edata = str_base64_encode(buf->ptr, buf->len, 256);
    strnadd(&edata, CONST_STR_LEN("\"}"));
    fmt->prov->fmtwrtstr(fmt, edata->ptr, edata->len, 0);
    free(edata);
    free(buf);
}

static void json_close (xsql_fmt_t *fmt) {
    xsql_json_t *json = (xsql_json_t*)fmt;
    if (XSQL_WRITE == fmt->flags)
        fmt->prov->fmtwrtstr(fmt, CONST_STR_LEN("]"), 0);
    if (json->str) free(json->str);
    if (json->fname) free(json->fname);
    if (json->name) free(json->name);
    if (json->fd > 0) close(json->fd);
    free(json);
}

xsql_fmtprov_t *xsql_create_json_provider () {
    xsql_fmtprov_t *ret = calloc(1, sizeof(xsql_fmtprov_t));
    ret->hnd = 0;
    ret->fmtname = json_fmtname;
    ret->fmtwrtname = file_wrtname;
    ret->fmtopen = json_open;
    ret->fmtwrtopen = file_wrtopen;
    ret->fmtstart_outrec = json_start_outrec;
    ret->fmtoutrec = json_outrec;
    ret->fmtend_outrec = json_end_outrec;
    ret->fmtstart_outvar = json_start_outvar;
    ret->fmtwrtvar = file_wrtvar;
    ret->fmtwrtstr = file_wrtstr;
    ret->fmtoutvar = json_outvar;
    ret->fmtend_outvar = json_end_outvar;
    ret->fmtwrtend = file_wrtend;
    ret->fmtclose = json_close;
    ret->fmtwrtclose = file_wrtclose;
    ret->fmtend = NULL;
    ret->fmtnewtab = NULL;
    ret->fmtplot = json_plot;
    return ret;
}
