#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <libex/str.h>
#include <libex/file.h>
#include <libex/list.h>
#include "db.h"

#define CONF_XSQL "xsql.rc"
#define CONF_PROVIDERS "providers"
#define CONF_FORMATTERS "formatters"

strptr_t conf_providers [] = {
    CONST_STR_INIT("libxsql_gds.so"),
    CONST_STR_INIT("libxsql_mdb.so"),
    CONST_STR_INIT("libxsql_mysql.so"),
    CONST_STR_INIT("libxsql_pg.so"),
    CONST_STR_INIT("libxsql_sqlite.so")
};
strptr_t conf_formatters [] = {
    CONST_STR_INIT("libxsql_xlsx.so")
};

str_t *providers = NULL,
      *formatters = NULL;

const char *dbfuncs [] = {
    "get_placeholder", "dbopen", "dbclose", "sqlprepare", "sqlexec", "sqlexec_direct", "fetch", "sqlclose"
};

const char *fmtfuncs [] = {
    "fmtwrtname", "fmtopen", "fmtwrtopen", "fmtstart_outrec", "fmtoutrec", "fmtend_outrec", "fmtstart_outvar",
    "fmtoutvar", "fmtwrtvar", "fmtwrtstr", "fmtend_outvar", "fmtwrtend", "fmtclose", "fmtwrtclose",
    "fmtplot", "fmtsetbg", "fmtsetfg"
};

static void load_xsql_conf () {
    load_conf(CONF_XSQL, CONF_HANDLER
        ASSIGN_CONF_STR(providers, CONF_PROVIDERS)
        ASSIGN_CONF_STR(formatters, CONF_FORMATTERS)
    CONF_HANDLER_END);
}

static void free_xsql_conf () {
    if (providers) free(providers);
    providers = NULL;
    if (formatters) free(formatters);
    formatters = NULL;
}

static void show_conf_items (str_t *conf_val) {
    int n;
    strptr_t val = { .ptr = conf_val->ptr, .len = conf_val->len };
    strptr_t tok;
    while ((n = strntok(&val.ptr, &val.len, CONST_STR_LEN(STR_SPACES), &tok)) > 0) {
        char *s = strndup(tok.ptr, tok.len);
        printf("\t%s\n", s);
        free(s);
    }
}

static void show () {
    load_xsql_conf();
    printf("Providers:\n");
    if (providers)
        show_conf_items(providers);
    printf("Formatters:\n");
    if (formatters)
        show_conf_items(formatters);
    free_xsql_conf();
}

static int check_lib (const char *libname, const char *fnames[], int size, const char *fname) {
    int rc = 0;
    void *hnd = dlopen(libname, RTLD_LAZY);
    if (!hnd) return -1;
    xsql_dbprov_name_h get_provider_name = (xsql_dbprov_name_h)dlsym(hnd, fname);
    if (!get_provider_name) {
        dlclose(hnd);
        return -1;
    }
    for (int i = 0; i < size; ++i) {
        if (!dlsym(hnd, fnames[i])) {
            rc = -1;
            break;
        }
    }
    if (-1 == rc)
        return -1;
    printf("%s found\n", get_provider_name());
    return 0;
}

static str_t *gen (strptr_t *name, const char *fnames[], int fsize, const char *fname) {
    str_t *res = NULL;
    char cmd [256];
    FILE *f;
    snprintf(cmd, sizeof cmd, "whereis %s", name->ptr);
    if ((f = popen(cmd, "r"))) {
        ssize_t readed;
        size_t n = 0;
        char *p, *line = NULL;
        str_t *s = stralloc(128, 128);
        while (-1 != (readed = getline(&line, &n, f))) {
            if (s->len > 0) {
                printf("too many libraries, see whereis %s\n", name->ptr);
                s->len = 0;
                break;
            }
            strnadd(&s, line, readed);
        }
        if ((p = strnchr(s->ptr, ':', s->len))) {
            size_t len = s->len - ((uintptr_t)p - (uintptr_t)s->ptr);
            strptr_t p_name;
            while (strntok(&p, &len, CONST_STR_LEN(STR_SPACES), &p_name) >= 0 && p_name.len > 0) {
                strptr_t f_name;
                if (0 == getfname(p_name.ptr, p_name.len, &f_name) &&
                    0 == cmpstr(f_name.ptr, f_name.len, name->ptr, name->len)) {
                    res = mkstr(p_name.ptr, p_name.len, 8);
                    if (0 == check_lib(res->ptr, fnames, fsize, fname))
                        break;
                    free(res);
                    res = NULL;
                }
            }
        }
        /*if ((p = strnchr(s->ptr, ':', s->len))) {
            ++p;
            while (isspace(*p)) ++p;
            if ('\0' != *p) {
                char *q = p;
                while (!isspace(*q)) ++q;
                res = mkstr(p, s->len - ((uintptr_t)p - (uintptr_t)s->ptr), 8);
                res->len--;
                res->ptr[res->len] = '\0';
                if (-1 == check_lib(res->ptr, fnames, fsize, fname)) {
                    free(res);
                    res = NULL;
                }
            }
            free(s);
        }*/
        free(s);
        if (line) free(line);
        pclose(f);
    }
    return res;
}

static list_t *enum_dlibs (strptr_t *confs, size_t size, const char *fnames[], int fsize, const char *fname) {
    list_t *lst = lst_alloc(on_default_free_item);
    for (int i = 0; i < size; ++i) {
        str_t *s = gen(&confs[i], fnames, fsize, fname);
        if (s) lst_adde(lst, s);
    }
    return lst;
}

static void upgrade (int fd, const char *hdr, strptr_t *confs, size_t size, const char *fnames[], int fsize, const char *fname) {
    list_t *lst = enum_dlibs(confs, size, fnames, fsize, fname);
    list_item_t *li;
    if ((li = lst->head)) {
        str_t *s = (str_t*)li->ptr;
        write(fd, hdr, strlen(hdr));
        write(fd, s->ptr, s->len);
        li = li->next;
        while (li != lst->head) {
            s = (str_t*)li->ptr;
            write(fd, CONST_STR_LEN(","));
            write(fd, s->ptr, s->len);
            li = li->next;
        }
    }
    lst_free(lst);
    write(fd, CONST_STR_LEN("\n"));
}

static void upgrade_conf (const char *rc_path) {
    int fd = open(rc_path, O_CREAT | O_TRUNC | O_WRONLY, 0644);
    if (fd) {
        upgrade(fd, "providers=", conf_providers, sizeof(conf_providers)/sizeof(strptr_t), dbfuncs, sizeof(dbfuncs)/sizeof(void*), "get_provider_name");
        upgrade(fd, "formatters=", conf_formatters, sizeof(conf_formatters)/sizeof(strptr_t), fmtfuncs, sizeof(fmtfuncs)/sizeof(void*), "fmtname");
        close(fd);
    }
}

static void help (int argc, const char *argv[]) {
    printf("%s options\n", argv[0]);
    printf(" -v show plugins\n");
    printf(" -u update file configuration /etc/xsql.rc\n");
}

int main (int argc, const char *argv[]) {
    if (2 == argc && 0 == strcmp(argv[1], "-v")) {
        show();
        return 0;
    }
    if (2 == argc && 0 == strcmp(argv[1], "-u")) {
        str_t *rc_path = NULL;
        if (0 == getuid())
            rc_path = mkstr(CONST_STR_LEN("/etc/xsql.rc"), 8);
        else {
            rc_path = get_spec_path(DIR_HOME);
            rc_path = path_add_path(rc_path, ".xsql.rc", NULL);
        }
        upgrade_conf(rc_path->ptr);
        free(rc_path);
        return 0;
    }
    help(argc, argv);
    return 0;
}
