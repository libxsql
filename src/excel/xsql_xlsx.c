#include "../../include/libxsql/fmt.h"
#include <xlsxwriter/workbook.h>
#include <xlsxwriter/worksheet.h>
#include <xlsxwriter/format.h>
#include <xlsxwriter/utility.h>

typedef struct {
    str_t *name;
    str_t *fname;
    xsql_fmtprov_t *prov;
    lxw_workbook *wb;
    lxw_worksheet *ws;
    lxw_format *fmt;
    lxw_format *fmts [IDFMT_MAX];
    int id_row;
} excel_t;

const char *fmtname () {
    return "excel";
}

str_t *fmtwrtname (const char *fname, size_t fname_len) {
    return file_wrtname(fname, fname_len);
}

xsql_fmt_t *fmtopen (xsql_fmtprov_t *prov, const char *fname, size_t fname_len, const char *name, size_t name_len, int flags) {
    excel_t *ret = calloc(1, sizeof(excel_t));
    ret->prov = prov;
    ret->name = mkstr(name, name_len, 8);
    ret->fname = prov->fmtwrtname(fname, fname_len);
    if (-1 == prov->fmtwrtopen((xsql_fmt_t*)ret)) {
        if (ret->fname) free(ret->fname);
        free(ret);
    }
    return (xsql_fmt_t*)ret;
}

void fmtsetbg (xsql_fmt_t *fmt, int id, int32_t color) {
    if (id >= 0 && id < IDFMT_MAX)
        format_set_bg_color(((excel_t*)fmt)->fmts[id], color);
}

void fmtsetfg (xsql_fmt_t *fmt, int id, int32_t color) {
    if (id >= 0 && id < IDFMT_MAX)
        format_set_font_color(((excel_t*)fmt)->fmts[id], color);
}

static void xls_set_color (excel_t *xls, int id, int32_t bg, int32_t fg, int is_bold) {
    lxw_format *fmt = xls->fmts[id] = workbook_add_format(xls->wb);
    format_set_bg_color(fmt, bg);
    format_set_font_color(fmt, fg);
    if (is_bold) format_set_bold(fmt);
}

int fmtwrtopen (xsql_fmt_t *fmt) {
    excel_t *xls = (excel_t*)fmt;
    //xls->wb = new_workbook(xls->fname->ptr);
    xls->wb = workbook_new(xls->fname->ptr);
    if (!xls->wb) return -1;
    xls_set_color(xls, IDFMT_NORMAL, BG_NORMAL, FG_NORMAL, 0);
    xls_set_color(xls, IDFMT_ODD, BG_ODD, FG_ODD, 0);
    xls_set_color(xls, IDFMT_EVEN, BG_EVEN, FG_EVEN, 0);
    xls_set_color(xls, IDFMT_FLDNAM, BG_FLDNAM, FG_FLDNAM, 1);
    xls_set_color(xls, IDFMT_HEADER, BG_HEADER, FG_HEADER, 0);
/*    xls->fmt = workbook_add_format(xls->wb);
    xls->fmt_header = workbook_add_format(xls->wb);
    format_set_bold(xls->fmt_header);
    format_set_font_color(xls->fmt_header, LXW_COLOR_WHITE);
    format_set_bg_color(xls->fmt_header, 0x429de3);
    xls->fmt_main = workbook_add_format(xls->wb);
    format_set_bg_color(xls->fmt_main, LXW_COLOR_SILVER);*/
    xls->ws = workbook_add_worksheet(xls->wb, fmt->name ? fmt->name->ptr : NULL);
    return 0;
}

void fmtsetstyle (xsql_fmt_t *fmt, int id) {
    if (id >= 0 && id < IDFMT_MAX) {
        excel_t *xls = (excel_t*)fmt;
        xls->fmt = xls->fmts[id];
    }
}

void fmtoutrec (xsql_fmt_t *fmt, xsql_stmt_t *stmt) {
    excel_t *xls = (excel_t*)fmt;
    xsql_fmtprov_t *prov = xls->prov;
    ++xls->id_row;
//    xls->fmt = xls->fmt_main;
    for (int i = 0; i < stmt->nflds; ++i)
        if (stmt->result[i])
            prov->fmtwrtstr(fmt, stmt->result[i], 0, i);
}

void fmtstart_outrec (xsql_fmt_t *fmt, xsql_stmt_t *stmt) {
    excel_t *xls = (excel_t*)fmt;
    xsql_fmtprov_t *prov = xls->prov;
    for (int i = 0; i < stmt->nflds; ++i) {
        str_t *fldnam = stmt->fldnams[i];
        prov->fmtwrtstr(fmt, fldnam->ptr, fldnam->len, i);
    }
//    xls->fmt = xls->fmt_main;
//    fmtoutrec(fmt, stmt);
}

void fmtend_outrec (xsql_fmt_t *fmt, xsql_stmt_t *stmt) {
    ((excel_t*)fmt)->id_row++;
}

void fmtstart_outvar (xsql_fmt_t *fmt) {
}

void fmtoutvar (xsql_fmt_t *fmt, xsql_var_t *var, size_t index) {
//    ((excel_t*)fmt)->fmt = NULL;
    fmt->prov->fmtwrtvar(fmt, var, index);
}

int fmtwrtvar (xsql_fmt_t *fmt, xsql_var_t *var, size_t index) {
    excel_t *xls = (excel_t*)fmt;
    switch (var->typ) {
        case TYP_STRING:
            worksheet_write_string(xls->ws, xls->id_row, index, var->raw.s_val->ptr, xls->fmt);
            break;
        case TYP_INT:
            worksheet_write_number(xls->ws, xls->id_row, index, var->raw.i_val, xls->fmt);
            break;
        case TYP_FLOAT:
            worksheet_write_number(xls->ws, xls->id_row, index, var->raw.f_val, xls->fmt);
            break;
        default:
            worksheet_write_string(xls->ws, xls->id_row, index, xsql_varstr(var), xls->fmt);
            break;
    }
    return 0;
}

int fmtwrtstr (xsql_fmt_t *fmt, const char *str, size_t str_len, size_t index) {
    excel_t *xls = (excel_t*)fmt;
    worksheet_write_string(xls->ws, xls->id_row, index, str, xls->fmt);
    return 0;
}

void fmtend_outvar (xsql_fmt_t *fmt) {
    fmt->prov->fmtwrtend(fmt);
}

int fmtwrtend (xsql_fmt_t *fmt) {
    ((excel_t*)fmt)->id_row++;
    return 0;
}

void fmtclose (xsql_fmt_t *fmt) {
    excel_t *xls = (excel_t*)fmt;
    workbook_close(xls->wb);
    free(xls->fname);
    if (xls->name) free(xls->name);
    free(xls);
}

void fmtwrtclose (xsql_fmt_t *fmt) {
    excel_t *xls = (excel_t*)fmt;
    workbook_close(xls->wb);
    if (fmt->fname)
        free(xls->fname);
    free(xls);
}

void fmtnewtab (xsql_fmt_t *fmt, const char *name, size_t name_len) {
    excel_t *xls = (excel_t*)fmt;
    if (!xls->wb) return;
    xls->ws = workbook_add_worksheet(xls->wb, name);
    xls->id_row = 0;
}

void fmtplot (xsql_fmt_t *fmt, const char *fname, size_t fname_len) {
    lxw_image_options opt = { .x_scale = 0.35, .y_scale = 0.35 };
    excel_t *xls = (excel_t*)fmt;
    worksheet_set_row(xls->ws, xls->id_row, 128, NULL);
    worksheet_insert_image_opt(xls->ws, xls->id_row++, 0, fname, &opt);
}
