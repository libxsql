#include "db.h"

list_t *dbprovs = NULL;

static char *load_provider (const char *libname) {
    char *errmsg = NULL;
    xsql_dbprov_t *prov = malloc(sizeof(xsql_dbprov_t));
    prov->hnd = dlopen(libname, RTLD_LAZY);
    if (!prov->hnd) {
        errmsg = dlerror();
        free(prov);
        goto err0;
    }
    dlerror();
    xsql_dbprov_name_h get_provider_name = (xsql_dbprov_name_h)dlsym(prov->hnd, "get_provider_name");
    if ((errmsg = dlerror())) goto err;
    const char *name = get_provider_name();
    prov->get_placeholder = (xsql_placeholder_h)dlsym(prov->hnd, "get_placeholder");
    if ((errmsg = dlerror())) goto err;
    prov->dbopen = (xsql_dbopen_h)dlsym(prov->hnd, "dbopen");
    if ((errmsg = dlerror())) goto err;
    prov->dbclose = (xsql_dbclose_h)dlsym(prov->hnd, "dbclose");
    if ((errmsg = dlerror())) goto err;
    prov->prepare = (xsql_sqlprepare_h)dlsym(prov->hnd, "sqlprepare");
    if ((errmsg = dlerror())) goto err;
    prov->sqlexec = (xsql_sqlexec_h)dlsym(prov->hnd, "sqlexec");
    if ((errmsg = dlerror())) goto err;
    prov->sqlexec_direct = (xsql_sqlexec_direct_h)dlsym(prov->hnd, "sqlexec_direct");
    if ((errmsg = dlerror())) goto err;
    prov->fetch = (xsql_fetch_h)dlsym(prov->hnd, "fetch");
    if ((errmsg = dlerror())) goto err;
    prov->sqlclose = (xsql_sqlclose_h)dlsym(prov->hnd, "sqlclose");
    if ((errmsg = dlerror())) goto err;
    prov->name = mkstr(name, strlen(name), 8);
    lst_adde(dbprovs, (void*)prov);
    return NULL;
    err:
    dlclose(prov->hnd);
    free(prov);
    err0:
    return errmsg;
}

xsql_dbprov_t *xsql_find_provider (const char *name, size_t name_len) {
    list_item_t *li;
    if (!dbprovs || !(li = dbprovs->head))
        return NULL;
    do {
        xsql_dbprov_t *p = (xsql_dbprov_t*)li->ptr;
        if (0 == cmpstr(p->name->ptr, p->name->len, name, name_len))
            return p;
        li = li->next;
    } while (li != dbprovs->head);
    return NULL;
}

static void dbfree (void *dummy, xsql_dbprov_t *p) {
    free(p->name);
    dlclose(p->hnd);
    free(p);
}

str_t *xsql_dbinit (const char *conf_str, size_t conf_str_len) {
    str_t *msgs = NULL;
    dbprovs = lst_alloc((free_h)dbfree);
    strptr_t instr = { .ptr = (char*)conf_str, .len = conf_str_len }, str;
    while (0 == strntok(&instr.ptr, &instr.len, CONST_STR_LEN(STR_SPACES ","), &str)) {
        char *fname = strndup(str.ptr, str.len),
             *msg = load_provider(fname);
        if (msg) {
            if (!msgs)
                msgs = mkstr(msg, strlen(msg), 64);
            else {
                strnadd(&msgs, CONST_STR_LEN("\n"));
                strnadd(&msgs, msg, strlen(msg));
            }
        }
        free(fname);
    }
    return msgs;
}

void xsql_dbdone () {
    lst_free(dbprovs);
}

int parse_conninfo (char *conninfo, size_t conninfo_len, char **host, char **dbname, char **user, char **pass) {
    char **val;
    strptr_t tok;
    while (0 == strntok(&conninfo, &conninfo_len, CONST_STR_LEN("=;"), &tok)) {
        if (0 == cmpcasestr(tok.ptr, tok.len, CONST_STR_LEN("HOST"))) val = host; else
        if (0 == cmpcasestr(tok.ptr, tok.len, CONST_STR_LEN("DBNAME"))) val = dbname; else
        if (0 == cmpcasestr(tok.ptr, tok.len, CONST_STR_LEN("USER"))) val = user; else
        if (0 == cmpcasestr(tok.ptr, tok.len, CONST_STR_LEN("PASS"))) val = pass; else
            return -1;
        if (0 != strntok(&conninfo, &conninfo_len, CONST_STR_LEN("=;"), &tok))
            return -1;
        *val = strndup(tok.ptr, tok.len);
    }
    return 0;
}

