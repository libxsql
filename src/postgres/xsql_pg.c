#include <time.h>
#include <libpq-fe.h>
#include "var.h"
#include "db.h"

#define SQL_DBTYPES "select oid, typbasetype from pg_type where typcategory = 'N' order by oid"

#define PGTYP_FLOAT4 700
#define PGTYP_FLOAT8 701
#define PGTYP_INT2 21
#define PGTYP_INT4 23
#define PGTYP_INT8 20
#define PGTYP_MONEY 790
#define PGTYP_NUMERIC 1700
#define PGTYP_OID 26

typedef struct {
    int32_t pg_type;
    int32_t pg_basetype;
    xsql_type_t type;
} xsql_pg_type_t;

typedef struct {
    int len;
    xsql_pg_type_t types [0];
} type_array_t;

typedef struct {
    PGresult *stmt_prep;
    int recno;
    char *name;
} pg_t;

const char *get_provider_name () {
    return "postgres";
}

char *get_placeholder (int n) {
    char *ret = malloc(24);
    snprintf(ret, 24, "$%d", n);
    return ret;
}

char templ ['9'-'0'+1+'Z'-'A'+1+'z'-'a'+1];

void init_templ () __attribute__ ((constructor));

void init_templ () {
    char *p = templ;
    char c = '0';
    while (c <= '9') { *p++ = c++; };
    c = 'A';
    while (c <= 'Z') { *p++ = c++; };
    c = 'a';
    while (c <= 'z') { *p++ = c++; };
    srand(time(0));
}

#define NAMLEN 8
#define TIMLEN 23
char *get_randnam () {
    char *ret = malloc(NAMLEN + TIMLEN);
    for (int i = 0; i < NAMLEN; ++i)
        ret[i] = templ[rand() % sizeof(templ)];
    snprintf(ret + NAMLEN, TIMLEN, "%lu", time(0));
    return ret;
}

static xsql_type_t pg2xsql_type (int32_t pg_type) {
    switch (pg_type) {
        case PGTYP_INT2:
        case PGTYP_INT4:
        case PGTYP_INT8:
        case PGTYP_OID:
            return TYP_INT;
        case PGTYP_FLOAT4:
        case PGTYP_FLOAT8:
            return TYP_FLOAT;
        default:
            return TYP_STRING;
    }
}

static xsql_pg_type_t *find_type (type_array_t *ta, int32_t search_type) {
    int l = 0, r = ta->len - 1, found_idx = 0;
    xsql_pg_type_t *ret = NULL;
    while (l <= r) {
        found_idx = (l + r) / 2;
        if (search_type > ta->types[found_idx].pg_type)
            l = found_idx + 1;
        else
        if (search_type < ta->types[found_idx].pg_type)
            r = found_idx - 1;
        else {
            ret = &ta->types[found_idx];
            break;
        }
    }
    if (ret && 0 != ret->pg_basetype)
        return find_type(ta, ret->pg_basetype);
    return ret;
}

static xsql_type_t pgbase2xsql_type (type_array_t *ta, int32_t pg_basetype) {
    xsql_pg_type_t *t = find_type(ta, pg_basetype);
    if (t)
        return pg2xsql_type(t->pg_type);
    return TYP_STRING;
}

static void get_database_types (xsql_conn_t *conn) {
    PGresult *res = PQexecParams((PGconn*)conn->hdb, SQL_DBTYPES, 0, NULL, NULL, NULL, NULL, 1);
    if (PGRES_TUPLES_OK == PQresultStatus(res)) {
        int nrecs = PQntuples(res);
        type_array_t *ta = conn->data = malloc(sizeof(int) + nrecs * sizeof(xsql_pg_type_t));
        ta->len = nrecs;
        for (int i = 0; i < nrecs; ++i) {
            ta->types[i].pg_type = be32toh(*((int32_t*)PQgetvalue(res, i, 0)));
            ta->types[i].pg_basetype = be32toh(*((int32_t*)PQgetvalue(res, i, 1)));
        }
        for (int i = 0; i < nrecs; ++i) {
            if (0 == ta->types[i].pg_basetype)
                ta->types[i].type = pg2xsql_type(ta->types[i].pg_type);
            else
                ta->types[i].type = pgbase2xsql_type(ta, ta->types[i].pg_basetype);
        }
    }
    PQclear(res);
}

xsql_conn_t *dbopen (xsql_dbprov_t *provider, const char *name, size_t name_len, const char *conninfo, size_t conninfo_len) {
    xsql_conn_t *ret = calloc(1, sizeof(xsql_conn_t));
    PGconn *conn = PQconnectdb(conninfo);
    ret->hdb = (xsql_dbconn_t*)conn;
    ret->provider = provider;
    ret->name = mkstr(name, name_len, 8);
    if (CONNECTION_OK != PQstatus(conn))
        ret->msg = strdup(PQerrorMessage(conn));
    else
        get_database_types(ret);
    return ret;
}

xsql_stmt_t *sqlprepare (xsql_conn_t *conn, xsql_stmt_t *curstmt, const char *cmd, size_t cmd_len, size_t param_count) {
    if (curstmt) {
        ((pg_t*)curstmt->data)->recno = 0;
        return curstmt;
    }
    xsql_stmt_t *ret = calloc(1, sizeof(xsql_stmt_t));
    ret->data = calloc(1, sizeof(pg_t));
    ((pg_t*)ret->data)->name = get_randnam();
    PGresult *res = PQprepare((PGconn*)conn->hdb, ((pg_t*)ret->data)->name, cmd, param_count, NULL);
    ((pg_t*)ret->data)->stmt_prep = res;
    ret->conn = conn;
    if (PGRES_COMMAND_OK != PQresultStatus(res))
        ret->msg = strdup(PQerrorMessage((PGconn*)conn->hdb));
    return ret;
}

int fetch (xsql_stmt_t *stmt) {
    if (((pg_t*)stmt->data)->recno >= stmt->nrecs) {
        stmt->ret = XSQL_FIN;
        return XSQL_FIN;
    }
    for (int i = 0; i < stmt->nflds; ++i) {
        if (PQgetisnull((PGresult*)stmt->hstmt, ((pg_t*)stmt->data)->recno, i))
            stmt->result[i] = NULL;
        else
            stmt->result[i] = PQgetvalue((PGresult*)stmt->hstmt, ((pg_t*)stmt->data)->recno, i);
    }
    ++((pg_t*)stmt->data)->recno;
    stmt->ret = XSQL_OK;
    return XSQL_OK;
}

static int get_stmt_result (xsql_stmt_t *stmt) {
    PGresult* res = (PGresult*)stmt->hstmt;
    switch (PQresultStatus(res)) {
        case PGRES_TUPLES_OK:
            stmt->nrecs = PQntuples(res);
            if (!stmt->fldnams) {
                stmt->nflds = PQnfields(res);
                stmt->result = malloc(stmt->nflds * sizeof(char*));
                stmt->fldnams = malloc(stmt->nflds * sizeof(str_t*));
                stmt->types = malloc(stmt->nflds * sizeof(xsql_type_t));
                for (int i = 0; i < stmt->nflds; ++i) {
                    char *fldnam = PQfname(res, i);
                    if (fldnam)
                        stmt->fldnams[i] = mkstr(fldnam, strlen(fldnam), 8);
                    else
                        stmt->fldnams[i] = mkstr(CONST_STR_LEN("?column?"), 8);
                    xsql_pg_type_t *t = find_type((type_array_t*)stmt->conn->data, PQftype(res, i));
                    if (t) stmt->types[i] = t->type; else stmt->types[i] = TYP_STRING;
                }
                if (0 < stmt->nrecs) {
                    for (int i = 0; i < stmt->nflds; ++i)
                        stmt->result[i] = PQgetvalue(res, ((pg_t*)stmt->data)->recno, i);
                    ++((pg_t*)stmt->data)->recno;
                    stmt->ret = XSQL_OK;
                    return XSQL_OK;
                }
            } else
            if (0 < stmt->nrecs)
                return fetch(stmt);
            stmt->ret = XSQL_FIN;
            return XSQL_FIN;
        case PGRES_COMMAND_OK:
            stmt->ret = XSQL_FIN;
            return XSQL_FIN;
        default:
            stmt->msg = strdup(PQerrorMessage((PGconn*)stmt->conn->hdb));
            stmt->ret = XSQL_ERROR;
            return XSQL_ERROR;
    }
}

typedef struct {
    char **param_vals;
    int *param_free;
    size_t len;
} param_struct_t;

static void fill_params (list_t *params, param_struct_t *result) {
    result->len = params->len;
    result->param_vals = malloc(sizeof(char*) * params->len);
    result->param_free = malloc(params->len * sizeof(int));
    list_item_t *li = params->head;
    int param_no = 0;
    do {
        xsql_var_t *var = (xsql_var_t*)li->ptr;
        result->param_free[param_no] = 0;
        if (TYP_STRING == var->typ) {
            if (var->raw.s_val->ptr)
                result->param_vals[param_no] = var->raw.s_val->ptr;
            else
                result->param_vals[param_no] = NULL;
        } else
        if (TYP_NONE == var->typ)
            result->param_vals[param_no] = NULL;
        else
        if (var->raw.s_val) {
            result->param_vals[param_no] = xsql_varstrdup(var);
            result->param_free[param_no] = 1;
        } else
            result->param_vals[param_no] = NULL;
        li = li->next;
        ++param_no;
    } while (li != params->head);
}

static void free_params (param_struct_t *p) {
    for (int param_no = 0; param_no < p->len; ++param_no)
        if (p->param_free[param_no])
            free(p->param_vals[param_no]);
    free(p->param_vals);
    free(p->param_free);
}

int sqlexec (xsql_stmt_t *stmt, list_t *params) {
    param_struct_t p;
    memset(&p, 0, sizeof(p));
    if (params->len > 0)
        fill_params(params, &p);
    PGresult *res = PQexecPrepared((PGconn*)stmt->conn->hdb, ((pg_t*)stmt->data)->name, params->len, (const char * const*)p.param_vals, NULL, NULL, 0);
    stmt->hstmt = (xsql_dbstmt_t*)res;
    if (p.param_vals)
        free_params(&p);
    return get_stmt_result(stmt);
}

int sqlexec_direct (xsql_conn_t *conn, xsql_stmt_t **curstmt, const char *cmd, size_t cmd_len, list_t *params) {
    PGresult *res;
    xsql_stmt_t *ret = *curstmt = calloc(1, sizeof(xsql_stmt_t));
    ret->data = calloc(1, sizeof(pg_t));
    if (params->len == 0) {
        res = PQexec((PGconn*)conn->hdb, cmd);
    } else {
        param_struct_t p;
        memset(&p, 0, sizeof(p));
        fill_params(params, &p);
        res = PQexecParams((PGconn*)conn->hdb, cmd, params->len, NULL, (const char * const*)p.param_vals, NULL, NULL, 0);
        free_params(&p);
    }
    ret->hstmt = (xsql_dbstmt_t)res;
    ret->conn = conn;
    return get_stmt_result(ret);
}

void sqlclose (xsql_stmt_t *stmt) {
    if (((pg_t*)stmt->data)->name)
        free(((pg_t*)stmt->data)->name);
    if (stmt->result)
        free(stmt->result);
    if (stmt->fldnams) {
        for (int i = 0; i < stmt->nflds; ++i)
            free(stmt->fldnams[i]);
        free(stmt->fldnams);
    }
    if (stmt->msg)
        free(stmt->msg);
    if (stmt->types)
        free(stmt->types);
    if (((pg_t*)stmt->data)->stmt_prep)
        PQclear(((pg_t*)stmt->data)->stmt_prep);
    PQclear((PGresult*)stmt->hstmt);
    free(stmt->data);
    free(stmt);
}

void dbclose (xsql_conn_t *conn) {
    free(conn->name);
    if (conn->data)
        free(conn->data);
    if (conn->hdb)
        PQfinish((PGconn*)conn->hdb);
    if (conn->msg)
        free(conn->msg);
    free(conn);
}
