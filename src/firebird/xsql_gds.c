#include <stdlib.h>
#include <time.h>
#include <ibase.h>
#include "db.h"

#if __x86_64 || __ppc64__
    #define ISC_INT64_FORMAT "l"
#else
    #define ISC_INT64_FORMAT "ll"
#endif
typedef PARAMVARY VARY2;

#define SHORT_SIZE 6
#define LONG_SIZE 11
#define INT64_SIZE 21
#define FLOAT_SIZE 15
#define DOUBLE_SIZE 24
#define TIMESTAMP_SIZE 24
#define DATE_SIZE 13
#define TIME_SIZE 10
#define BLOB_SIZE 17

typedef enum { TR_NO, TR_START, TR_COMMIT, TR_ROLLBACK } tr_what_t;

typedef struct {
    isc_tr_handle tr;
    int is_auto_transaction;
    tr_what_t tr_what;
} gds_db_t;

typedef struct {
    XSQLDA *in_sqlda;
    XSQLDA *out_sqlda;
    str_t **in_data;
    char **out_data;
    short *in_ind;
    short *out_ind;
    short length;
    int statement_type;
    char **result;
    int is_query;
} gds_stmt_t;

const char *get_provider_name () {
    return "firebird";
}

char *get_placeholder (int n) {
    return strdup("?");
}

void get_error (ISC_STATUS_ARRAY status, xsql_stmt_t *stmt) {
    long sqlcode = isc_sqlcode(status);
    char buf [512];
    isc_sql_interprete(sqlcode, buf, sizeof buf);
    stmt->msg = strdup(buf);
}

xsql_conn_t *dbopen (xsql_dbprov_t *prov, const char *name, size_t name_len, const char *conninfo, size_t conninfo_len) {
    ISC_STATUS_ARRAY status;
    ISC_STATUS ret;
    xsql_conn_t *conn = calloc(1, sizeof(xsql_conn_t));
    char *host = NULL, *dbname = NULL, *user = NULL, *pass = NULL, *dpb = NULL, buf [512], *p_dpb = NULL;
    short dpb_len = 0, dpb_size;
    conn->name = mkstr(name, name_len, 8);
    conn->provider = prov;
    if (-1 == parse_conninfo((char*)conninfo, conninfo_len, &host, &dbname, &user, &pass) || !dbname || !user || !pass) {
        snprintf(buf, sizeof buf, "conection string error: %s", conninfo);
        conn->msg = strdup(buf);
        return conn;
    }
    str_t *host_dbname = stralloc(64, 64);
    if (host) {
        strnadd(&host_dbname, host, strlen(host));
        strnadd(&host_dbname, CONST_STR_LEN(":"));
    }
    strnadd(&host_dbname, dbname, strlen(dbname));
    size_t user_len = strlen(user), pass_len = strlen(pass);
    dpb_size = user_len + pass_len + 24;
    p_dpb = dpb = malloc(dpb_size);
    dpb_len = 1;
    *dpb = isc_dpb_version1;
    isc_modify_dpb(&dpb, &dpb_len, isc_dpb_user_name, user, user_len);
    isc_modify_dpb(&dpb, &dpb_len, isc_dpb_password, pass, pass_len);
    if (0 != (ret = isc_attach_database(status, host_dbname->len, host_dbname->ptr, (isc_db_handle*)&conn->hdb, dpb_len, dpb)))
        conn->msg = strdup("connection error");
    conn->data = calloc(1, sizeof(gds_db_t));
    if (host_dbname) free(host_dbname);
    if (p_dpb) free(p_dpb);
    if (pass) free(pass);
    if (user) free(user);
    if (dbname) free(dbname);
    if (host) free(host);
    return conn;
}

static void set_in_sqlda (gds_stmt_t *gds) {
    XSQLDA *in_sqlda = gds->in_sqlda;
    gds->in_data = calloc(in_sqlda->sqld, sizeof(str_t*));
    gds->in_ind = calloc(in_sqlda->sqld, sizeof(short));
    for (int i = 0; i < in_sqlda->sqld; ++i) {
        in_sqlda->sqlvar[i].sqltype = SQL_TEXT;
        in_sqlda->sqlvar[i].sqlind = &gds->in_ind[i];
    }
}

static void set_params (gds_stmt_t *gds, list_t *params) {
    int i = 0;
    list_item_t *li = params->head;
    XSQLDA *in_sqlda = gds->in_sqlda;
    if (!li) return;
    do {
        xsql_var_t *var = (xsql_var_t*)li->ptr;
        str_t *str = gds->in_data[i];
        switch (var->typ) {
            case TYP_NONE:
                gds->in_ind[i] = 1;
                break;
            case TYP_INT:
                if (!str)
                    gds->in_data[i] = str = stralloc(24, 8);
                str->len = snprintf(str->ptr, str->bufsize, LONG_FMT, var->raw.i_val);
                STR_ADD_NULL(str);
                gds->in_ind[i] = 0;
                break;
            case TYP_FLOAT:
                if (!str)
                    gds->in_data[i] = str = stralloc(32, 8);
                str->len = snprintf(str->ptr, str->bufsize, "%f", var->raw.f_val);
                STR_ADD_NULL(str);
                gds->in_ind[i] = 0;
                break;
            case TYP_STRING:
                if (str)
                    strput(&str, var->raw.s_val->ptr, var->raw.s_val->len, 0);
                else
                    str = mkstr(var->raw.s_val->ptr, var->raw.s_val->len, 32);
                gds->in_data[i] = str;
                gds->in_ind[i] = 0;
                break;
            default: break; // TYP_BOOL - ?
        }
        in_sqlda->sqlvar[i].sqldata = gds->in_data[i]->ptr;
        in_sqlda->sqlvar[i].sqllen = gds->in_data[i]->len;
        ++i;
        li = li->next;
    } while (li != params->head);
}

static void set_out_sqlda (xsql_stmt_t *stmt) {
    gds_stmt_t *gds = (gds_stmt_t*)stmt->data;
    XSQLDA *out_sqlda = gds->out_sqlda;
    stmt->result = calloc(out_sqlda->sqld, sizeof(char*));
    stmt->nflds = out_sqlda->sqld;
    stmt->fldnams = calloc(stmt->nflds, sizeof(str_t*));
    gds->out_data = calloc(stmt->nflds, sizeof(char*));
    gds->out_ind = calloc(out_sqlda->sqld, sizeof(short));
    gds->result = malloc(stmt->nflds * sizeof(char*));
    stmt->types = malloc(stmt->nflds * sizeof(xsql_type_t));
    for (int i = 0; i < out_sqlda->sqld; ++i) {
        XSQLVAR *sqlvar = &out_sqlda->sqlvar[i];
        short dtype = sqlvar->sqltype & ~1;
        gds->out_data[i] = malloc(sizeof(short) + out_sqlda->sqlvar[i].sqllen + 1);
        out_sqlda->sqlvar[i].sqldata = (char*)gds->out_data[i];
        out_sqlda->sqlvar[i].sqlind = &gds->out_ind[i];
        stmt->fldnams[i] = mkstr(out_sqlda->sqlvar[i].sqlname, out_sqlda->sqlvar[i].sqlname_length, 8);
        switch (dtype) {
            case SQL_TEXT:
            case SQL_VARYING:
                gds->result[i] = malloc(sqlvar->sqllen + 1);
                stmt->types[i] = TYP_STRING;
                break;
            case SQL_SHORT:
                gds->result[i] = malloc(SHORT_SIZE + 1);
                stmt->types[i] = TYP_INT;
                break;
            case SQL_LONG:
                gds->result[i] = malloc(LONG_SIZE + 1);
                stmt->types[i] = TYP_INT;
                break;
            case SQL_INT64:
                gds->result[i] = malloc(INT64_SIZE + 1);
                stmt->types[i] = TYP_INT;
                break;
            case SQL_FLOAT:
                gds->result[i] = malloc(FLOAT_SIZE + 1);
                stmt->types[i] = TYP_FLOAT;
                break;
            case SQL_DOUBLE:
                gds->result[i] = malloc(DOUBLE_SIZE + 1);
                stmt->types[i] = TYP_FLOAT;
                break;
            case SQL_TIMESTAMP:
                gds->result[i] = malloc(TIMESTAMP_SIZE + 1);
                stmt->types[i] = TYP_STRING; // TODO : TYP_TIMESTAMP
                break;
            case SQL_TYPE_DATE:
                gds->result[i] = malloc(DATE_SIZE + 1);
                stmt->types[i] = TYP_STRING; // TODO : TYP_DATE
                break;
            case SQL_TYPE_TIME:
                gds->result[i] = malloc(TIME_SIZE + 1);
                stmt->types[i] = TYP_STRING; // TODO : TYP_TIME
                break;
            case SQL_BLOB:
            case SQL_ARRAY:
            default:
                gds->result[i] = malloc(BLOB_SIZE + 1);
                stmt->types[i] = TYP_STRING; // TODO :
                break;
        }
    }
}

static ISC_STATUS start_transaction (ISC_STATUS_ARRAY status, xsql_conn_t *conn, xsql_stmt_t *stmt, int is_auto) {
    gds_db_t *gds_db = (gds_db_t*)conn->data;
    if (gds_db->tr) {
        if (TR_NO == gds_db->tr_what && is_auto) {
            isc_commit_transaction(status, &gds_db->tr);
            return isc_start_transaction(status, &gds_db->tr, 1, (isc_db_handle*)&conn->hdb, 0, NULL);
        }
    } else
        return isc_start_transaction(status, &gds_db->tr, 1, (isc_db_handle*)&conn->hdb, 0, NULL);
    return 0;
}

static ISC_STATUS commit (ISC_STATUS_ARRAY status, xsql_conn_t *conn, xsql_stmt_t *stmt, int is_auto) {
    gds_db_t *gds_db = (gds_db_t*)conn->data;
    if (gds_db->tr && gds_db->tr_what == TR_NO)
        return isc_commit_transaction(status, (isc_tr_handle*)&conn->data);
    return 0;
}

static int rollback (ISC_STATUS_ARRAY status, xsql_conn_t *conn, xsql_stmt_t *stmt, int is_auto) {
    gds_db_t *gds_db = (gds_db_t*)conn->data;
    if (gds_db->tr && gds_db->tr_what == TR_NO)
        return isc_rollback_transaction(status, (isc_tr_handle*)&conn->data);
    return 0;
}

static ISC_STATUS sqlquery_prepare (ISC_STATUS_ARRAY status, xsql_stmt_t *stmt, const char *cmd, size_t cmd_len, size_t param_count) {
    ISC_STATUS ret = 0;
    gds_stmt_t *gds_stmt = (gds_stmt_t*)stmt->data;
    xsql_conn_t *conn = stmt->conn;
    gds_db_t *gds_db = (gds_db_t*)conn->data;
    size_t dummy_param_count = param_count > 0 ? param_count : 1;
    XSQLDA *in_sqlda = gds_stmt->in_sqlda = malloc(XSQLDA_LENGTH(dummy_param_count)),
           *out_sqlda = gds_stmt->out_sqlda = malloc(XSQLDA_LENGTH(20));
    memset(in_sqlda, 0, XSQLDA_LENGTH(dummy_param_count));
    in_sqlda->sqln = dummy_param_count;
    in_sqlda->sqld = param_count;
    out_sqlda->sqln = 20;
    in_sqlda->version = out_sqlda->version = SQLDA_VERSION1;
    gds_stmt->is_query = 1;
    if (0 == (ret = isc_dsql_allocate_statement(status, (isc_db_handle*)&conn->hdb, (isc_stmt_handle*)&stmt->hstmt)) &&
        0 == (ret = start_transaction(status, conn, stmt, 1)) &&
        0 == (ret = isc_dsql_prepare(status, &gds_db->tr, (isc_stmt_handle*)&stmt->hstmt, cmd_len, cmd, SQL_DIALECT_V6, gds_stmt->out_sqlda))) {
        if (param_count != in_sqlda->sqld) {
            stmt->msg = strdup("parameter count error");
            stmt->ret = XSQL_ERROR;
        } else {
            if (in_sqlda->sqld > 0) {
                if (in_sqlda->sqld > in_sqlda->sqln) {
                    in_sqlda = realloc(in_sqlda, XSQLDA_LENGTH(in_sqlda->sqld));
                    if (in_sqlda)
                        gds_stmt->in_sqlda = in_sqlda;
                }
                set_in_sqlda(gds_stmt);
            }
            if (out_sqlda->sqld > 0) {
                if (out_sqlda->sqld > out_sqlda->sqln) {
                    out_sqlda = realloc(out_sqlda, XSQLDA_LENGTH(out_sqlda->sqld));
                    if (out_sqlda)
                        gds_stmt->out_sqlda = out_sqlda;
                }
                set_out_sqlda(stmt);
            }
        }
    }
    return ret;
}

xsql_stmt_t *sqlprepare (xsql_conn_t *conn, xsql_stmt_t *curstmt, const char *cmd, size_t cmd_len, size_t param_count) {
    ISC_STATUS ret = 0;
    ISC_STATUS_ARRAY status;
    if (curstmt)
        return curstmt;
    xsql_stmt_t *stmt = calloc(1, sizeof(xsql_stmt_t));
    stmt->data = calloc(1, sizeof(gds_stmt_t));
    gds_db_t *gds_db = (gds_db_t*)conn->data;
    tr_what_t tr_what = TR_NO;
    stmt->conn = conn;
    if (0 == cmpcasestr(cmd, cmd_len, CONST_STR_LEN("START TRANSACTION")))
        tr_what = TR_START;
    else
    if (0 == cmpcasestr(cmd, cmd_len, CONST_STR_LEN("COMMIT")))
        tr_what = TR_COMMIT;
    else
    if (0 == cmpcasestr(cmd, cmd_len, CONST_STR_LEN("ROLLBACK")))
        tr_what = TR_ROLLBACK;
    else
        ret = sqlquery_prepare(status, stmt, cmd, cmd_len, param_count);
    if (tr_what != TR_NO) {
        if (gds_db->tr_what != TR_NO)
            return stmt;
        gds_db->tr_what = tr_what;
    }
    if (0 != ret) {
        get_error(status, stmt);
        stmt->ret = XSQL_ERROR;
        return stmt;
    }
    stmt->ret = XSQL_OK;
    return stmt;
}

static void set_value (XSQLVAR *var, gds_stmt_t *gds, int index, ISC_INT64 value, short field_width) {
    short dscale = var->sqlscale;
    if (dscale < 0) {
        ISC_INT64 tens = 1;
        for (short i = 0; i > dscale; i--)
            tens *= 10;
        if (value >= 0)
            sprintf(gds->result[index], "%lld.%0*lld", (ISC_INT64)value / tens, -dscale, (ISC_INT64)value % tens);
        else
        if (0 != (value / tens))
            sprintf(gds->result[index], "%lld.%0*lld", (ISC_INT64)(value / tens), -dscale, (ISC_INT64)-(value % tens));
        else
            sprintf(gds->result[index], "%s.%0*lld", "-0", -dscale, (ISC_INT64)-(value % tens));
    } else
    if (dscale)
        sprintf(gds->result[index], "%lld%0*d", (ISC_INT64)value, dscale, 0);
    else
        sprintf(gds->result[index], "%lld", (ISC_INT64)value);
}

static void get_stmt_result (xsql_stmt_t *stmt) {
    gds_stmt_t *gds = (gds_stmt_t*)stmt->data;
    for (int i = 0; i < stmt->nflds; ++i) {
        XSQLVAR *var = &gds->out_sqlda->sqlvar[i];
        VARY2 *vary2;
        ISC_INT64 value;
        ISC_QUAD bid;
        struct tm times;
        short dtype = var->sqltype & ~1;
        if ((var->sqltype & 1) && (*var->sqlind < 0)) {
            stmt->result[i] = NULL;
            continue;
        }
        switch (dtype) {
            case SQL_TEXT:
                strncpy(gds->result[i], var->sqldata, var->sqllen);
                break;
            case SQL_VARYING:
                vary2 = (VARY2*)var->sqldata;
                strncpy(gds->result[i], (const char*)vary2->vary_string, vary2->vary_length);
                gds->result[i][vary2->vary_length] = '\0';
                break;
            case SQL_SHORT:
                value = (ISC_INT64)*(short*)var->sqldata;
                set_value(var, gds, i, value, SHORT_SIZE);
                break;
            case SQL_LONG:
                value = (ISC_INT64)*(int*)var->sqldata;
                set_value(var, gds, i, value, LONG_SIZE);
                break;
            case SQL_INT64:
                value = (ISC_INT64)*(ISC_INT64*)var->sqldata;
                set_value(var, gds, i, value, INT64_SIZE);
                break;
            case SQL_FLOAT:
                sprintf(gds->result[i], "%g", *(float*)var->sqldata);
                break;
            case SQL_DOUBLE:
                sprintf(gds->result[i], "%f", *(double*)var->sqldata);
                break;
            case SQL_TIMESTAMP:
                isc_decode_timestamp((ISC_TIMESTAMP*)var->sqldata, &times);
                sprintf(gds->result[i], "%04d-%02d-%02d %02d:%02d:%02d.%04d", times.tm_year+1900, times.tm_mon+1, times.tm_mday,
                    times.tm_hour, times.tm_min, times.tm_sec, ((ISC_TIMESTAMP*)var->sqldata)->timestamp_time * 10000);
                break;
            case SQL_TYPE_DATE:
                isc_decode_sql_date((ISC_DATE*)var->sqldata, &times);
                sprintf(gds->result[i], "%04d-%02d-%02d", times.tm_year+1900, times.tm_mon+1, times.tm_mday);
                break;
            case SQL_TYPE_TIME:
                isc_decode_sql_time((ISC_TIME*)var->sqldata, &times);
                sprintf(gds->result[i], "%02d:%02d:%02d.%04d", times.tm_hour, times.tm_min, times.tm_sec, (*((ISC_TIME*)var->sqldata)) % 10000);
                break;
            case SQL_BLOB:
            case SQL_ARRAY:
                bid = *(ISC_QUAD*)var->sqldata;
                sprintf(gds->result[i], "%08x:%08x", bid.gds_quad_high, bid.gds_quad_low);
                break;
            default:
                strcpy(gds->result[i], "(unknown)");
                break;
        }
        stmt->result[i] = gds->result[i];
    }
}

int fetch (xsql_stmt_t *stmt) {
    ISC_STATUS_ARRAY status;
    int ret;
    gds_stmt_t *gds = (gds_stmt_t*)stmt->data;
    if (0 == (ret = isc_dsql_fetch(status, (isc_stmt_handle*)&stmt->hstmt, SQL_DIALECT_V6, gds->out_sqlda)))
        get_stmt_result(stmt);
    switch (ret) {
        case 0: ret = XSQL_OK; break;
        case 100: ret = XSQL_FIN; break;
        default:
            get_error(status, stmt);
            ret = XSQL_ERROR;
    }
    return ret;
}

static ISC_STATUS sqlexec_query (ISC_STATUS_ARRAY status, xsql_conn_t *conn, xsql_stmt_t *stmt, list_t *params) {
    ISC_STATUS ret;
    gds_stmt_t *gds_stmt = (gds_stmt_t*)stmt->data;
    gds_db_t *gds_db = (gds_db_t*)conn->data;
    XSQLDA *in_sqlda = gds_stmt->in_sqlda, *out_sqlda = gds_stmt->out_sqlda;
    set_params(gds_stmt, params);
    if (0 == out_sqlda->sqld) {
        if (0 == in_sqlda->sqld) {
            if (0 == (ret = isc_dsql_execute(status, &gds_db->tr, (isc_stmt_handle*)&stmt->hstmt, SQL_DIALECT_V6, NULL)))
                stmt->ret = XSQL_FIN;
        } else {
            if (0 == (ret = isc_dsql_execute2(status, &gds_db->tr, (isc_stmt_handle*)&stmt->hstmt, SQL_DIALECT_V6, in_sqlda, NULL)))
                stmt->ret = XSQL_FIN;
        }
    } else {
        if (0 == in_sqlda->sqld) {
            if (0 == (ret = isc_dsql_execute(status, &gds_db->tr, (isc_stmt_handle*)&stmt->hstmt, SQL_DIALECT_V6, out_sqlda)))
                stmt->ret = XSQL_OK;
        } else {
            if (0 == (ret = isc_dsql_execute2(status, &gds_db->tr, (isc_stmt_handle*)&stmt->hstmt, SQL_DIALECT_V6, in_sqlda, NULL)))
                stmt->ret = XSQL_OK;
            isc_dsql_set_cursor_name(status, (isc_stmt_handle*)&stmt->hstmt, "dyn_cursor", 0); // FIXME : set unique cursoe name .... may be
        }
    }
    return ret;
}

int sqlexec (xsql_stmt_t *stmt, list_t *params) {
    ISC_STATUS_ARRAY status;
    ISC_STATUS ret = 0;
    xsql_conn_t *conn = stmt->conn;
    gds_stmt_t *gds_stmt = (gds_stmt_t*)stmt->data;
    gds_db_t *gds_db = (gds_db_t*)conn->data;
    switch (gds_db->tr_what) {
        case TR_START:
            if (0 == (ret = start_transaction(status, conn, stmt, 0)))
                stmt->ret = XSQL_FIN;
            break;
        case TR_COMMIT:
            if (0 == (ret = commit(status, conn, stmt, 0)))
                stmt->ret = XSQL_FIN;
            break;
        case TR_ROLLBACK:
            if (0 == (ret = rollback(status, conn, stmt, 0)))
                stmt->ret = XSQL_FIN;
            break;
        default: break;
    }
    if (gds_stmt->is_query)
        ret = sqlexec_query(status, conn, stmt, params);
    if (0 != ret) {
        get_error(status, stmt);
        stmt->ret = XSQL_ERROR;
        return XSQL_ERROR;
    }
    if (stmt->ret == XSQL_FIN)
        return XSQL_FIN;
    return fetch(stmt);
}

int sqlexec_direct (xsql_conn_t *conn, xsql_stmt_t **curstmt, const char *cmd, size_t cmd_len, list_t *params) {
    xsql_stmt_t *stmt = *curstmt = sqlprepare(conn, *curstmt, cmd, cmd_len, params->len);
    if (!stmt->msg)
        return sqlexec(stmt, params);
    stmt->ret = XSQL_ERROR;
    return XSQL_ERROR;
}

void sqlclose (xsql_stmt_t *stmt) {
    ISC_STATUS_ARRAY status;
    gds_stmt_t *gds = (gds_stmt_t*)stmt->data;
    commit(status, stmt->conn, stmt, 1);
    if (gds) {
        if (gds->in_data) {
            for (int i = 0; i < gds->in_sqlda->sqld; ++i)
                if (gds->in_data[i]) free(gds->in_data[i]);
            free(gds->in_data);
        }
        if (gds->in_ind) free(gds->in_ind);
        if (gds->out_data) {
            for (int i = 0; i < gds->out_sqlda->sqld; ++i)
                free(gds->out_data[i]);
            free(gds->out_data);
        }
        if (gds->out_ind) free(gds->out_ind);
        if (gds->in_sqlda)
            free(gds->in_sqlda);
        if (gds->out_sqlda)
            free(gds->out_sqlda);
        for (int i = 0; i < stmt->nflds; ++i)
            free(gds->result[i]);
        free(gds->result);
        free(gds);
    }
    if (stmt->result)
        free(stmt->result);
    if (stmt->fldnams) {
        for (int i = 0; i < stmt->nflds; ++i)
            free(stmt->fldnams[i]);
        free(stmt->fldnams);
    }
    if (stmt->types)
        free(stmt->types);
    if (stmt->msg)
        free(stmt->msg);
//    isc_commit_transaction(status, (isc_tr_handle*)&stmt->conn->data);
    if (stmt->hstmt)
        isc_dsql_free_statement(status, (isc_stmt_handle*)&stmt->hstmt, DSQL_close);
    free(stmt);
}

void dbclose (xsql_conn_t *conn) {
    ISC_STATUS_ARRAY status;
    gds_db_t *gds_db = (gds_db_t*)conn->data;
    free(conn->name);
    if (gds_db) {
        if (gds_db->tr)
            isc_commit_transaction(status, &gds_db->tr);
        free(gds_db);
    }
    if (conn->hdb)
        isc_detach_database(status, (isc_db_handle*)&conn->hdb);
    if (conn->msg)
        free(conn->msg);
    free(conn);
}
