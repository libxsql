#include "fmt.h"

list_t *fmtprovs = NULL;
xsql_fmtprov_t *fmtstd_prov = NULL;

xsql_fmtprov_t *xsql_find_fmtprov (const char *name, size_t name_len) {
    if (!fmtprovs || !fmtprovs->head)
        return NULL;
    list_item_t *li = fmtprovs->head;
    do {
        xsql_fmtprov_t *prov = (xsql_fmtprov_t*)li->ptr;
        const char *nam = prov->fmtname();
        size_t len = strlen(nam);
        if (0 == cmpstr(nam, len, name, name_len))
            return prov;
        li = li->next;
    } while (li != fmtprovs->head);
    return NULL;
}

static char *load_fmtprovider (const char *libfname) {
    char *errmsg = NULL;
    void *hnd = dlopen(libfname, RTLD_LAZY);
    if (!hnd) {
        errmsg = dlerror();
        goto err0;
    }
    dlerror();
    xsql_fmtprov_t *prov = malloc(sizeof(xsql_fmtprov_t));
    prov->hnd = hnd;
    prov->fmtname = (xsql_fmt_name_h)dlsym(hnd, "fmtname");
    if ((errmsg = dlerror())) goto err;
    prov->fmtwrtname = (xsql_fmt_wrtname_h)dlsym(hnd, "fmtwrtname");
    if ((errmsg = dlerror())) goto err;
    prov->fmtopen = (xsql_fmt_open_h)dlsym(hnd, "fmtopen");
    if ((errmsg = dlerror())) goto err;
    prov->fmtwrtopen = (xsql_fmt_wrtopen_h)dlsym(hnd, "fmtwrtopen");
    if ((errmsg = dlerror())) goto err;
    prov->fmtstart_outrec = (xsql_fmt_start_outrec_h)dlsym(hnd, "fmtstart_outrec");
    if ((errmsg = dlerror())) goto err;
    prov->fmtoutrec = (xsql_fmt_outrec_h)dlsym(hnd, "fmtoutrec");
    if ((errmsg = dlerror())) goto err;
    prov->fmtend_outrec = (xsql_fmt_end_outrec_h)dlsym(hnd, "fmtend_outrec");
    if ((errmsg = dlerror())) goto err;
    prov->fmtstart_outvar = (xsql_fmt_start_outvar_h)dlsym(hnd, "fmtstart_outvar");
    if ((errmsg = dlerror())) goto err;
    prov->fmtoutvar = (xsql_fmt_outvar_h)dlsym(hnd, "fmtoutvar");
    if ((errmsg = dlerror())) goto err;
    prov->fmtwrtvar = (xsql_fmt_wrtvar_h)dlsym(hnd, "fmtwrtvar");
    if ((errmsg = dlerror())) goto err;
    prov->fmtwrtstr = (xsql_fmt_wrtstr_h)dlsym(hnd, "fmtwrtstr");
    if ((errmsg = dlerror())) goto err;
    prov->fmtend_outvar = (xsql_fmt_end_outvar_h)dlsym(hnd, "fmtend_outvar");
    if ((errmsg = dlerror())) goto err;
    prov->fmtwrtend = (xsql_fmt_wrtend_h)dlsym(hnd, "fmtwrtend");
    if ((errmsg = dlerror())) goto err;
    prov->fmtclose = (xsql_fmt_close_h)dlsym(hnd, "fmtclose");
    if ((errmsg = dlerror())) goto err;
    prov->fmtwrtclose = (xsql_fmt_wrtclose_h)dlsym(hnd, "fmtwrtclose");
    if ((errmsg = dlerror())) goto err;
    prov->fmtend = (xsql_fmt_end_h)dlsym(hnd, "fmtend");
    prov->fmtnewtab = (xsql_fmt_newtab_h)dlsym(hnd, "fmtnewtab");
    prov->fmtplot = (xsql_fmt_plot_h)dlsym(hnd, "fmtplot");
    prov->fmtsetbg = (xsql_fmt_set_bg_h)dlsym(hnd, "fmtsetbg");
    if ((errmsg = dlerror())) goto err;
    prov->fmtsetfg = (xsql_fmt_set_fg_h)dlsym(hnd, "fmtsetfg");
    if ((errmsg = dlerror())) goto err;
    prov->fmtsetstyle = (xsql_fmt_set_style_h)dlsym(hnd, "fmtsetstyle");
    prov->fmtread = (xsql_fmt_read_h)dlsym(hnd, "fmtread");
    if ((errmsg = dlerror())) goto err;
    lst_adde(fmtprovs, (void*)prov);
    return NULL;
err:
    dlclose(hnd);
    free(prov);
err0:
    return errmsg;
}

static void fmtprov_free (void *dummy, xsql_fmtprov_t *p) {
    if (p->hnd)
        dlclose(p->hnd);
    free(p);
}

str_t *xsql_fmtinit (const char *conf_str, size_t conf_str_len) {
    str_t *msgs = NULL;
    fmtprovs = lst_alloc((free_h)fmtprov_free);
    lst_adde(fmtprovs, (void*)xsql_create_text_provider());
    lst_adde(fmtprovs, (void*)xsql_create_json_provider());
    lst_adde(fmtprovs, (void*)xsql_create_bin_provider());
    strptr_t str, instr = { .ptr = (char*)conf_str, .len = conf_str_len };
    while (0 == strntok(&instr.ptr, &instr.len, CONST_STR_LEN(STR_SPACES ","), &str)) {
        char *fname = strndup(str.ptr, str.len),
             *msg = load_fmtprovider(fname);
        if (msg) {
            if (!msgs)
                msgs = mkstr(msg, strlen(msg), 64);
            else {
                strnadd(&msgs, CONST_STR_LEN("\n"));
                strnadd(&msgs, msg, strlen(msg));
            }
        }
        free(fname);
    }
    return msgs;
}

void xsql_fmtdone () {
    lst_free(fmtprovs);
}

int file_wrtopen (xsql_fmt_t *fmt) {
    xsql_text_t *txt = (xsql_text_t*)fmt;
    switch (txt->flags) {
        case XSQL_READ:
            txt->fd = open(txt->fname->ptr, O_RDONLY);
            break;
        case XSQL_WRITE:
            txt->fd = open(txt->fname->ptr, O_CREAT | O_TRUNC | O_WRONLY, 0644);
            break;
    }
    return txt->fd > 0 ? 0 : -1;
}

str_t *file_wrtname (const char *fname, size_t fname_len) {
    str_t *str = mkstr(fname, fname_len, 16);
    return str;
}

int file_wrtvar (xsql_fmt_t *fmt, xsql_var_t *var, size_t index) {
    const char *str;
    size_t str_len;
    if (TYP_STRING == var->typ) {
        str = (const char*)var->raw.s_val->ptr;
        str_len = var->raw.s_val->len;
    } else {
        str = xsql_varstr(var);
        str_len = strlen(str);
    }
    return str_len == write(((xsql_text_t*)fmt)->fd, str, str_len) ? 0 : -1;
}

int file_wrtstr (xsql_fmt_t *fmt, const char *str, size_t str_len, size_t index) {
    return str_len == write(((xsql_text_t*)fmt)->fd, str, str_len) ? 0 : -1;
}

void file_wrtclose (xsql_fmt_t *fmt) {
    xsql_text_t *txt = (xsql_text_t*)fmt;
    if (txt->fd > 0)
        close(txt->fd);
}

int file_wrtend (xsql_fmt_t *fmt) {
    return fmt->prov->fmtwrtstr(fmt, CONST_STR_LEN("\n"), 0);
}
