#include "plot.h"

static const char *gnuplot_formats [] = { "png", "postscript" };
static const char *gnuplot_styles [] = { "lines", "points", "impulses", "dots", "steps", "errorbars", "boxes", "boxeserrorbars" };

void gnuplot_perform (gnuplot_t *plot, str_t **result) {
    const char *str;
    FILE *file;
    if (!plot->cmd && !plot->stmt) return;
    if (plot->cmd && plot->stmt) return;
    str_t *cmd = mkstr(CONST_STR_LEN("gnuplot -e \""), 512);
    if (plot->xlabel) {
        strnadd(&cmd, CONST_STR_LEN("set xlabel \""));
        strnadd(&cmd, plot->xlabel->ptr, plot->xlabel->len);
        strnadd(&cmd, CONST_STR_LEN("\";"));
    }
    if (plot->ylabel) {
        strnadd(&cmd, CONST_STR_LEN("set ylabel \""));
        strnadd(&cmd, plot->ylabel->ptr, plot->ylabel->len);
        strnadd(&cmd, CONST_STR_LEN("\";"));
    }
    strnadd(&cmd, CONST_STR_LEN("set term "));
    str = gnuplot_formats[plot->format];
    strnadd(&cmd, str, strlen(str));
    strnadd(&cmd, CONST_STR_LEN(";"));
    strnadd(&cmd, CONST_STR_LEN("set output \""));
    strnadd(&cmd, plot->out_fname->ptr, plot->out_fname->len);
    strnadd(&cmd, CONST_STR_LEN("\";"));
    strnadd(&cmd, CONST_STR_LEN("plot "));
    if (plot->cmd)
        strnadd(&cmd, plot->cmd->ptr, plot->cmd->len);
    else
        strnadd(&cmd, CONST_STR_LEN("'-'"));
    strnadd(&cmd, CONST_STR_LEN(" with "));
    str = gnuplot_styles[plot->style];
    strnadd(&cmd, str, strlen(str));
    strnadd(&cmd, CONST_STR_LEN("\""));
    if ((file = popen(cmd->ptr, "w"))) {
        ssize_t readed;
        char buf [1024];
        if (plot->stmt) {
            xsql_stmt_t *stmt = plot->stmt;
            xsql_fetch_h fetch = stmt->conn->provider->fetch;
            while (XSQL_OK == fetch(stmt)) {
                for (int i = 0; i < stmt->nflds; ++i) {
                    fputs(stmt->result[i], file);
                    fputs("\t", file);
                }
                fputs("\n", file);
            }
            fputs("e", file);
        }
        fflush(file);
        while (0 < (readed = fread(buf, sizeof(buf), 1, file)))
            strnadd(result, buf, readed);
        pclose(file);
    }
    free(cmd);
}
