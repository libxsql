#include "var.h"

xsql_var_t xsql_var_init (xsql_type_t typ, xsql_raw_t *raw, xsql_var_free_h on_free) {
    xsql_var_t r;
    r.name = NULL;
    r.typ = typ;
    r.on_free = on_free;
    memcpy(&r.raw, raw, sizeof(xsql_raw_t));
    return r;
}
void xsql_var_free_str (xsql_var_t *v) {
    free(v->raw.s_val);
    v->on_free = NULL;
    v->raw.s_val = NULL;
};

__thread char var_str_buf [32];
const char *xsql_varstr (xsql_var_t *v) {
    var_str_buf[0] = '\0';
    const char *ret = var_str_buf;
    switch (v->typ) {
        case TYP_NONE: strcpy(var_str_buf, "null"); break;
        case TYP_INT: snprintf(var_str_buf, sizeof(var_str_buf), FMT_INT, v->raw.i_val); break;
        case TYP_FLOAT: snprintf(var_str_buf, sizeof(var_str_buf), "%f", v->raw.f_val); break;
        case TYP_STRING: if (v->raw.s_val) ret = (const char*)v->raw.s_val->ptr; else strcpy(var_str_buf, "null"); break;
        default: break;
    }
    return ret;
}

#define VAR_INT_STR_LEN 24
#define VAR_FLOAT_STR_LEN 32
char *xsql_varstrdup (xsql_var_t *v) {
    char *ret = NULL;
    switch (v->typ) {
        case TYP_INT:
            ret = malloc(VAR_INT_STR_LEN);
            snprintf(ret, VAR_INT_STR_LEN, FMT_INT, v->raw.i_val);
            break;
        case TYP_FLOAT:
            ret = malloc(VAR_FLOAT_STR_LEN);
            snprintf(ret, VAR_FLOAT_STR_LEN, "%f", v->raw.f_val);
            break;
        case TYP_STRING:
            if (v->raw.s_val)
                ret = strndup(v->raw.s_val->ptr, v->raw.s_val->len);
            else
                ret = strdup("null");
            break;
        case TYP_NONE:
            ret = strdup("null");
            break;
        default: break;
    }
    return ret;
}

const char *xsql_typstr (xsql_type_t typ) {
    switch (typ) {
        case TYP_INT: strncpy(var_str_buf, "int", sizeof(var_str_buf)); break;
        case TYP_FLOAT: strncpy(var_str_buf, "float", sizeof(var_str_buf)); break;
        case TYP_STRING: strncpy(var_str_buf, "string", sizeof(var_str_buf)); break;
        case TYP_BOOL: strncpy(var_str_buf, "bool", sizeof(var_str_buf)); break;
        default: strncpy(var_str_buf, "(null)", sizeof(var_str_buf)); break;
    }
    return (const char*)var_str_buf;
}

xsql_type_t xsql_max_type (xsql_type_t x, xsql_type_t y) {
    if (TYP_FLOAT == x || TYP_FLOAT == y) return TYP_FLOAT;
    return x;
}

// int,int
xsql_var_t xsql_var_int_add_int (xsql_var_t x, xsql_var_t y) {
    xsql_int_t z = x.raw.i_val + y.raw.i_val;
    return xsql_var_init(TYP_INT, (void*)&z, NULL);
}

xsql_var_t xsql_var_int_sub_int (xsql_var_t x, xsql_var_t y) {
    xsql_int_t z = x.raw.i_val - y.raw.i_val;
    return xsql_var_init(TYP_INT, (void*)&z, NULL);
}

xsql_var_t xsql_var_int_mul_int (xsql_var_t x, xsql_var_t y) {
    xsql_int_t z = x.raw.i_val * y.raw.i_val;
    return xsql_var_init(TYP_INT, (void*)&z, NULL);
}

xsql_var_t xsql_var_int_div_int (xsql_var_t x, xsql_var_t y) {
    xsql_int_t z = x.raw.i_val / y.raw.i_val;
    return xsql_var_init(TYP_INT, (void*)&z, NULL);
}

xsql_var_t xsql_var_int_eq_int (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.i_val == y.raw.i_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_int_neq_int (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.i_val != y.raw.i_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_int_gt_int (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.i_val > y.raw.i_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_int_lt_int (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.i_val < y.raw.i_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_int_ge_int (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.i_val >= y.raw.i_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_int_le_int (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.i_val <= y.raw.i_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

// int,float
xsql_var_t xsql_var_int_add_float (xsql_var_t x, xsql_var_t y) {
    xsql_float_t z = x.raw.i_val + y.raw.f_val;
    return xsql_var_init(TYP_FLOAT, (void*)&z, NULL);
}

xsql_var_t xsql_var_int_sub_float (xsql_var_t x, xsql_var_t y) {
    xsql_float_t z = x.raw.i_val - y.raw.f_val;
    return xsql_var_init(TYP_FLOAT, (void*)&z, NULL);
}

xsql_var_t xsql_var_int_mul_float (xsql_var_t x, xsql_var_t y) {
    xsql_float_t z = x.raw.i_val * y.raw.f_val;
    return xsql_var_init(TYP_FLOAT, (void*)&z, NULL);
}

xsql_var_t xsql_var_int_div_float (xsql_var_t x, xsql_var_t y) {
    xsql_float_t z = x.raw.i_val / y.raw.f_val;
    return xsql_var_init(TYP_FLOAT, (void*)&z, NULL);
}

xsql_var_t xsql_var_int_eq_float (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.i_val == y.raw.f_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_int_neq_float (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.i_val != y.raw.f_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_int_gt_float (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.i_val > y.raw.f_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_int_lt_float (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.i_val < y.raw.f_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_int_ge_float (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.i_val >= y.raw.f_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_int_le_float (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.i_val <= y.raw.f_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

// float,int
xsql_var_t xsql_var_float_add_int (xsql_var_t x, xsql_var_t y) {
    xsql_float_t z = x.raw.f_val + y.raw.i_val;
    return xsql_var_init(TYP_FLOAT, (void*)&z, NULL);
}

xsql_var_t xsql_var_float_sub_int (xsql_var_t x, xsql_var_t y) {
    xsql_float_t z = x.raw.f_val - y.raw.i_val;
    return xsql_var_init(TYP_FLOAT, (void*)&z, NULL);
}

xsql_var_t xsql_var_float_mul_int (xsql_var_t x, xsql_var_t y) {
    xsql_float_t z = x.raw.f_val * y.raw.i_val;
    return xsql_var_init(TYP_FLOAT, (void*)&z, NULL);
}

xsql_var_t xsql_var_float_div_int (xsql_var_t x, xsql_var_t y) {
    xsql_float_t z = x.raw.f_val / y.raw.i_val;
    return xsql_var_init(TYP_FLOAT, (void*)&z, NULL);
}

xsql_var_t xsql_var_float_eq_int (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.f_val == y.raw.i_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_float_neq_int (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.f_val != y.raw.i_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_float_gt_int (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.f_val > y.raw.i_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_float_lt_int (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.f_val < y.raw.i_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_float_ge_int (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.f_val >= y.raw.i_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_float_le_int (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.f_val <= y.raw.i_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

// float,float
xsql_var_t xsql_var_float_add_float (xsql_var_t x, xsql_var_t y) {
    xsql_float_t z = x.raw.f_val + y.raw.f_val;
    return xsql_var_init(TYP_FLOAT, (void*)&z, NULL);
}

xsql_var_t xsql_var_float_sub_float (xsql_var_t x, xsql_var_t y) {
    xsql_float_t z = x.raw.f_val - y.raw.f_val;
    return xsql_var_init(TYP_FLOAT, (void*)&z, NULL);
}

xsql_var_t xsql_var_float_mul_float (xsql_var_t x, xsql_var_t y) {
    xsql_float_t z = x.raw.f_val * y.raw.f_val;
    return xsql_var_init(TYP_FLOAT, (void*)&z, NULL);
}

xsql_var_t xsql_var_float_div_float (xsql_var_t x, xsql_var_t y) {
    xsql_float_t z = x.raw.f_val / y.raw.f_val;
    return xsql_var_init(TYP_FLOAT, (void*)&z, NULL);
}

xsql_var_t xsql_var_float_eq_float (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.f_val == y.raw.f_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_float_neq_float (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.f_val != y.raw.f_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_float_gt_float (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.f_val > y.raw.f_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_float_lt_float (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.f_val < y.raw.f_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_float_ge_float (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.f_val >= y.raw.f_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_float_le_float (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.f_val <= y.raw.f_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

// string,string
xsql_var_t xsql_var_string_add_string (xsql_var_t x, xsql_var_t y) {
    xsql_str_t *z = mkstr(x.raw.s_val->ptr, x.raw.s_val->len, XSQL_STR_CHUNK_SIZE);
    strnadd(&z, y.raw.s_val->ptr, y.raw.s_val->len);
    return xsql_var_init(TYP_STRING, (void*)&z, xsql_var_free_str);
}

xsql_var_t xsql_var_string_eq_string (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = 0 == cmpstr(x.raw.s_val->ptr, x.raw.s_val->len, y.raw.s_val->ptr, y.raw.s_val->len);
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_string_neq_string (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = 0 != cmpstr(x.raw.s_val->ptr, x.raw.s_val->len, y.raw.s_val->ptr, y.raw.s_val->len);
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

// bool,bool
xsql_var_t xsql_var_bool_eq_bool (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.b_val == y.raw.b_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_bool_neq_bool (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.b_val != y.raw.b_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_bool_gt_bool (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.b_val > y.raw.b_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_bool_lt_bool (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.b_val < y.raw.b_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_bool_ge_bool (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.b_val >= y.raw.b_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_bool_le_bool (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.b_val <= y.raw.b_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_bool_and_bool (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.b_val && y.raw.b_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_bool_or_bool (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = x.raw.b_val || y.raw.b_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

// uop
xsql_var_t xsql_var_neg_int (xsql_var_t v) {
    xsql_int_t z = -v.raw.i_val;
    return xsql_var_init(TYP_INT, (void*)&z, NULL);
}

xsql_var_t xsql_var_neg_float (xsql_var_t v) {
    xsql_float_t z = -v.raw.f_val;
    return xsql_var_init(TYP_FLOAT, (void*)&z, NULL);
}

xsql_var_t xsql_var_not_bool (xsql_var_t v) {
    xsql_bool_t z = !v.raw.b_val;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

// null
xsql_var_t xsql_var_true (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = 1;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

xsql_var_t xsql_var_false (xsql_var_t x, xsql_var_t y) {
    xsql_bool_t z = 0;
    return xsql_var_init(TYP_BOOL, (void*)&z, NULL);
}

int xsql_get_inst (xsql_type_t left, xsql_inst_t inst, xsql_type_t right, xsql_inst_h *ret_inst, xsql_type_t *ret_typ) {
    switch (left) {
        case TYP_INT:
            switch (right) {
                case TYP_INT:
                    switch (inst) {
                        case INST_ADD: *ret_inst = xsql_var_int_add_int; *ret_typ = TYP_INT; return 0;
                        case INST_SUB: *ret_inst = xsql_var_int_sub_int; *ret_typ = TYP_INT; return 0;
                        case INST_MUL: *ret_inst = xsql_var_int_mul_int; *ret_typ = TYP_INT; return 0;
                        case INST_DIV: *ret_inst = xsql_var_int_div_int; *ret_typ = TYP_INT; return 0;
                        case INST_EQ: *ret_inst = xsql_var_int_eq_int; *ret_typ = TYP_BOOL; return 0;
                        case INST_NEQ: *ret_inst = xsql_var_int_neq_int; *ret_typ = TYP_BOOL; return 0;
                        case INST_GT: *ret_inst = xsql_var_int_gt_int; *ret_typ = TYP_BOOL; return 0;
                        case INST_LT: *ret_inst = xsql_var_int_lt_int; *ret_typ = TYP_BOOL; return 0;
                        case INST_GE: *ret_inst = xsql_var_int_ge_int; *ret_typ = TYP_BOOL; return 0;
                        case INST_LE: *ret_inst = xsql_var_int_le_int; *ret_typ = TYP_BOOL; return 0;
                        default: break;
                    }
                    break;
                case TYP_FLOAT:
                    switch (inst) {
                        case INST_ADD: *ret_inst = xsql_var_int_add_float; *ret_typ = TYP_FLOAT; return 0;
                        case INST_SUB: *ret_inst = xsql_var_int_sub_float; *ret_typ = TYP_FLOAT; return 0;
                        case INST_MUL: *ret_inst = xsql_var_int_mul_float; *ret_typ = TYP_FLOAT; return 0;
                        case INST_DIV: *ret_inst = xsql_var_int_div_float; *ret_typ = TYP_FLOAT; return 0;
                        case INST_EQ: *ret_inst = xsql_var_int_eq_float; *ret_typ = TYP_BOOL; return 0;
                        case INST_NEQ: *ret_inst = xsql_var_int_neq_float; *ret_typ = TYP_BOOL; return 0;
                        case INST_GT: *ret_inst = xsql_var_int_gt_float; *ret_typ = TYP_BOOL; return 0;
                        case INST_LT: *ret_inst = xsql_var_int_lt_float; *ret_typ = TYP_BOOL; return 0;
                        case INST_GE: *ret_inst = xsql_var_int_ge_float; *ret_typ = TYP_BOOL; return 0;
                        case INST_LE: *ret_inst = xsql_var_int_le_float; *ret_typ = TYP_BOOL; return 0;
                        default: break;
                    }
                    break;
                case TYP_NONE:
                    switch (inst) {
                        case INST_EQ: *ret_inst = xsql_var_false; *ret_typ = TYP_BOOL; return 0;
                        case INST_NEQ: *ret_inst = xsql_var_true; *ret_typ = TYP_BOOL; return 0;
                        default: break;
                    }
                    break;
                default: break;
            }
            break;
        case TYP_FLOAT:
            switch (right) {
                case TYP_INT:
                    switch (inst) {
                        case INST_ADD: *ret_inst = xsql_var_float_add_int; *ret_typ = TYP_FLOAT; return 0;
                        case INST_SUB: *ret_inst = xsql_var_float_sub_int; *ret_typ = TYP_FLOAT; return 0;
                        case INST_MUL: *ret_inst = xsql_var_float_mul_int; *ret_typ = TYP_FLOAT; return 0;
                        case INST_DIV: *ret_inst = xsql_var_float_div_int; *ret_typ = TYP_FLOAT; return 0;
                        case INST_EQ: *ret_inst = xsql_var_float_eq_int; *ret_typ = TYP_BOOL; return 0;
                        case INST_NEQ: *ret_inst = xsql_var_float_neq_int; *ret_typ = TYP_BOOL; return 0;
                        case INST_GT: *ret_inst = xsql_var_float_gt_int; *ret_typ = TYP_BOOL; return 0;
                        case INST_LT: *ret_inst = xsql_var_float_lt_int; *ret_typ = TYP_BOOL; return 0;
                        case INST_GE: *ret_inst = xsql_var_float_ge_int; *ret_typ = TYP_BOOL; return 0;
                        case INST_LE: *ret_inst = xsql_var_float_le_int; *ret_typ = TYP_BOOL; return 0;
                        default: break;
                    }
                    break;
                case TYP_FLOAT:
                    switch (inst) {
                        case INST_ADD: *ret_inst = xsql_var_float_add_float; *ret_typ = TYP_FLOAT; return 0;
                        case INST_SUB: *ret_inst = xsql_var_float_sub_float; *ret_typ = TYP_FLOAT; return 0;
                        case INST_MUL: *ret_inst = xsql_var_float_mul_float; *ret_typ = TYP_FLOAT; return 0;
                        case INST_DIV: *ret_inst = xsql_var_float_div_float; *ret_typ = TYP_FLOAT; return 0;
                        case INST_EQ: *ret_inst = xsql_var_float_eq_float; *ret_typ = TYP_BOOL; return 0;
                        case INST_NEQ: *ret_inst = xsql_var_float_neq_float; *ret_typ = TYP_BOOL; return 0;
                        case INST_GT: *ret_inst = xsql_var_float_gt_float; *ret_typ = TYP_BOOL; return 0;
                        case INST_LT: *ret_inst = xsql_var_float_lt_float; *ret_typ = TYP_BOOL; return 0;
                        case INST_GE: *ret_inst = xsql_var_float_ge_float; *ret_typ = TYP_BOOL; return 0;
                        case INST_LE: *ret_inst = xsql_var_float_le_float; *ret_typ = TYP_BOOL; return 0;
                        default: break;
                    }
                    break;
                case TYP_NONE:
                    switch (inst) {
                        case INST_EQ: *ret_inst = xsql_var_false; *ret_typ = TYP_BOOL; return 0;
                        case INST_NEQ: *ret_inst = xsql_var_true; *ret_typ = TYP_BOOL; return 0;
                        default: break;
                    }
                    break;
                default: break;
            }
            break;
        case TYP_STRING:
            switch (right) {
                case TYP_STRING:
                    switch (inst) {
                        case INST_ADD: *ret_inst = xsql_var_string_add_string; *ret_typ = TYP_STRING; return 0;
                        case INST_EQ: *ret_inst = xsql_var_string_eq_string; *ret_typ = TYP_BOOL; return 0;
                        case INST_NEQ: *ret_inst = xsql_var_string_neq_string; *ret_typ = TYP_BOOL; return 0;
                        default: break;
                    }
                    break;
                case TYP_NONE:
                    switch (inst) {
                        case INST_EQ: *ret_inst = xsql_var_false; *ret_typ = TYP_BOOL; return 0;
                        case INST_NEQ: *ret_inst = xsql_var_true; *ret_typ = TYP_BOOL; return 0;
                        default: break;
                    }
                    break;
                default: break;
            }
            break;
        case TYP_BOOL:
            switch (right) {
                case TYP_BOOL:
                    switch (inst) {
                        case INST_EQ: *ret_inst = xsql_var_bool_eq_bool; *ret_typ = TYP_BOOL; return 0;
                        case INST_NEQ: *ret_inst = xsql_var_bool_neq_bool; *ret_typ = TYP_BOOL; return 0;
                        case INST_GT: *ret_inst = xsql_var_bool_gt_bool; *ret_typ = TYP_BOOL; return 0;
                        case INST_LT: *ret_inst = xsql_var_bool_lt_bool; *ret_typ = TYP_BOOL; return 0;
                        case INST_GE: *ret_inst = xsql_var_bool_ge_bool; *ret_typ = TYP_BOOL; return 0;
                        case INST_LE: *ret_inst = xsql_var_bool_le_bool; *ret_typ = TYP_BOOL; return 0;
                        case INST_AND: *ret_inst = xsql_var_bool_and_bool; *ret_typ = TYP_BOOL; return 0;
                        case INST_OR: *ret_inst = xsql_var_bool_or_bool; *ret_typ = TYP_BOOL; return 0;
                        default: break;
                    }
                    break;
                case TYP_NONE:
                    switch (inst) {
                        case INST_EQ: *ret_inst = xsql_var_false; *ret_typ = TYP_BOOL; return 0;
                        case INST_NEQ: *ret_inst = xsql_var_true; *ret_typ = TYP_BOOL; return 0;
                        default: break;
                    }
                    break;
                default: break;
            }
            break;
        case TYP_NONE:
            switch (right) {
                case TYP_NONE:
                    switch (inst) {
                        case INST_EQ: *ret_inst = xsql_var_true; *ret_typ = TYP_BOOL; return 0;
                        case INST_NEQ: *ret_inst = xsql_var_false; *ret_typ = TYP_BOOL; return 0;
                        default: break;
                    }
                    break;
                default:
                    switch (inst) {
                        case INST_EQ: *ret_inst = xsql_var_false; *ret_typ = TYP_BOOL; return 0;
                        case INST_NEQ: *ret_inst = xsql_var_true; *ret_typ = TYP_BOOL; return 0;
                        default: break;
                    }
                    break;
            }
            break;
        default: break;
    }
    return -1;
}

int xsql_get_uinst (xsql_type_t typ, xsql_inst_t inst, xsql_uinst_h *ret_inst, xsql_type_t *ret_typ) {
    switch (typ) {
        case TYP_INT:
            if (INST_NEG == inst) {
                *ret_inst = xsql_var_neg_int; *ret_typ = TYP_INT; return 0;
            }
            break;
        case TYP_FLOAT:
            if (INST_NEG == inst) {
                *ret_inst = xsql_var_neg_float; *ret_typ = TYP_FLOAT; return 0;
            }
            break;
        case TYP_BOOL:
            if (INST_NOT == inst) {
                *ret_inst = xsql_var_not_bool; *ret_typ = TYP_BOOL; return 0;
            }
            break;
        default: break;
    }
    return -1;
}
