#include <stdlib.h>
#include <mapi.h>
#include "db.h"

#define MAPI_AUTO	0
#define MAPI_TINY	1
#define MAPI_UTINY	2
#define MAPI_SHORT	3
#define MAPI_USHORT	4
#define MAPI_INT	5
#define MAPI_UINT	6
#define MAPI_LONG	7
#define MAPI_ULONG	8
#define MAPI_LONGLONG	9
#define MAPI_ULONGLONG	10
#define MAPI_CHAR	11
#define MAPI_VARCHAR	12
#define MAPI_FLOAT	13
#define MAPI_DOUBLE	14
#define MAPI_DATE	15
#define MAPI_TIME	16
#define MAPI_DATETIME	17
#define MAPI_NUMERIC	18

const char *get_provider_name () {
    return "monetdb";
}

const char *get_placeholder (int n) {
    return strdup("?");
}

static int parse_host (const char *host, char **hostname, int *port) {
    char *p = strchr(host, ':'), *tail;
    if (!p) return -1;
    *hostname = strndup(host, (uintptr_t)p - (uintptr_t)host);
    *port = strtol(++p, &tail, 0);
    return '\0' == *tail && ERANGE != errno ? 0 : -1;
}

xsql_conn_t *dbopen (xsql_dbprov_t *prov, const char *name, size_t name_len, const char *conninfo, size_t conninfo_len) {
    char *host = NULL, *dbname = NULL, *user = NULL, *pass = NULL, *hostname = NULL;
    int port;
    xsql_conn_t *conn = calloc(1, sizeof(xsql_conn_t));
    conn->name = mkstr(name, name_len, 8);
    conn->provider = prov;
    if (-1 == parse_conninfo((char*)conninfo, conninfo_len, &host, &dbname, &user, &pass) || !dbname || !user || !pass ||
        -1 == parse_host(host, &hostname, &port)) {
        char buf [256];
        snprintf(buf, sizeof buf, "connection string error %s", conninfo);
        conn->msg = strdup(buf);
    } else
    if (!conn->msg && (!(conn->hdb = mapi_connect(hostname, port, user, pass, "sql", dbname)) || mapi_error((Mapi)conn->hdb)))
        conn->msg = strdup(mapi_error_str((Mapi)conn->hdb));
    if (hostname) free(hostname);
    if (pass) free(pass);
    if (user) free(user);
    if (dbname) free(dbname);
    if (host) free(host);
    return conn;
}

xsql_stmt_t *sqlprepare (xsql_conn_t *conn, xsql_stmt_t *curstmt, const char *cmd, size_t cmd_len, size_t param_count) {
    xsql_stmt_t *stmt;
    if (curstmt) {
        stmt = curstmt;
        return stmt;
    }
    stmt = calloc(1, sizeof(xsql_stmt_t));
    stmt->conn = conn;
    stmt->hstmt = mapi_new_handle((Mapi)conn->hdb);
    if (MOK != mapi_prepare_handle((MapiHdl)stmt->hstmt, cmd))
        stmt->msg = strdup(mapi_result_error((MapiHdl)stmt->hstmt));
    return stmt;
}

static int fill_params (xsql_stmt_t *stmt, list_t *params) {
    size_t i = 0;
    int rc = XSQL_OK;
    lst_enum(params, (list_item_h)({
        int fn (list_item_t *li, void *userdata) {
            xsql_var_t *v = (xsql_var_t*)li->ptr;
            switch (v->typ) {
                case TYP_NONE:
                    stmt->msg = malloc(64);
                    snprintf(stmt->msg, 64, "parameter " SIZE_FMT " can't be a 'none' type", i);
                    rc = XSQL_ERROR;
                    return ENUM_BREAK;
                case TYP_INT:
                    mapi_param_type((MapiHdl)stmt->hstmt, i, MAPI_LONGLONG, MAPI_LONGLONG, (void*)&v->raw.i_val);
                    break;
                case TYP_FLOAT:
                    mapi_param_type((MapiHdl)stmt->hstmt, i, MAPI_DOUBLE, MAPI_DOUBLE, (void*)&v->raw.b_val);
                    break;
                case TYP_STRING:
                    mapi_param_type((MapiHdl)stmt->hstmt, i, MAPI_VARCHAR, MAPI_VARCHAR, (void*)v->raw.s_val->ptr);
                    break;
                case TYP_BOOL:
                    mapi_param_type((MapiHdl)stmt->hstmt, i, MAPI_INT, MAPI_INT, (void*)&v->raw.b_val);
                default:
                    stmt->msg = malloc(64);
                    snprintf(stmt->msg, 64, "parameter " SIZE_FMT " is unknown type", i);
                    rc = XSQL_ERROR;
                    return ENUM_BREAK;
            }
            ++i;
            return ENUM_CONTINUE;
        } fn;
    }), NULL, ENUM_STOP_IF_BREAK);
    return rc;
}

/*#define MONET_INTEGER_COUNT 5
#define MONET_FLOAT_COUNT 5
static const char *monet_integer [] = { "int", "tinyint", "bigint", "hugeint", "smallint" }; // TODO : + wrd, oid
static const char *monet_float [] = { "double", "float", "decimal", "real", "dbl" };

static int find_type_name (const char **a, const char *str, int count) {
    for (int i = 0; i < count; ++i)
        if (0 == strcmp(a[i], str)) return 1;
    return 0;
}*/

int fetch (xsql_stmt_t *stmt) {
    int nflds = mapi_fetch_row((MapiHdl)stmt->hstmt);
    if (nflds > 0) {
        if (!stmt->result)
            stmt->result = malloc(nflds * sizeof(char*));
        else
        if (nflds > stmt->nflds) {
            char **result = realloc(stmt->result, nflds * sizeof(char*));
            if (!result)
                return XSQL_ERROR;
            stmt->result = result;
        }
        if (!stmt->fldnams)
            stmt->fldnams = malloc(nflds * sizeof(str_t*));
        else
        if (nflds > stmt->nflds) {
            str_t **fldnams = realloc(stmt->fldnams, nflds * sizeof(str_t*));
            if (!fldnams)
                return XSQL_ERROR;
            stmt->fldnams = fldnams;
        }
    }
    for (int i = 0; i < nflds; ++i) {
        char *name = mapi_get_name((MapiHdl)stmt->hstmt, i);
        if (name && *name) {
            if (stmt->fldnams[i])
                strput(&stmt->fldnams[i], name, strlen(name), 0);
            else
                stmt->fldnams[i] = mkstr(name, strlen(name), 8);
        } else {
            if (stmt->fldnams[i])
                strput(&stmt->fldnams[i], CONST_STR_LEN("?column?"), 0);
            else
                stmt->fldnams[i] = mkstr(CONST_STR_LEN("?column?"), 8);
        }
        stmt->result[i] = mapi_fetch_field((MapiHdl)stmt->hstmt, i);
    }
    for (int i = nflds; i < stmt->nflds; ++i) {
        free(stmt->fldnams[i]);
        stmt->fldnams[i] = NULL;
        stmt->result[i] = NULL;
    }
    stmt->nflds = nflds;
    return nflds ? XSQL_OK : XSQL_FIN;
}

int sqlexec (xsql_stmt_t *stmt, list_t *params) {
    if (XSQL_ERROR == fill_params(stmt, params))
        return XSQL_ERROR;
    if (MOK != mapi_execute((MapiHdl)stmt->hstmt)) {
        stmt->msg = strdup(mapi_result_error((MapiHdl)stmt->hstmt));
        return XSQL_ERROR;
    }
    if (Q_TABLE != mapi_get_querytype((MapiHdl)stmt->hstmt))
        return XSQL_FIN;
    return fetch(stmt);
}

int sqlexec_direct (xsql_conn_t *conn, xsql_stmt_t **curstmt, const char *cmd, size_t cmd_len, list_t *params) {
    xsql_stmt_t *stmt = sqlprepare(conn, *curstmt, cmd, cmd_len, params->len);
    *curstmt = stmt;
    if (stmt->msg)
        return XSQL_ERROR;
    return sqlexec(stmt, params);
}

void sqlclose (xsql_stmt_t *stmt) {
    if (stmt->hstmt)
        mapi_close_handle((MapiHdl)stmt->hstmt);
    if (stmt->result)
        free(stmt->result);
    if (stmt->fldnams) {
        for (int i = 0; i < stmt->nflds; ++i)
            free(stmt->fldnams[i]);
        free(stmt->fldnams);
    }
    if (stmt->types)
        free(stmt->types);
    if (stmt->msg)
        free(stmt->msg);
    free(stmt);
}

void dbclose (xsql_conn_t *conn) {
    if (conn->name)
        free(conn->name);
    if (conn->hdb)
        mapi_disconnect((Mapi)conn->hdb);
    if (conn->msg)
        free(conn->msg);
    free(conn);
}
