#include "xsql.h"

#ifdef NDEBUG
#define AGG_SIZE 8
#else
#define AGG_SIZE 2
#endif

typedef struct {
    xsql_op_t *next;
    xsql_op_exec_h exec;
    xsql_op_destroy_h destroy;
    xsql_op_clone_h clone;
    xsql_script_t *scr;
    xsql_var_t *x;
} xsql_opstr_t;

int report_flags = 0;
xsql_getfname_h xsql_getfname = NULL;

static void var_clear (void *x, void *y) {
    xsql_var_t *var = (xsql_var_t*)y;
    free(var->name);
    free(var);
}

int xsql_gen_error (xsql_source_t *txt, const char *arg, ...) {
    if (txt->err) free(txt->err);
    va_list ap;
    va_start(ap, arg);
    txt->err = malloc(XSQL_ERR_BUF_SIZE);
    vsnprintf(txt->err, XSQL_ERR_BUF_SIZE, arg, ap);
    va_end(ap);
    char *p = txt->text_ptr;
    txt->line_no = 0;
    while (p >= txt->text)
        if ('\n' == *(p--)) ++txt->line_no;
    return XSQL_ERROR;
}

int xsql_gen_error_var_not_exists (xsql_source_t *txt, strptr_t *token) {
    token->ptr[token->len] = '\0';
    return xsql_gen_error(txt, "Variable \"%s\" not exists", token->ptr);
}

int xsql_gen_error_inv_conv_type (xsql_source_t *txt, xsql_type_t x_typ, xsql_type_t y_typ) {
    char x_str [32], y_str [32];
    strcpy(x_str, xsql_typstr(x_typ));
    strcpy(y_str, xsql_typstr(y_typ));
    return xsql_gen_error(txt, "Invalid conversion from '%s' to '%s'", x_str, y_str);
}

int xsql_gen_error_inv_arg (xsql_source_t *txt, xsql_type_t expected_typ, xsql_type_t typ) {
    char x_str [32], y_str [32];
    strcpy(x_str, xsql_typstr(expected_typ));
    strcpy(y_str, xsql_typstr(typ));
    return xsql_gen_error(txt, "expected %s but argument is of type %s", x_str, y_str);
}

static char *parse_meta_desc (char *p, xsql_meta_t *meta) {
    char *ps, *pe;
    while (*p && isspace(*p)) ++p;
    if (*p == ';')
        return p++;
    if (*p != '"')
        return NULL;
    ps = ++p;
    while (*p && *p != '"') ++p;
    if (*p != '"')
        return NULL;
    pe = p++;
    while (*p && *p != ';') ++p;
    if (*p != ';')
        return NULL;
    meta->desc = mkstr(ps, (uintptr_t)pe - (uintptr_t)ps, 8);
    return ++p;
}

static int parse_params (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token) {
    xsql_t *script = scr->scr_main;
    int ret;
    while (XSQL_OK == (ret = xsql_get_token(txt, token))) {
        xsql_type_t typ;
        strptr_t var_name, prompt;
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("INT"))) typ = TYP_INT; else
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("STRING"))) typ = TYP_STRING; else
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("FLOAT"))) typ = TYP_FLOAT; else
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("BOOL"))) typ = TYP_BOOL; else
            return xsql_gen_error(txt, "unknown type name");
        if (XSQL_OK != (ret = xsql_get_token(txt, token)))
            break;
        if (!(isalpha(token->ptr[0]) || '_' == token->ptr[0]))
            return xsql_gen_error(txt, "invalid variable name");
        var_name = *token;
        prompt = *token;
        xsql_param_t *param = malloc(sizeof(xsql_param_t));
        param->typ = typ;
        param->var_name = mkstr(var_name.ptr, var_name.len, 8);
        param->prompt = mkstr(prompt.ptr, prompt.len, 8);
        lst_adde(script->input_param_list, param);
        param->var = xsql_add_var(scr, var_name.ptr, var_name.len, typ);
        if (XSQL_OK != (ret = xsql_get_token(txt, token)))
            break;
        if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN(",")))
            continue;
        break;
    }
    return ret;
}

static void meta_free (void *dummy, xsql_meta_param_t *p) {
    if (p->label)
        free(p->label);
    free(p);
}

static char *parse_meta_params (char *p, xsql_meta_t *meta) {
    meta->params = lst_alloc((free_h)meta_free);
    while (*p && isspace(*p)) ++p;
    while (*p && *p != ';') {
        xsql_type_t t;
        char *ps, *pe;
        ps = p++;
        while (*p && isalpha(*p)) ++p;
        pe = p;
        while (*p && isspace(*p)) ++p;
        if (0 == cmpcasestr(ps, (uintptr_t)pe - (uintptr_t)ps, CONST_STR_LEN("INT"))) t = TYP_INT; else
        if (0 == cmpcasestr(ps, (uintptr_t)pe - (uintptr_t)ps, CONST_STR_LEN("FLOAT"))) t = TYP_FLOAT; else
        if (0 == cmpcasestr(ps, (uintptr_t)pe - (uintptr_t)ps, CONST_STR_LEN("CHAR"))) t = TYP_STRING; else
        if (0 == cmpcasestr(ps, (uintptr_t)pe - (uintptr_t)ps, CONST_STR_LEN("BOOL"))) t = TYP_BOOL; else
            return NULL;
        while (*p && isspace(*p)) ++p;
        ps = pe = NULL;
        if (*p == '"') {
            ps = ++p;
            while (*p && *p != '"') ++p;
            if (*p != '"')
                return NULL;
            pe = p++;
            while (*p && *p != ',' && *p != ';') ++p;
            if (*p == ',') {
                ++p;
                while (*p && isspace(*p)) ++p;
            }
        } else
        if (*p == ',') {
            ++p;
            while (*p && isspace(*p)) ++p;
            continue;
        }
        xsql_meta_param_t *param = calloc(1, sizeof(xsql_meta_param_t));
        param->typ = t;
        if (ps)
            param->label = mkstr(ps, (uintptr_t)pe - (uintptr_t)ps, 8);
        lst_adde(meta->params, param);
    }
    if (*p == ';')
        return ++p;
    return NULL;
}

xsql_meta_t *xsql_load_meta (const char *fname) {
    int fd;
    xsql_meta_t *meta = calloc(1, sizeof(xsql_meta_t));
    if (-1 != (fd = open(fname, O_RDONLY))) {
        struct stat st;
        char *buf = NULL;
        if (-1 != fstat(fd, &st) && (buf = malloc(st.st_size+1)) && st.st_size == read(fd, buf, st.st_size)) {
            char *p = buf;
            buf[st.st_size] = '\0';
            while ((p = strchr(p, '.'))) {
                ++p;
                if (!meta->desc && 0 == cmpcasestr(p, sizeof("NAME")-1, CONST_STR_LEN("NAME"))) {
                    if (!(p = parse_meta_desc(p + sizeof("NAME")-1, meta))) {
                        xsql_free_meta(meta);
                        meta = NULL;
                        break;
                    }
                } else
                if (!meta->params && 0 == cmpcasestr(p, sizeof("INPUT")-1, CONST_STR_LEN("INPUT"))) {
                    if (!(p = parse_meta_params(p + sizeof("INPUT")-1, meta))) {
                        xsql_free_meta(meta);
                        meta = NULL;
                        break;
                    }
                }
                if (meta->desc && meta->params)
                    break;
            }
        }
        if (buf) free(buf);
        close(fd);
    }
    return meta;
}

void xsql_free_meta (xsql_meta_t *meta) {
    if (meta->desc)
        free(meta->desc);
    if (meta->params)
        lst_free(meta->params);
    free(meta);
}

void xsql_script_destroy (xsql_t *scr) {
    if (scr->fmtstd) {
        xsql_fmtprov_t *p = scr->fmtstd->prov;
        p->fmtclose(scr->fmtstd);
    }
    if (scr->aggs) free(scr->aggs);
    free(scr->name);
    lst_free(scr->param_list);
    if (scr->op) xsql_op_free(scr->op);
    lst_free(scr->var_list);
    lst_free(scr->conn_list);
    if (scr->err) free(scr->err);
    if (scr->input_param_list)
        lst_free(scr->input_param_list);
    lst_free(scr->files);
    memset(scr, 0, sizeof(xsql_t));
}

int xsql_load_file (const char *fname, xsql_source_t *txt) {
    struct stat st;
    errno = 0;
    memset(txt, 0, sizeof(xsql_source_t));
    txt->fname = mkstr(fname, strlen(fname), 8);
    int fd = open(txt->fname->ptr, O_RDONLY);
    if (-1 != fd && -1 != fstat(fd, &st)) {
        txt->text = txt->text_ptr = malloc(st.st_size+1);
        if (st.st_size == read(fd, txt->text, st.st_size))
            txt->text[st.st_size] = '\0';
        else
            xsql_gen_error(txt, txt->fname->ptr, strerror(errno));
    } else
        xsql_gen_error(txt, txt->fname->ptr, strerror(errno));
    if (-1 != fd)
        close(fd);
    return txt->err ? XSQL_ERROR : XSQL_OK;
}

void xsql_source_destroy (xsql_source_t *txt) {
    if (txt->text) free(txt->text);
    if (txt->fname) free(txt->fname);
    if (txt->err) free(txt->err);
    memset(txt, 0, sizeof(xsql_source_t));
}

xsql_var_t *xsql_get_var (xsql_script_t *scr, const char *name, size_t name_len) {
    list_item_t *x = scr->var_list->head;
    if (x) {
        do {
            xsql_var_t *var = (xsql_var_t*)x->ptr;
            if (0 == cmpstr(var->name->ptr, var->name->len, name, name_len))
                return var;
            x = x->next;
        } while (x != scr->var_list->head);
    }
    if (scr != (xsql_script_t*)scr->scr_main)
        return xsql_get_var((xsql_script_t*)scr->scr_main, name, name_len);
    return NULL;
}

xsql_var_t *xsql_add_var (xsql_script_t *scr, const char *name, size_t name_len, xsql_type_t typ) {
    xsql_var_t *var = calloc(1, sizeof(xsql_var_t));
    var->name = mkstr(name, name_len, 8);
    var->typ = typ;
    lst_adde(scr->var_list, (void*)var);
    return var;
}

static const char *delim_chars = ";+-*/=!><()\",[]:";

static int get_token (xsql_source_t *txt, strptr_t *ret) {
    char *p = txt->text_ptr, *q;
    ret->ptr = NULL; ret->len = 0;
    while (*p && isspace(*p)) ++p;
    if ('\0' == *p)
        return XSQL_FIN;
    q = p;
    if ('"' == *p) {
        ret->ptr = p++;
        while (*p && *p != '"') {
            if ('\\' == *p) ++p;
            ++p;
        }
        if (!*p) {
            xsql_gen_error(txt, "'\"' expected");
            return XSQL_ERROR;
        }
        txt->text_ptr = ++p;
        ret->ptr = q;
        ret->len = (uintptr_t)p - (uintptr_t)q;
        return XSQL_OK;
    }
    switch (*p++) {
        case '+':
        case '-':
        case '*':
        case ';':
        case '(':
        case ')':
        case ',':
            ret->ptr = q; ret->len = 1; txt->text_ptr = p;
            return XSQL_OK;
        case '=':
        case '!':
        case '<':
        case '>':
            ret->ptr = q;
            if ('=' == *p) {
                ret->len = 2; txt->text_ptr = ++p;
                return XSQL_OK;
            }
            ret->len = 1; txt->text_ptr = p;
            return XSQL_OK;
        case '/':
            switch (*p) {
                case '*':
                    ++p;
                    if ((p = strstr(p, "*/"))) {
                        ret->ptr = q; p += 2; ret->len = (uintptr_t)p - (uintptr_t)q; txt->text_ptr = p;
                        return XSQL_COMMENT;
                    } else
                        return xsql_gen_error(txt, "\"*/\" expected");
                    break;
                case '/':
                    while (*p && *p != '\n') ++p;
                    while (*p && isspace(*p)) ++p;
                    ret->ptr = q; ret->len = (uintptr_t)p - (uintptr_t)q; txt->text_ptr = p;
                    return XSQL_COMMENT;
                default:
                    ret->ptr = q; ret->len = 1; txt->text_ptr = p;
                    return XSQL_OK;
            }
            break;
    }
    while (*p && !isspace(*p) && !strchr(delim_chars, *p)) ++p;
    char *e = p;
    if (e > q) {
        ret->ptr = q;
        ret->len = (uintptr_t)p - (uintptr_t)q;
        txt->text_ptr = p;
    }
    return XSQL_OK;
}

int xsql_get_token (xsql_source_t *txt, strptr_t *ret) {
    int rc;
    while (XSQL_COMMENT == (rc = get_token(txt, ret)));
    return rc;
}
#if 0
static int is_strptr (char *str, strptr_t *found_str, strptr_t **str_temp, size_t str_temp_len) {
    for (size_t i = 0; i < str_temp_len; ++i) {
        strptr_t *s = str_temp[i];
        if (0 == strncmp(str, s->ptr, s->len)) {
            *found_str = *s;
            return 1;
        }
    }
    return 0;
}

static int scan_while_not_strs (xsql_script_t *scr, strptr_t *result, strptr_t *found_str, strptr_t **str_temp, size_t str_temp_len) {
    int ret = XSQL_OK;
    char *p = scr->text_ptr;
    while (*p && is_strptr(p, found_str, str_temp, str_temp_len)) {
        switch (*p++) {
            case '/':
                switch (*p++) {
                    case '*':
                        if (!(p = strstr(p, "*/")))
                            ret = xsql_gen_error(scr, "\"*/\" expected");
                        break;
                    case '/':
                        while (*p && !isspace(*p)) ++p;
                        break;
                    default: break;
                }
            case '"':
                while (*p && '"' != *p) ++p;
                break;
            default: break;
        }
    }
    if (XSQL_ERROR == ret)
        return ret;
    result->ptr = scr->text_ptr;
    result->len = (uintptr_t)p - (uintptr_t)scr->text_ptr;
    scr->text_ptr = p;
    return '\0' == *p ? XSQL_FIN : XSQL_OK;
}
#endif
static int xsql_get_token_nf (xsql_source_t *txt, strptr_t *token) {
    int ret = xsql_get_token(txt, token);
    if (XSQL_FIN == ret) xsql_gen_error(txt, "Identifier expected");
    return ret;
}

static void eval_free (void *dummy, xsql_ev_t *ev) {
    xsql_eval_free(ev);
}

static int parse_cmpb (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_ev_t **result);
static int parse_var (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_ev_t **result) {
    xsql_raw_t raw;
    xsql_inst_t uinst = INST_NOP;
    int ret = xsql_get_token(txt, token);
    *result = NULL;
    if (XSQL_OK != ret) return ret;
    
    if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("-"))) {
        uinst = INST_NEG;
        if (XSQL_ERROR == xsql_get_token_nf(txt, token)) return XSQL_ERROR;
    }
    
    if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("!"))) {
        uinst = INST_NOT;
        if (XSQL_ERROR == xsql_get_token_nf(txt, token)) return XSQL_ERROR;
    }
    
    if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("("))) {
        if (XSQL_ERROR == (ret = parse_cmpb(txt, scr, token, result)))
            return ret;
        if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN(")")))
            return xsql_get_token_nf(txt, token);
        return xsql_gen_error(txt, "\")\" expected");
    }
    
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("TRUE"))) {
        raw.b_val = 1;
        *result = xsql_eval_add_var(TYP_BOOL, &raw, NULL);
        return xsql_get_token_nf(txt, token);
    }

    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("FALSE"))) {
        raw.b_val = 0;
        *result = xsql_eval_add_var(TYP_BOOL, &raw, NULL);
        return xsql_get_token_nf(txt, token);
    }
    
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("NULL"))) {
        memset(&raw, 0, sizeof(xsql_raw_t));
        *result = xsql_eval_add_var(TYP_NONE, &raw, NULL);
        return xsql_get_token_nf(txt, token);
    }
    
    if (isalpha(token->ptr[0]) || '_' == token->ptr[0]) {
        xsql_var_t *var = xsql_get_var(scr, token->ptr, token->len);
        if (!var)
            return xsql_gen_error_var_not_exists(txt, token);
        *result = xsql_eval_add_var_link(var);
        return xsql_get_token_nf(txt, token);
    }
    
    if ('"' == token->ptr[0] && '"' == token->ptr[token->len-1]) {
        raw.s_val = str_unescape(token->ptr+1, token->len-2, 32);
        *result = xsql_eval_add_var(TYP_STRING, &raw, xsql_var_free_str);
        return xsql_get_token_nf(txt, token);
    }
    
    char *s = strndup(token->ptr, token->len), *tail;
    raw.i_val = strtoll(s, &tail, 0);
    if ('\0' == *tail && ERANGE != errno)
        *result = xsql_eval_add_var(TYP_INT, &raw, NULL);
    else {
        raw.f_val = strtod(s, &tail);
        if ('\0' == *tail && ERANGE != errno)
            *result = xsql_eval_add_var(TYP_FLOAT, &raw, NULL);
        else {
            ret = xsql_gen_error(txt, "invalid suffix \"%s\" on constant", s);
            free(s);
            return ret;
        }
    }
    free(s);
    
    if (INST_NOP != uinst) {
        xsql_ev_t *ev = xsql_eval_add_uinst(uinst, *result);
        if (!ev)
            return xsql_gen_error(txt, "Wrong type argument to unary minus");
        *result = ev;
    }

    return xsql_get_token_nf(txt, token);
}

static xsql_inst_t find_muldiv (strptr_t *token) {
    if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("*"))) return INST_MUL;
    if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("/"))) return INST_DIV;
    return INST_NOP;
}

static int parse_muldiv (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_ev_t **result) {
    xsql_ev_t *left;
    int ret = parse_var(txt, scr, token, &left);
    if (XSQL_OK == ret) {
        xsql_inst_t inst;
        *result = left;
        while (XSQL_OK == ret && INST_NOP != (inst = find_muldiv(token))) {
            xsql_ev_t *right;
            if (XSQL_OK == (ret = parse_var(txt, scr, token, &right))) {
                xsql_ev_t *ev = *result = xsql_eval_add_inst(inst, left, right);
                if (ev)
                    left = ev;
                else
                    ret = xsql_gen_error_inv_conv_type(txt, left->var.typ, right->var.typ);
            }
        }
    }
    return ret;
}

static xsql_inst_t find_addsub (strptr_t *token) {
    if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("+"))) return INST_ADD;
    if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("-"))) return INST_SUB;
    return INST_NOP;
}

static int parse_addsub (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_ev_t **result) {
    xsql_ev_t *left;
    int ret = parse_muldiv(txt, scr, token, &left);
    if (XSQL_OK == ret) {
        xsql_inst_t inst;
        *result = left;
        while (XSQL_OK == ret && INST_NOP != (inst = find_addsub(token))) {
            xsql_ev_t *right;
            if (XSQL_OK == (ret = parse_muldiv(txt, scr, token, &right))) {
                xsql_ev_t *ev = *result = xsql_eval_add_inst(inst, left, right);
                if (ev)
                    left = ev;
                else
                    ret = xsql_gen_error_inv_conv_type(txt, left->var.typ, right->var.typ);
            }
        }
    }
    return ret;
}

static xsql_inst_t find_cmpv (strptr_t *token) {
    if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("=="))) return INST_EQ;
    if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("!="))) return INST_NEQ;
    if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN(">"))) return INST_GT;
    if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("<"))) return INST_LT;
    if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN(">="))) return INST_GE;
    if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("<="))) return INST_LE;
    return INST_NOP;
}

static int parse_cmpv (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_ev_t **result) {
    xsql_ev_t *left;
    int ret = parse_addsub(txt, scr, token, &left);
    if (XSQL_OK == ret) {
        xsql_inst_t inst;
        *result = left;
        while (XSQL_OK == ret && INST_NOP != (inst = find_cmpv(token))) {
            xsql_ev_t *right;
            if (XSQL_OK == (ret = parse_addsub(txt, scr, token, &right))) {
                xsql_ev_t *ev = *result = xsql_eval_add_inst(inst, left, right);
                if (ev)
                    left = ev;
                else
                    ret = xsql_gen_error_inv_conv_type(txt, left->var.typ, right->var.typ);
            }
        }
    }
    return ret;
}

static xsql_inst_t find_cmpb (strptr_t *token) {
    if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("AND"))) return INST_AND;
    if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("OR"))) return INST_OR;
    return INST_NOP;
}

static int parse_cmpb (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_ev_t **result) {
    xsql_ev_t *left;
    int ret = parse_cmpv(txt, scr, token, &left);
    if (XSQL_OK == ret) {
        xsql_inst_t inst;
        *result = left;
        while (XSQL_OK == ret && INST_NOP != (inst = find_cmpb(token))) {
            xsql_ev_t *right;
            if (XSQL_OK == (ret = parse_cmpv(txt, scr, token, &right))) {
                xsql_ev_t *ev = *result = xsql_eval_add_inst(inst, left, right);
                if (ev)
                    left = ev;
                else
                    ret = xsql_gen_error_inv_conv_type(txt, left->var.typ, right->var.typ);
            }
        }
    }
    return ret;
}

xsql_ev_t *xsql_parse_eval (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token) {
    xsql_ev_t *ev = NULL;
    if (XSQL_ERROR == parse_cmpb(txt, scr, token, &ev)) {
        xsql_eval_free(ev);
        return NULL;
    }
    return ev;
}

strptr_t if_tokens_end [] = { {3, "END"}, {4, "ELSE"} };
strptr_t endif_token = {3, "END"};
strptr_t endfor_token = {3, "END"};
strptr_t endwhile_token = {3, "END"};
strptr_t endstore_token = {3, "END"};
strptr_t endload_token = {3, "END"};
strptr_t endloop_token = {3, "END" };
strptr_t case_tokens_end [] = { {4, "WHEN"}, {3, "END"} };

static int is_token_end (strptr_t *token, strptr_t *end_tokens, int end_tokens_cnt) {
    for (int i = 0; i < end_tokens_cnt; ++i)
        if (0 == cmpcasestr(token->ptr, token->len, end_tokens[i].ptr, end_tokens[i].len))
            return 1;
    return 0;
}

static xsql_op_t *xsql_parse_block (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_op_t **op_last, strptr_t *end_tokens, size_t end_tokens_cnt);

static int xsql_parse_assign (xsql_source_t *txt, xsql_script_t *scr, strptr_t *name, strptr_t *token, xsql_op_t **result) {
    if (0 == cmpstr(name->ptr, name->len, CONST_STR_LEN(SYS_STATUS)))
        return xsql_gen_error(txt, "assignment of read-only variable \"%s\"", SYS_STATUS);
    xsql_ev_t *ev = xsql_parse_eval(txt, scr, token);
    if (!ev)
        return XSQL_ERROR;
    xsql_var_t *var = xsql_get_var(scr, name->ptr, name->len);
    if (!var)
        var = xsql_add_var(scr, name->ptr, name->len, ev->var_ptr->typ);
    else
        var->typ = ev->var_ptr->typ;
    xsql_assign_t *op = calloc(1, sizeof(xsql_assign_t));
    op->exec = xsql_assign_exec;
    op->destroy = xsql_assign_destroy;
    op->scr = scr;
    op->var = var;
    op->ev = ev;
    op->next = NULL;
    *result = (xsql_op_t*)op;
    return XSQL_OK;
}

static int xsql_parse_print (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_op_t **result) {
    xsql_ev_t *ev;
    xsql_print_t *op = calloc(1, sizeof(xsql_print_t));
    *result = (xsql_op_t*)op;
    op->scr = scr;
    op->ev_list = lst_alloc((free_h)eval_free);
    while ((ev = xsql_parse_eval(txt, scr, token))) {
        lst_adde(op->ev_list, (void*)ev);
        if (0 != cmpstr(token->ptr, token->len, CONST_STR_LEN(",")))
            break;
    }
    op->exec = xsql_print_exec;
    op->destroy = xsql_print_destroy;
    return txt->err ? XSQL_ERROR : XSQL_OK;
}

static int xsql_parse_if (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_op_t **result) {
    xsql_if_t *op = calloc(1, sizeof(xsql_if_t));
    xsql_ev_t *ev = op->ev = xsql_parse_eval(txt, scr, token);
    *result = (xsql_op_t*)op;
    if (!ev)
        return XSQL_ERROR;
    if (0 != cmpcasestr(token->ptr, token->len, CONST_STR_LEN("THEN")))
        return xsql_gen_error(txt, "\"THEN\" expected");
    op->exec = xsql_if_exec;
    op->destroy = xsql_if_destroy;
    op->scr = scr;
    op->op_true = xsql_parse_block(txt, scr, token, NULL, if_tokens_end, sizeof(if_tokens_end)/sizeof(strptr_t));
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("ELSE")))
        op->op_false = xsql_parse_block(txt, scr, token, NULL, &endif_token, 1);
    if (0 != cmpcasestr(token->ptr, token->len, CONST_STR_LEN("END")))
        return xsql_gen_error(txt, "\"END\" expected");
    return xsql_get_token(txt, token);
}

static int xsql_parse_case_item (xsql_source_t *txt, xsql_case_t *op, strptr_t *token) {
    xsql_case_item_t *ci = calloc(1, sizeof(xsql_case_item_t));
    lst_adde(op->items, (void*)ci);
    ci->ev = xsql_parse_eval(txt, op->scr, token);
    if (!ci->ev)
        return XSQL_ERROR;
    if (0 != cmpcasestr(token->ptr, token->len, CONST_STR_LEN("THEN")))
        return xsql_gen_error(txt, "\"THEN\" expected");
    ci->body = xsql_parse_block(txt, op->scr, token, NULL, case_tokens_end, sizeof(case_tokens_end)/sizeof(strptr_t));
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("END")))
        return XSQL_FIN;
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("WHEN")))
        return XSQL_OK;
    return xsql_gen_error(txt, "\"WHEN\" expected");
}

static void case_items_free (void *dummy, xsql_case_item_t *p) {
    xsql_op_free(p->body);
    xsql_eval_free(p->ev);
    free(p);
}

static int xsql_parse_case (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_op_t **result) {
    int ret;
    xsql_case_t *op = calloc(1, sizeof(xsql_case_t));
    *result = (xsql_op_t*)op;
    op->scr = scr;
    op->exec = xsql_case_exec;
    op->destroy = xsql_case_destroy;
    op->items = lst_alloc((free_h)case_items_free);
    if (XSQL_ERROR == (ret = xsql_get_token(txt, token)))
        return ret;
    if (0 != cmpcasestr(token->ptr, token->len, CONST_STR_LEN("WHEN")))
        return xsql_gen_error(txt, "\"WHEN\" expected");
    while (XSQL_OK == (ret = xsql_parse_case_item(txt, op, token)));
    if (XSQL_ERROR == ret)
        return ret;
    return xsql_get_token(txt, token);
}

static int xsql_parse_for (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_op_t **result) {
    int ret;
    strptr_t name;
    *result = calloc(1, sizeof(xsql_for_t));
    xsql_for_t *op = (xsql_for_t*)*result;
    op->exec = xsql_for_exec;
    op->destroy = xsql_for_destroy;
    op->scr = scr;
    if (XSQL_OK != (ret = xsql_get_token(txt, &name)))
        return xsql_gen_error(txt, "Variable expected");
    if (XSQL_OK != xsql_get_token(txt, token) || 0 != cmpstr(token->ptr, token->len, CONST_STR_LEN("=")))
        return xsql_gen_error(txt, "\"=\" expected");
    if (!(op->ev_init = xsql_parse_eval(txt, scr, token)))
        return XSQL_ERROR;
    if (0 != cmpstr(token->ptr, token->len, CONST_STR_LEN(";")))
        return xsql_gen_error(txt, "\";\" expected");
    if (!(op->var_iter = xsql_get_var(scr, name.ptr, name.len)))
        op->var_iter = xsql_add_var(scr, name.ptr, name.len, op->ev_init->var.typ);
    if (!(op->ev_end = xsql_parse_eval(txt, scr, token)))
        return XSQL_ERROR;
    if (0 != cmpstr(token->ptr, token->len, CONST_STR_LEN(";")))
        return xsql_gen_error(txt, "\";\" expected");
    if (!(op->ev_step = xsql_parse_eval(txt, scr, token)))
        return XSQL_ERROR;
    if (0 != cmpcasestr(token->ptr, token->len, CONST_STR_LEN("DO")))
        return xsql_gen_error(txt, "\"DO\" expected");
    if (!(op->op_body = xsql_parse_block(txt, scr, token, NULL, &endfor_token, 1)) && txt->err)
        return XSQL_ERROR;
    if (0 != cmpcasestr(token->ptr, token->len, CONST_STR_LEN("END")))
        return xsql_gen_error(txt, "\"END\" expected");
    return xsql_get_token(txt, token);
}

static int xsql_parse_while (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_op_t **result) {
    xsql_while_t *op = calloc(1, sizeof(xsql_while_t));
    *result = (xsql_op_t*)op;
    op->exec = xsql_while_exec;
    op->destroy = xsql_while_destroy;
    op->scr = scr;
    if (!(op->ev = xsql_parse_eval(txt, scr, token)))
        return XSQL_ERROR;
    if (0 != cmpcasestr(token->ptr, token->len, CONST_STR_LEN("DO")))
        return xsql_gen_error(txt, "\"DO\" expected");
    if (!(op->op_body = xsql_parse_block(txt, scr, token, NULL, &endwhile_token, 1)) && txt->err)
        return XSQL_ERROR;
    if (0 != cmpcasestr(token->ptr, token->len, CONST_STR_LEN("END")))
        return xsql_gen_error(txt, "\"END\" expected");
    return xsql_get_token(txt, token);
}

str_t *xsql_getpath (const char *s, size_t l) {
    if (!is_abspath(s, l)) {
        char d [1024];
        getcwd(d, sizeof(d));
        if (xsql_getfname)
            return xsql_getfname(d, strlen(d), s, l);
        return path_expand(d, strlen(d), s, l);
    }
    if (xsql_getfname)
        return xsql_getfname(NULL, 0, s, l);
    return mkstr(s, l, 8);
}

static int xsql_parse_store (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_op_t **result) {
    xsql_store_t *op = calloc(1, sizeof(xsql_store_t));
    *result = (xsql_op_t*)op;
    op->exec = xsql_store_exec;
    op->destroy = xsql_store_destroy;
    op->scr = scr;
    op->flags = report_flags;
    int ret;
    strptr_t name = {0,NULL}, format = {0,NULL}, fname = {0,NULL};
    while (XSQL_OK == (ret = xsql_get_token(txt, token)) && 0 != cmpcasestr(token->ptr, token->len, CONST_STR_LEN("DO"))) {
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("NAME"))) {
            if (XSQL_OK != xsql_get_token(txt, &name) || name.len < 3)
                return xsql_gen_error(txt, "name expected");
            name.ptr++; name.len -= 2;
        } else
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("FORMAT"))) {
            if (XSQL_OK != xsql_get_token(txt, &format) || format.len < 3)
                return xsql_gen_error(txt, "format expected");
            format.ptr++; format.len -= 2;
        } else
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("TO"))) {
            if (XSQL_OK != xsql_get_token(txt, &fname) || fname.len < 7)
                return xsql_gen_error(txt, "file name expected");
        } else
            return xsql_gen_error(txt, "identifier expected");
    }
    if (XSQL_OK != ret)
        return xsql_gen_error(txt, "identifier expected");
    if (name.ptr)
        op->name = mkstr(name.ptr, name.len, 8);
    if (fname.ptr) {
        if ('"' == fname.ptr[0] && '"' == fname.ptr[fname.len-1])
            op->fname = xsql_getpath(fname.ptr+1, fname.len-2);
        else
        if ('"'!= fname.ptr[0] && '"' != fname.ptr[fname.len-1]) {
            if (!(op->v_fname = xsql_get_var(scr, fname.ptr, fname.len)))
                return xsql_gen_error(txt, "identifier expected");
        } else
            return xsql_gen_error(txt, "\" expected");
    }
    if (!format.ptr) {
        format.ptr = "text";
        format.len = sizeof("text")-1;
    }
    if (!(op->fmtprov = xsql_find_fmtprov(format.ptr, format.len)))
        return xsql_gen_error(txt, "formatter not found");
    if (!(op->op_body = xsql_parse_block(txt, scr, token, NULL, &endload_token, 1)))
        return XSQL_ERROR;
    if (0 != cmpcasestr(token->ptr, token->len, CONST_STR_LEN("END")))
        return xsql_gen_error(txt, "\"END\" expected");
    return xsql_get_token(txt, token);
}

static int on_add_var (list_item_t *li, void *userdata) {
    xsql_script_t *scr = (xsql_script_t*)userdata;
    strptr_t *name = (strptr_t*)li->ptr;
    xsql_var_t *v = xsql_get_var(scr, name->ptr, name->len);
    if (v)
        v->typ = TYP_STRING;
    else
        v = xsql_add_var(scr, name->ptr, name->len, TYP_STRING);
    return ENUM_CONTINUE;
}

static int xsql_parse_load (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_op_t **result) {
    int ret;
    strptr_t format = CONST_STR_INIT_NULL, fname = CONST_STR_INIT_NULL;
    xsql_load_t *op = calloc(1, sizeof(xsql_load_t));
    *result = (xsql_op_t*)op;
    op->exec = xsql_load_exec;
    op->destroy = xsql_load_destroy;
    op->scr = scr;
    op->var_list = lst_alloc(on_default_free_item);
    while (XSQL_OK == (ret = xsql_get_token(txt, token)) && 0 != cmpcasestr(token->ptr, token->len, CONST_STR_LEN("DO"))) {
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("FORMAT"))) {
            if (XSQL_OK != xsql_get_token(txt, &format))
                return xsql_gen_error(txt, "format expected");
            format.ptr++; format.len -= 2;
        } else
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("FROM"))) {
            if (XSQL_OK != xsql_get_token(txt, &fname))
                return xsql_gen_error(txt, "file name expected");
        } else
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("INTO"))) {
            while (XSQL_OK == (ret = xsql_get_token(txt, token))) {
                strptr_t *s = malloc(sizeof(strptr_t));
                *s = *token;
                lst_adde(op->var_list, s);
                if (XSQL_OK != xsql_get_token(txt, token))
                    return xsql_gen_error(txt, "identifier expected");
                if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("DO")))
                    goto do_achieved;
                if (0 != cmpstr(token->ptr, token->len, CONST_STR_LEN(",")))
                    return xsql_gen_error(txt, "\",\" or \"DO\" expected");
            }
        }
    }
do_achieved:
    if (0 == op->var_list->len)
        return xsql_gen_error(txt, "variable list expected");
    lst_enum(op->var_list, on_add_var, scr, 0);
    if (!fname.ptr)
        return xsql_gen_error(txt, "file name expected");
    if (!format.ptr) {
        format.ptr = "binary";
        format.len = sizeof("binary")-1;
    }
    if ('"' == fname.ptr[0] && '"' == fname.ptr[fname.len-1])
        op->fname = xsql_getpath(fname.ptr+1, fname.len-2);
    else
    if ('"'!= fname.ptr[0] && '"' != fname.ptr[fname.len-1]) {
        if (!(op->v_fname = xsql_get_var(scr, fname.ptr, fname.len)))
            return xsql_gen_error(txt, "identifier expected");
    } else
        return xsql_gen_error(txt, "\" expected");
    if (!(op->fmtprov = xsql_find_fmtprov(format.ptr, format.len)))
        return xsql_gen_error(txt, "formatter not found");
    if (!(op->op_body = xsql_parse_block(txt, scr, token, NULL, &endload_token, 1)))
        return XSQL_ERROR;
    if (0 != cmpcasestr(token->ptr, token->len, CONST_STR_LEN("END")))
        return xsql_gen_error(txt, CONST_STR_LEN("\"END\" expected"));
    return xsql_get_token(txt, token);
}

static int xsql_parse_connect (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_op_t **result) {
    xsql_t *scr_main = scr->scr_main;
    xsql_connect_t *op = calloc(1, sizeof(xsql_connect_t));
    *result = (xsql_op_t*)op;
    op->exec = xsql_connect_exec;
    op->destroy = xsql_connect_destroy;
    op->scr = scr;
    strptr_t conninfo = {0,NULL}, provider_name = {0,NULL}, name = {0,NULL};
    if (XSQL_OK != xsql_get_token(txt, token))
        return xsql_gen_error(txt, "connection information expected");
    conninfo = *token;
    if (XSQL_OK != xsql_get_token(txt, token) || 0 != cmpcasestr(token->ptr, token->len, CONST_STR_LEN("PROVIDER")))
        return xsql_gen_error(txt, "\"PROVIDER\" expected");
    if (XSQL_OK != xsql_get_token(txt, token))
        return xsql_gen_error(txt, "provider expected");
    provider_name = *token;
    if ('"' != *(provider_name.ptr) || '"' != *(provider_name.ptr+provider_name.len-1))
        return xsql_gen_error(txt, "\" expected");
    ++provider_name.ptr; provider_name.len -= 2;
    op->provider = scr_main->cur_prov = xsql_find_provider(provider_name.ptr, provider_name.len);
    if (!scr_main->cur_prov)
        return xsql_gen_error(txt, "provider not found");
    if (XSQL_OK != xsql_get_token(txt, token))
        return xsql_gen_error(txt, "identifier expected");
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("AS"))) {
        if (XSQL_OK != xsql_get_token(txt, token))
            return xsql_gen_error(txt, "connection name expected");
        name = *token;
        if ('"' != *(name.ptr) || '"' != *(name.ptr+name.len-1))
            return xsql_gen_error(txt, "\" expected");
        ++name.ptr; name.len -= 2;
    }
    if ('"' == *(conninfo.ptr) && '"' == *(conninfo.ptr+conninfo.len-1))
        op->conninfo = mkstr(conninfo.ptr+1, conninfo.len-2, 8);
    else
    if ('"' != *(conninfo.ptr) && '"' != *(conninfo.ptr+conninfo.len-1)) {
        if (!(op->v_conninfo = xsql_get_var(scr, conninfo.ptr, conninfo.len)))
            return xsql_gen_error_var_not_exists(txt, &conninfo);
        if (TYP_STRING != op->v_conninfo->typ)
            return xsql_gen_error_inv_arg(txt, TYP_STRING, op->v_conninfo->typ);
    } else
        return xsql_gen_error(txt, "\" expected");
    op->provider_name = mkstr(provider_name.ptr, provider_name.len, 8);
    lst_adde(scr_main->conn_list, op);
    if (name.len) {
        op->name = mkstr(name.ptr, name.len, 8);
        return xsql_get_token(txt, token);
    } else
        op->name = mkstr(CONST_STR_LEN("default"), 8);
    return XSQL_OK;
}

static str_t *extract_query_char (xsql_source_t *txt, char *p, char e) {
    char *q = p;
    while (*p && *p != e) {
        if ('"' == *p || '\'' == *p) {
            char c = *p++;
            while (*p && *p != c) ++p;
            if (*p++ != c) {
                xsql_gen_error(txt, "end of string expected");
                return NULL;
            }
        } else
            ++p;
    }
    if (!(*p)) {
        xsql_gen_error(txt, "identifier expected");
        return NULL;
    }
    str_t *str = mkstr(q, (uintptr_t)p - (uintptr_t)q, 256);
    txt->text_ptr = p;
    return str;
}

static int is_endstr (char **pstr, const char *ep, size_t eplen) {
    char *p = *pstr;
    const char *e = ep + eplen;
    while (*p && ep < e) {
        if (' ' == *ep) {
            if (!isspace(*p))
                return 0;
        } else
        if (toupper(*p) != toupper(*ep))
            return 0;
        ++p; ++ep;
    }
    *pstr = p;
    return 1;
}

static str_t *extract_query_str (xsql_source_t *txt, char *p, const char *e, size_t elen) {
    char *q = p;
    while (!is_endstr(&p, e, elen)) {
        if ('"' == *p || '\'' == *p) {
            char c = *p++;
            while (*p && *p != c) ++p;
            if (*p != c) {
                xsql_gen_error(txt, "end of string expected");
                return NULL;
            }
        }
        ++p;
    }
    p -= elen;
    if (!(*p)) {
        xsql_gen_error(txt, "end of string expected");
        return NULL;
    }
    str_t *ret = mkstr(q, (uintptr_t)p - (uintptr_t)q, 256);
    txt->text_ptr = p;
    return ret;
}

static char *replace_param (xsql_script_t *scr, str_t **query, char *begin, char *end, int param_no) {
    xsql_t *scr_main = scr->scr_main;
    char *ph = scr_main->cur_prov->get_placeholder(param_no);
    size_t ph_len = strlen(ph), begin_len = (uintptr_t)begin-(uintptr_t)(*query)->ptr, dst_len = (uintptr_t)end-(uintptr_t)begin;
    strepl(query, begin, dst_len, ph, ph_len);
    free(ph);
    return (*query)->ptr + begin_len + ph_len;
}

static char *prepare_param (xsql_source_t *txt, xsql_script_t *scr, str_t **query, char *p, list_t *params, int param_no) {
    char *q = p++;
    while (*p && !isspace(*p) && !strchr(delim_chars, *p)) ++p;
    if (p == q) {
        xsql_gen_error(txt, "parmeter name expected");
        return NULL;
    }
    xsql_var_t *var = xsql_get_var(scr, q+1, (uintptr_t)p-(uintptr_t)q-1);
    if (var) {
        lst_adde(params, (void*)var);
        return replace_param(scr, query, q, p, param_no);
    }
    q = strndup(q+1, (uintptr_t)p-(uintptr_t)q-1);
    xsql_gen_error(txt, "variable \"%s\" not exists", q);
    free(q);
    return NULL;
}

static int prepare_params (xsql_source_t *txt, xsql_script_t *scr, str_t **query, list_t *params) {
    size_t param_no = 1;
    char *p = (*query)->ptr;
    while (*p) {
        while (*p && *p != '$' && *p != '"' && *p != '\'') ++p;
        switch (*p) {
            case '"':
                ++p; while (*p && *p != '"') ++p;
                if (!(*p++))
                    return xsql_gen_error(txt, "end of string expected");
                break;
            case '\'':
                ++p; while (*p && *p != '\'') ++p;
                if (!(*p++))
                    return xsql_gen_error(txt, "end of string expected");
                break;
            case '$':
                if (!(p = prepare_param(txt, scr, query, p, params, param_no)))
                    return XSQL_ERROR;
                ++param_no;
                break;
            default: break;
        }
    }
    return XSQL_OK;
}

static int xsql_parse_query (xsql_source_t *txt, xsql_script_t *scr, char *str, strptr_t *token, xsql_op_t **result) {
    xsql_t *scr_main = scr->scr_main;
    if (!scr_main->cur_prov)
        return xsql_gen_error(txt, "database provider is not defined");
    xsql_sqlexec_t *op = calloc(1, sizeof(xsql_sqlexec_t));
    *result = (xsql_op_t*)op;
    op->exec = xsql_sqlexec_exec;
    op->destroy = xsql_sqlexec_destroy;
    op->scr = scr;
    op->params = lst_alloc(NULL);
    if (!(op->query = extract_query_char(txt, str, ';')))
        return XSQL_ERROR;
    if (XSQL_ERROR == prepare_params(txt, scr, &op->query, op->params))
        return XSQL_ERROR;
    return xsql_get_token(txt, token);
}

static int parse_plot_style (strptr_t *token, xsql_plot_t *op) {
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("LINES"))) op->format = "lines"; else
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("POINTS"))) op->format = "points"; else
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("LINESPOINTS"))) op->format = "linespoints"; else
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("IMPULSES"))) op->format = "impulses"; else
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("DOTS"))) op->format = "dots"; else
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("STEPS"))) op->format = "steps"; else
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("ERRORBARS"))) op->format = "errorbars"; else
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("BOXES"))) op->format = "boxes"; else
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("BOXERRORBARS"))) op->format = "boxerrorbars"; else
        return XSQL_ERROR;
    return XSQL_OK;
}

static int parse_plot_format (strptr_t *token, xsql_plot_t *op) {
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("PNG"))) op->format = "png"; else
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("POSTSCRIPT"))) op->format = "postscript"; else
        return XSQL_ERROR;
    return XSQL_OK;
}

static int parse_plot_param (xsql_source_t *txt, strptr_t *token, xsql_plot_t *op) {
    if (XSQL_OK != xsql_get_token(txt, token))
        return xsql_gen_error(txt, "identifier expected");
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("FORMAT"))) {
        if (XSQL_OK != xsql_get_token(txt, token))
            return XSQL_ERROR;
        if (XSQL_ERROR == parse_plot_format(token, op))
            return xsql_gen_error(txt, "plot format error");
    } else
    if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("STYLE"))) {
        if (XSQL_OK != xsql_get_token(txt, token))
            return XSQL_ERROR;
        if (XSQL_ERROR == parse_plot_style(token, op))
            return xsql_gen_error(txt, "plot style error");
    } else
        return XSQL_FIN;
    return XSQL_OK;
}

static int xsql_parse_plot (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_op_t **result) {
    int ret;
    xsql_t *scr_main = scr->scr_main;
    if (!scr_main->cur_prov)
        return xsql_gen_error(txt, "database provider is not defined");
    xsql_plot_t *op = calloc(1, sizeof(xsql_plot_t));
    *result = (xsql_op_t*)op;
    op->exec = xsql_plot_exec;
    op->destroy = xsql_plot_destroy;
    op->scr = scr;
    while (XSQL_OK == (ret = parse_plot_param(txt, token, op)));
    if (XSQL_ERROR == ret) return ret;
    if (!op->format) op->format = "png";
    if (!op->style) op->style = "lines";
    strptr_t fstr = { .ptr = token->ptr, .len = token->len };
    if (!(op->query = extract_query_char(txt, fstr.ptr, ';')))
        return XSQL_ERROR;
    op->params = lst_alloc(NULL);
    if (XSQL_ERROR == prepare_params(txt, scr, &op->query, op->params))
        return XSQL_ERROR;
    return xsql_get_token(txt, token);
}

static int xsql_parse_loop (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_op_t **result) {
    xsql_loop_t *op = calloc(1, sizeof(xsql_loop_t));
    *result = (xsql_op_t*)op;
    op->exec = xsql_loop_exec;
    op->destroy = xsql_loop_destroy;
    op->scr = scr;
    op->params = lst_alloc(NULL);
    op->var_list = lst_alloc(NULL);
    int ret;
    while (XSQL_OK == (ret = xsql_get_token(txt, token))) {
        xsql_var_t *var = xsql_get_var(scr, token->ptr, token->len);
        if (!var)
            var = xsql_add_var(scr, token->ptr, token->len, TYP_STRING);
        else
            var->typ = TYP_STRING;
        lst_adde(op->var_list, var);
        if (XSQL_OK != xsql_get_token(txt, token))
            return xsql_gen_error(txt, "identifier expected");
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("IN")))
            break;
        if (0 != cmpstr(token->ptr, token->len, CONST_STR_LEN(",")))
            return xsql_gen_error(txt, "\",\" or \"IN\" expected");
    }
    if (op->var_list->len > 0) {
        op->svals_len = op->var_list->len;
        op->svals = calloc(op->svals_len, sizeof(str_t*));
    }
    if (XSQL_OK != xsql_get_token(txt, token) && 0 != cmpcasestr(token->ptr, token->len, CONST_STR_LEN("IN")))
        return xsql_gen_error(txt, "\"IN\" expected");
    if (!(op->query = extract_query_str(txt, token->ptr, CONST_STR_LEN(" DO "))))
        return XSQL_ERROR;
    if (XSQL_ERROR == prepare_params(txt, scr, &op->query, op->params))
        return XSQL_ERROR;
    if (XSQL_OK != (ret = xsql_get_token(txt, token)) || 0 != cmpcasestr(token->ptr, token->len, CONST_STR_LEN("DO")))
        return xsql_gen_error(txt, "\"DO\" expected");
    if (!(op->op_body = xsql_parse_block(txt, scr, token, NULL, &endloop_token, 1)) && txt->err)
        return XSQL_ERROR;
    if (0 != cmpcasestr(token->ptr, token->len, CONST_STR_LEN("END")))
        return xsql_gen_error(txt, "\"END\" expected");
    return xsql_get_token(txt, token);
}

static int xsql_parse_desc (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token) {
    if (XSQL_OK == xsql_get_token(txt, token) && token->len > 2 && '"' == *(token->ptr) && '"' == *(token->ptr+token->len-1)) {
        xsql_t *scr_main = scr->scr_main;
        if ('"' != token->ptr[0] || '"' != token->ptr[token->len-1])
            return xsql_gen_error(txt, "\" expected");
        strput(&scr_main->name, token->ptr+1, token->len-2, 0);
        return xsql_get_token(txt, token);
    }
    return xsql_gen_error(txt, "description expected");
}

static int xsql_parse_newtab (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_op_t **result) {
    int ret;
    xsql_newtab_t *op = calloc(1, sizeof(xsql_newtab_t));
    *result = (xsql_op_t*)op;
    op->scr = scr;
    op->exec = xsql_newtab_exec;
    op->destroy = xsql_newtab_destroy;
    if (XSQL_OK == (ret = xsql_get_token(txt, token))) {
        if ('"' == token->ptr[0] && '"' == token->ptr[token->len-1]) {
            strptr_t name = { .ptr = token->ptr+1, .len = token->len-2 };
            if (token->len > 0)
                op->name = mkstr(name.ptr, name.len, 8);
        } else
            return xsql_gen_error(txt, "tab name error");
    }
    return xsql_get_token(txt, token);
}

static int xsql_parse_use_connection (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_op_t **result) {
    int ret;
    xsql_t *scr_main = scr->scr_main;
    xsql_use_t *op = calloc(1, sizeof(xsql_use_t));
    *result = (xsql_op_t*)op;
    op->scr = scr;
    op->exec = xsql_use_exec;
    op->destroy = xsql_use_destroy;
    if (token->len >= 2 && XSQL_OK == (ret = xsql_get_token(txt, token)) && '"' == token->ptr[0] && '"' == token->ptr[token->len-1]) {
        strptr_t name = { .ptr = token->ptr+1, .len = token->len-2 };
        op->connection_name = mkstr(name.ptr, name.len, 8);
        list_item_t *li = scr_main->conn_list->head;
        if (!li)
            return xsql_gen_error(txt, "provider not found");
        do {
            xsql_connect_t *op_conn = (xsql_connect_t*)li->ptr;
            str_t *test_conn_name = op_conn->name;
            if (0 == cmpstr(test_conn_name->ptr, test_conn_name->len, name.ptr, name.len)) {
                op->op_conn = op_conn;
                op->provider = op_conn->provider;
                break;
            }
            li = li->next;
        } while (li != scr_main->conn_list->head);
        if (!op->provider)
            return xsql_gen_error(txt, "provider not found");
        return xsql_get_token(txt, token);
    }
    return xsql_gen_error(txt, "provider name error");
}

static xsql_op_t *xsql_parse_block (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token, xsql_op_t **op_last, strptr_t *end_tokens, size_t end_tokens_cnt) {
    int ret, is_skip_op = 0;
    xsql_t *scr_main = scr->scr_main;
    xsql_op_t *op = NULL, **op_next = &op;
    xsql_dbprov_t *old_prov = scr_main->cur_prov;
    if (op_last) *op_last = NULL;
    while (XSQL_OK == (ret = xsql_get_token(txt, token))) {
        if (is_token_end(token, end_tokens, end_tokens_cnt))
            return op;
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("IF"))) {
            if (XSQL_ERROR == (ret = xsql_parse_if(txt, scr, token, op_next)))
                goto err;
        } else
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("FOR"))) {
            if (XSQL_ERROR == (ret = xsql_parse_for(txt, scr, token, op_next)))
                goto err;
        } else
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("WHILE"))) {
            if (XSQL_ERROR == (ret = xsql_parse_while(txt, scr, token, op_next)))
                goto err;
        } else
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("PRINT"))) {
            if (XSQL_ERROR == (ret = xsql_parse_print(txt, scr, token, op_next)))
                goto err;
        } else
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("STORE"))) {
            if (XSQL_ERROR == (ret = xsql_parse_store(txt, scr, token, op_next)))
                goto err;
        } else
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("LOAD"))) {
            if (XSQL_ERROR == (ret = xsql_parse_load(txt, scr, token, op_next)))
                goto err;
        } else
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("CONNECT"))) {
            if (XSQL_ERROR == (ret = xsql_parse_connect(txt, scr, token, op_next)))
                goto err;
            old_prov = ((xsql_connect_t*)op_next)->provider;
        } else
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("LOOP"))) {
            if (XSQL_ERROR == (ret = xsql_parse_loop(txt, scr, token, op_next)))
                goto err;
        } else
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("CASE"))) {
            if (XSQL_ERROR == (ret = xsql_parse_case(txt, scr, token, op_next)))
                goto err;
        } else
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("PLOT"))) {
            if (XSQL_ERROR == (ret = xsql_parse_plot(txt, scr, token, op_next)))
                goto err;
        } else
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("NEWTAB"))) {
            if (XSQL_ERROR == (ret = xsql_parse_newtab(txt, scr, token, op_next)))
                goto err;
        } else
        if (0 == cmpcasestr(token->ptr, token->len, CONST_STR_LEN("USE"))) {
            if (XSQL_ERROR == (ret = xsql_parse_use_connection(txt, scr, token, op_next)))
                goto err;
        } else {
            // params
            strptr_t name = { .ptr = token->ptr, .len = token->len };
            if ('.' == *(token->ptr)) {
                name.ptr++; name.len--;
                is_skip_op = 1;
                if (0 == cmpcasestr(name.ptr, name.len, CONST_STR_LEN("NAME"))) {
                    if (XSQL_ERROR == (ret = xsql_parse_desc(txt, scr, token)))
                        goto err;
                } else
                if (0 == cmpcasestr(name.ptr, name.len, CONST_STR_LEN("INPUT"))) {
                    if (XSQL_ERROR == (ret = parse_params(txt, scr, token)))
                        goto err;
                } else {
                    xsql_gen_error(txt, "unknown identifier");
                    goto err;
                }
            } else
            if (XSQL_ERROR != (ret = xsql_get_token(txt, token))) {
                if (0 == cmpstr(token->ptr, token->len, CONST_STR_LEN("="))) {
                    if (XSQL_ERROR == (ret = xsql_parse_assign(txt, scr, &name, token, op_next)))
                        goto err;
                } else
                if (XSQL_ERROR == xsql_parse_query(txt, scr, name.ptr, token, op_next))
                    goto err;
            } else {
                    xsql_gen_error(txt, "unknown error");
                    goto err;
            }
        }
        if (0 != cmpstr(token->ptr, token->len, CONST_STR_LEN(";"))) {
            xsql_gen_error(txt, "\";\" expected");
            goto err;
        }
        if (!is_skip_op) {
            if (op_last) *op_last = *op_next;
            op_next = &(*op_next)->next;
        }
        is_skip_op = 0;
    }
    scr_main->cur_prov = old_prov;
    return op;
err:
    xsql_op_free(op);
    return NULL;
}

static void input_free (void *dummy, xsql_param_t *p) {
    free(p->prompt);
    free(p->var_name);
    free(p);
}

static void keyval_free (void *dummy, keyval_t *p) {
    free(p->key);
    free(p->val);
    free(p);
}

int xsql_parse (xsql_source_t *txt, xsql_t *scr) {
    strptr_t token;
    scr->scr_main = (xsql_t*)scr;
    scr->var_list = lst_alloc((free_h)var_clear);
    scr->param_list = lst_alloc((free_h)eval_free);
    scr->input_param_list = lst_alloc((free_h)input_free);
    scr->conn_list = lst_alloc(NULL);
    scr->files = lst_alloc((free_h)keyval_free);
    scr->name = mkstr(CONST_STR_LEN("<unnamed>"), 64);
    scr->op = xsql_parse_block(txt, (xsql_script_t*)scr, &token, NULL, NULL, 0);
    return scr->op ? 0 : -1;
}
