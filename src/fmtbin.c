#include "fmt.h"

typedef struct {
    str_t *name;
    str_t *fname;
    xsql_fmtprov_t *prov;
    int flags;
    int fd;
} xsql_bin_t;

static const char *bin_fmtname () {
    return "binary";
}

static xsql_fmt_t *bin_open (xsql_fmtprov_t *prov, const char *fname, size_t fname_len, const char *name, size_t name_len, int flags) {
    xsql_bin_t *res = calloc(1, sizeof(xsql_bin_t));
    res->name = mkstr(name, name_len, 8);
    res->fname = prov->fmtwrtname(fname, fname_len);
    res->prov = prov;
    res->flags = flags;
    switch (flags) {
        case XSQL_READ:
            res->fd = open(fname, O_RDONLY);
            break;
        case XSQL_WRITE:
            res->fd = open(fname, O_CREAT | O_TRUNC | O_WRONLY, 0644);
            break;
    }
    if (-1 == res->fd) {
        free(res->fname);
        free(res->name);
        free(res);
        return NULL;
    }
    if (XSQL_WRITE == res->flags) {
        strbuf_t msg;
        msg_alloc(&msg, 8, 8);
        msg_addi32(&msg, FMT_BEGIN);
        if (msg.len != write(res->fd, msg.ptr, msg.len)) {
            msg_destroy(&msg);
            if (res->fname) free(res->fname);
            if (res->name) free(res->name);
            free(res);
            return NULL;
        }
        msg_destroy(&msg);
    }
    return (xsql_fmt_t*)res;
}

static void bin_start_outrec (xsql_fmt_t *fmt, xsql_stmt_t *stmt) {
    xsql_bin_t *bin = (xsql_bin_t*)fmt;
    strbuf_t msg;
    msg_alloc(&msg, 128, 128);
    msg_addi32(&msg, FMT_BEGIN_DATA);
    msg_addi32(&msg, stmt->nflds);
    for (int i = 0; i < stmt->nflds; ++i) {
        str_t *fldname = stmt->fldnams[i];
        msg_addstr(&msg, fldname->ptr, fldname->len);
    }
    write(bin->fd, msg.ptr, msg.len);
    msg_destroy(&msg);
}

static void bin_outrec (xsql_fmt_t *fmt, xsql_stmt_t *stmt) {
    xsql_bin_t *bin = (xsql_bin_t*)fmt;
    strbuf_t msg;
    msg_alloc(&msg, 256, 256);
    msg_addi32(&msg, FMT_RECORD);
    for (int i = 0; i < stmt->nflds; ++i) {
        char *s = stmt->result[i];
        if (s)
            msg_addstr(&msg, s, strlen(s));
        else
            msg_addstr(&msg, NULL, 0);
    }
    write(bin->fd, msg.ptr, msg.len);
    msg_destroy(&msg);
}

static void bin_end_outrec (xsql_fmt_t *fmt, xsql_stmt_t *stmt) {
    xsql_bin_t *bin = (xsql_bin_t*)fmt;
    strbuf_t msg;
    msg_alloc(&msg, 8, 8);
    msg_addi32(&msg, FMT_END_DATA);
    write(bin->fd, msg.ptr, msg.len);
    msg_destroy(&msg);
}

static void bin_start_outvar (xsql_fmt_t *fmt) {
    xsql_bin_t *bin = (xsql_bin_t*)fmt;
    strbuf_t msg;
    msg_alloc(&msg, 8, 8);
    msg_addi32(&msg, FMT_NODATA);
    write(bin->fd, msg.ptr, msg.len);
    msg_destroy(&msg);
}

static void bin_outvar (xsql_fmt_t *fmt, xsql_var_t *var, size_t index) {
}

static void bin_end_outvar (xsql_fmt_t *fmt) {
}

static int loadmsg (xsql_bin_t *bin, strbuf_t *msg) {
    msglen_t len;
    if (sizeof(msglen_t) != read(bin->fd, &len, sizeof(msglen_t)))
        return -1;
    if (-1 == strbufalloc(msg, len + sizeof(msglen_t), 64))
        return -1;
    if (len != read(bin->fd, msg->ptr + sizeof(msglen_t), len)) {
        free(msg->ptr);
        msg->ptr = NULL;
        return -1;
    }
    return msg_parse(msg, 0);
}

static int bin_read (xsql_fmt_t *fmt, xsql_fmt_read_t *data) {
    strbuf_t msg;
    int32_t n;
    int rc = loadmsg((xsql_bin_t*)fmt, &msg);
    if (rc < 0)
        return rc;
    msg_geti32(&msg, &n);
    switch (n) {
        case FMT_BEGIN_DATA:
            if (FMT_RECORD == data->typ || FMT_BEGIN_DATA == data->typ) {
                rc = -1;
                break;
            }
            data->typ = n;
            msg_geti32(&msg, &n);
            data->len = n;
            if (data->ptr)
                free(data->ptr);
            if (n > 0) {
                data->ptr = malloc(sizeof(strptr_t)*n);
                for (int i = 0; i < n; ++i)
                    msg_getstrcpy(&msg, &data->ptr[i]);
            }
            break;
        case FMT_RECORD:
            if (FMT_RECORD != data->typ && FMT_BEGIN_DATA != data->typ) {
                rc = -1;
                break;
            }
            data->typ = n;
            for (int i = 0; i < data->len; ++i) {
                if (data->ptr[i].ptr)
                    free(data->ptr[i].ptr);
                msg_getstrcpy(&msg, &data->ptr[i]);
            }
            break;
        case FMT_END_DATA:
            if (FMT_RECORD != data->typ && FMT_BEGIN_DATA != data->typ) {
                rc = -1;
                break;
            }
            data->typ = n;
            if (data->ptr) {
                for (int i = 0; i < data->len; ++i)
                    if (data->ptr[i].ptr)
                        free(data->ptr[i].ptr);
                free(data->ptr);
            }
            data->ptr = NULL;
            data->len = 0;
            break;
        case FMT_BEGIN:
        case FMT_NODATA:
        case FMT_END:
            data->typ = n;
            break;
        default:
            rc = -1;
    }
    if (-1 ==rc) {
        if (data->ptr) {
            for (int i = 0; i < data->len; ++i)
                free(data->ptr[i].ptr);
            free(data->ptr);
            data->ptr = NULL;
            data->len = 0;
        }
    }
    msg_destroy(&msg);
    return rc;
}

static void bin_close (xsql_fmt_t *fmt) {
    xsql_bin_t *bin = (xsql_bin_t*)fmt;
    if (XSQL_WRITE == fmt->flags) {
        strbuf_t msg;
        msg_alloc(&msg, 8, 8);
        msg_addi32(&msg, FMT_END);
        write(bin->fd, msg.ptr, msg.len);
        msg_destroy(&msg);
    }
    if (bin->fname)
        free(bin->fname);
    if (bin->name)
        free(bin->name);
    if (bin->fd > 0)
        close(bin->fd);
    free(bin);
}

xsql_fmtprov_t *xsql_create_bin_provider () {
    xsql_fmtprov_t *prov = calloc(1, sizeof(xsql_fmtprov_t));
    prov->fmtname = bin_fmtname;
    prov->fmtopen = bin_open;
    prov->fmtstart_outrec = bin_start_outrec;
    prov->fmtoutrec = bin_outrec;
    prov->fmtend_outrec = bin_end_outrec;
    prov->fmtstart_outvar = bin_start_outvar;
    prov->fmtoutvar = bin_outvar;
    prov->fmtend_outvar = bin_end_outvar;
    prov->fmtclose = bin_close;
    prov->fmtwrtname = file_wrtname;
    prov->fmtread = bin_read;
    return prov;
}
