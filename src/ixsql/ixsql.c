#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <libex/file.h>
#include <libex/tree.h>
#include "linenoise.h"
#include "xsql.h"

const char *filename = NULL;
list_t *arg_list = NULL;

static const char *stdout_fmtname () {
    return "stdout";
}

static int stdout_wrtopen (xsql_fmt_t *fmt) {
    return 0;
}

str_t *stdout_wrtname (const char *fname, size_t fname_len) {
    return NULL;
}

int stdout_wrtvar (xsql_fmt_t *fmt, xsql_var_t *var, size_t index) {
    const char *str;
    size_t str_len;
    if (TYP_STRING == var->typ) {
        str = (const char*)var->raw.s_val->ptr;
        str_len = var->raw.s_val->len;
    } else {
        str = xsql_varstr(var);
        str_len = strlen(str);
    }
    write(STDOUT_FILENO, str, str_len);
    return 0;
}

int stdout_wrtstr (xsql_fmt_t *fmt, const char *str, size_t str_len, size_t index) {
    return str_len == write(STDOUT_FILENO, str, str_len) ? 0 : -1;
}

void stdout_wrtclose (xsql_fmt_t *fmt) {
}

int stdout_wrtend (xsql_fmt_t *fmt) {
    write(STDOUT_FILENO, CONST_STR_LEN("\n"));
    return 0;
}

static int parse_args (int argc, const char *argv[]) {
    int rc = -1;
    if (argc > 1) {
        filename = strdup(argv[1]);
        rc = 0;
    }
    return rc;
}

static xsql_fmtprov_t *create_stdout_provider () {
    xsql_fmtprov_t *prov = xsql_create_text_provider();
    prov->fmtname = stdout_fmtname;
    prov->fmtwrtname = stdout_wrtname;
    prov->fmtwrtopen = stdout_wrtopen;
    prov->fmtwrtvar = stdout_wrtvar;
    prov->fmtwrtstr = stdout_wrtstr;
    prov->fmtwrtend = stdout_wrtend;
    prov->fmtwrtclose = stdout_wrtclose;
    return prov;
}

static int init () {
    str_t *ps = NULL, *fs = NULL;
    load_conf("xsql.rc", CONF_HANDLER
        ASSIGN_CONF_STR(ps, "providers")
        ASSIGN_CONF_STR(fs, "formatters")
    CONF_HANDLER_END);
    if (ps) {
        str_t *msgs = xsql_dbinit(ps->ptr, ps->len);
        if (msgs) {
            printf("%s\n", msgs->ptr);
            free(msgs);
        }
        free(ps);
    }
    if (fs) {
        str_t *msgs = xsql_fmtinit(fs->ptr, fs->len);
        if (msgs) {
            printf("%s\n", msgs->ptr);
            free(msgs);
        }
        free(fs);
    } else {
        str_t *msgs = xsql_fmtinit(CONST_STR_NULL);
        if (msgs) {
            printf("%s\n", msgs->ptr);
            free(msgs);
        }
    }
    fmtstd_prov = create_stdout_provider();
    lst_adde(fmtprovs, fmtstd_prov);
    return 0;
}

static void done () {
    if (arg_list)
        lst_free(arg_list);
    if (filename) free((void*)filename);
    xsql_fmtdone();
    xsql_dbdone();
}

static void print_out (list_t *l) {
    list_item_t *li = l->head;
    if (!li) return;
    do {
        keyval_t *kv = (keyval_t*)li->ptr;
        printf("%s -> %s\n", kv->key->ptr, kv->val->ptr);
        li = li->next;
    } while (li != l->head);
}

static void input_params (xsql_t *scr, int argc, const char *argv[]) {
    int i = 2;
    if (!arg_list)
        arg_list = lst_alloc(on_default_free_item);
    if (0 == scr->input_param_list->len)
        return;
    list_item_t *li = scr->input_param_list->head;
    do {
        xsql_param_t *param = (xsql_param_t*)li->ptr;
        if (i < argc)
            xsql_add_arg_ex(scr, arg_list, argv[i], param->typ);
        else {
            size_t prompt_len = param->prompt->len + 2;
            char *prompt = malloc(prompt_len);
            snprintf(prompt, prompt_len, "%s: ", param->prompt->ptr);
            xsql_add_arg_ex(scr, arg_list, linenoise(prompt, NULL), param->typ);
            free(prompt);
        }
        ++i;
        li = li->next;
    } while (li != scr->input_param_list->head);
}

int main (int argc, const char *argv[]) {
    int ret;
    if (-1 == parse_args(argc, argv))
        return 1;
    if (-1 == init())
        return 1;
    xsql_source_t txt;
    if (XSQL_OK == (ret = xsql_load_file(filename, &txt))) {
        xsql_t scr = { .fmtstd = fmtstd_prov->fmtopen(fmtstd_prov, CONST_STR_NULL, CONST_STR_NULL, 0) };
        if (XSQL_OK == (ret = xsql_parse(&txt, &scr))) {
            input_params(&scr, argc, argv);
            if (XSQL_OK == xsql_script_exec(&scr, arg_list))
                print_out(scr.files);
            else
                fprintf(stderr, "%s\n", scr.err);
        } else
            fprintf(stderr, "line %d %s\n", txt.line_no, txt.err);
        xsql_script_destroy(&scr);
    } else
        fprintf(stderr, "%s\n", txt.err);
    xsql_source_destroy(&txt);
    done();
    return 0;
}
