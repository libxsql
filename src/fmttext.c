#include "fmt.h"

static const char *text_fmtname () {
    return "text";
}

static xsql_fmt_t *text_open (xsql_fmtprov_t *prov, const char *fname, size_t fname_len, const char *name, size_t name_len, int flags) {
    xsql_text_t *ret = calloc(1, sizeof(xsql_text_t));
    if (name && name_len > 0)
        ret->name = mkstr(name, name_len, 8);
    ret->fname = prov->fmtwrtname(fname, fname_len);
    ret->prov = prov;
    ret->result = NULL;
    ret->fldlen = 0;
    ret->flags = flags;
    if (-1 == prov->fmtwrtopen((xsql_fmt_t*)ret)) {
        if (ret->fname) free(ret->fname);
        free(ret);
        return NULL;
    }
    return (xsql_fmt_t*)ret;
}

static void add_result (xsql_text_t *txt, xsql_stmt_t *stmt) {
    str_t **res = malloc(stmt->nflds * sizeof(str_t*));
    for (int i = 0; i < stmt->nflds; ++i) {
        char *s = stmt->result[i];
        str_t *val;
        if (s)
            val = res[i] = mkstr(s, strlen(s), 32);
        else
            val = res[i] = mkstr(CONST_STR_LEN("null"), 32);
        if (val->len > txt->fldlen[i])
            txt->fldlen[i] = val->len;
    }
    lst_adde(txt->result, (void*)res);
}

static void text_start_outrec (xsql_fmt_t *fmt, xsql_stmt_t *stmt) {
    xsql_text_t *txt = (xsql_text_t*)fmt;
//    xsql_fmtprov_t *prov = txt->prov;
    txt->result = lst_alloc(NULL);
    txt->fldlen = malloc(stmt->nflds * sizeof(int));
    for (int i = 0; i < stmt->nflds; ++i)
        txt->fldlen[i] = stmt->fldnams[i]->len;
//    add_result(txt, stmt);
}

static void text_outrec (xsql_fmt_t *fmt, xsql_stmt_t *stmt) {
    add_result((xsql_text_t*)fmt, stmt);
}

static void text_end_outrec (xsql_fmt_t *fmt, xsql_stmt_t *stmt) {
    xsql_text_t *txt = (xsql_text_t*)fmt;
    xsql_fmtprov_t *prov = txt->prov;
    str_t *str = stralloc(256, 32);
    for (int i = 0; i < stmt->nflds; ++i) {
        str_t *fldnam = stmt->fldnams[i];
        strput(&str, fldnam->ptr, fldnam->len, 0);
        strpad(&str, txt->fldlen[i], ' ', STR_CENTER);
        prov->fmtwrtstr(fmt, str->ptr, str->len, 0);
        prov->fmtwrtstr(fmt, CONST_STR_LEN(" "), 0);
    }
    prov->fmtwrtend(fmt);
    for (int i = 0; i < stmt->nflds; ++i) {
        str->len = 0;
        strpad(&str, txt->fldlen[i], '-', STR_LEFT);
        prov->fmtwrtstr(fmt, str->ptr, str->len, 0);
        prov->fmtwrtstr(fmt, CONST_STR_LEN(" "), 0);
    }
    prov->fmtwrtend(fmt);
    free(str);
    lst_enum(txt->result, ({
        int fn (list_item_t *li, void *dummy) {
            str_t **res = (str_t**)li->ptr;
            for (int i = 0; i < stmt->nflds; ++i) {
                strpad(&res[i], txt->fldlen[i], ' ', STR_LEFT);
                prov->fmtwrtstr(fmt, res[i]->ptr, res[i]->len, 0);
                prov->fmtwrtstr(fmt, CONST_STR_LEN(" "), 0);
                free(res[i]);
            }
            free(res);
            prov->fmtwrtend(fmt);
            return ENUM_CONTINUE;
        } fn;
    }), NULL, 0);
    lst_free(txt->result);
    free(txt->fldlen);
    char buf [64];
    snprintf(buf, sizeof buf, " %d row(s)\n", stmt->nrecs);
    prov->fmtwrtstr(fmt, buf, strlen(buf), 0);
    txt->result = NULL;
    txt->fldlen = NULL;
}

static void text_start_outvar (xsql_fmt_t *fmt) {
}

static void text_outvar (xsql_fmt_t *fmt, xsql_var_t *var, size_t index) {
    fmt->prov->fmtwrtvar(fmt, var, 0);
}

static void text_end_outvar (xsql_fmt_t *fmt) {
    fmt->prov->fmtwrtend(fmt);
}

static void text_close (xsql_fmt_t *fmt) {
    fmt->prov->fmtwrtclose(fmt);
    if (fmt->fname) free(fmt->fname);
    if (fmt->name) free(fmt->name);
    free(fmt);
}

xsql_fmtprov_t *xsql_create_text_provider () {
    xsql_fmtprov_t *ret = malloc(sizeof(xsql_fmtprov_t));
    ret->hnd = NULL;
    ret->fmtname = text_fmtname;
    ret->fmtwrtname = file_wrtname;
    ret->fmtopen = text_open;
    ret->fmtwrtopen = file_wrtopen;
    ret->fmtstart_outrec = text_start_outrec;
    ret->fmtoutrec = text_outrec;
    ret->fmtend_outrec = text_end_outrec;
    ret->fmtstart_outvar = text_start_outvar;
    ret->fmtwrtvar = file_wrtvar;
    ret->fmtwrtstr = file_wrtstr;
    ret->fmtoutvar = text_outvar;
    ret->fmtend_outvar = text_end_outvar;
    ret->fmtwrtend = file_wrtend;
    ret->fmtclose = text_close;
    ret->fmtwrtclose = file_wrtclose;
    ret->fmtend = NULL;
    ret->fmtnewtab = NULL;
    ret->fmtsetstyle = NULL;
    return ret;
}
