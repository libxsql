#include <stdlib.h>
#include <libex/str.h>
#include <mysql.h>
#include "var.h"
#include "db.h"

typedef void (*convert_h) (char **in, size_t fldno, char **out, unsigned long out_len);
typedef struct {
    MYSQL_BIND *params;
    size_t param_count;
    MYSQL_RES *res;
    MYSQL_BIND *columnes;
    my_bool *param_nulls;
    my_bool *nulls;
    my_bool *errors;
    unsigned long *lengths;
    unsigned long *param_lengths;
    unsigned long *max_lengths;
    char **result;
    convert_h *converts;
} mysql_t;

const char *get_provider_name () {
    return "mysql";
}

char *get_placeholder (int n) {
    return strdup("?");
}

xsql_conn_t *dbopen (xsql_dbprov_t *provider, const char *name, size_t name_len, const char *conninfo, size_t conninfo_len) {
    char *host = NULL, *dbname = NULL, *user = NULL, *pass = NULL;
    xsql_conn_t *conn = calloc(1, sizeof(xsql_conn_t));
    conn->name = mkstr(name, name_len, 8);
    conn->provider = provider;
    if (-1 == parse_conninfo((char*)conninfo, conninfo_len, &host, &dbname, &user, &pass)) {
        char buf [strlen(conninfo) + 64];
        snprintf(buf, sizeof(buf), "connection name \"%s\" error", conninfo);
        conn->msg = strdup(buf);
    } else {
        conn->hdb = malloc(sizeof(MYSQL));
        mysql_init((MYSQL*)conn->hdb);
        if (!mysql_real_connect((MYSQL*)conn->hdb, host, user, pass, dbname, 0, NULL, 0))
            conn->msg = strdup(mysql_error((MYSQL*)conn->hdb));
    }
    if (pass) free(pass);
    if (user) free(user);
    if (dbname) free(dbname);
    if (host) free(host);
    return conn;
}

xsql_stmt_t *sqlprepare (xsql_conn_t *conn, xsql_stmt_t *curstmt, const char *cmd, size_t cmd_len, size_t param_count) {
    xsql_stmt_t *stmt = NULL;
    int mysql_ret = 1;
    if (curstmt) {
        stmt = curstmt;
        mysql_ret = mysql_stmt_reset((MYSQL_STMT*)stmt->hstmt);
        stmt->nrecs = 0;
    } else {
        stmt = calloc(1, sizeof(xsql_stmt_t));
        MYSQL_STMT *my_stmt = stmt->hstmt = mysql_stmt_init((MYSQL*)conn->hdb);
        mysql_t *data = stmt->data = calloc(1, sizeof(mysql_t));
        if (0 == (mysql_ret = mysql_stmt_prepare(my_stmt, cmd, cmd_len))) {
            data->param_count = mysql_stmt_param_count(my_stmt);
            if (param_count == data->param_count) {
                if (param_count > 0) {
                    data->params = malloc(data->param_count * sizeof(MYSQL_BIND));
                    data->param_nulls = malloc(data->param_count * sizeof(my_bool));
                    data->param_lengths = malloc(data->param_count * sizeof(unsigned long));
                }
                if ((data->res = mysql_stmt_result_metadata(my_stmt))) {
                    stmt->nflds = mysql_num_fields(data->res);
                    stmt->fldnams = malloc(stmt->nflds * sizeof(str_t*));
                    stmt->result = malloc(stmt->nflds * sizeof(char*));
                    data->columnes = malloc(stmt->nflds * sizeof(MYSQL_BIND));
                    data->nulls = malloc(stmt->nflds * sizeof(my_bool));
                    data->errors = malloc(stmt->nflds * sizeof(my_bool));
                    data->lengths = malloc(stmt->nflds * sizeof(unsigned long));
                    data->max_lengths = malloc(stmt->nflds * sizeof(unsigned long));
                    data->result = malloc(stmt->nflds * sizeof(char*));
                    data->converts = malloc(stmt->nflds * sizeof(convert_h));
                }
                stmt->conn = conn;
            } else
                mysql_ret = 0;
        }
    }
    if (0 != mysql_ret)
        stmt->msg = strdup(mysql_stmt_error((MYSQL_STMT*)stmt->hstmt));
    return stmt;
}

static int set_params (xsql_stmt_t *stmt, list_t *params) {
    int mysql_ret = 0;
    list_item_t *li = params->head;
    if (li) {
        size_t param_no = 0;
        mysql_t *data = (mysql_t*)stmt->data;
        MYSQL_STMT *my_stmt = (MYSQL_STMT*)stmt->hstmt;
        memset(data->param_nulls, 0, data->param_count * sizeof(my_bool));
        do {
            xsql_var_t *var = (xsql_var_t*)li->ptr;
            MYSQL_BIND *bind = &data->params[param_no];
            memset(bind, 0, sizeof(MYSQL_BIND));
            switch (var->typ) {
                case TYP_NONE:
                    data->param_nulls[param_no] = 1;
                    bind->is_null = &data->param_nulls[param_no];
                    break;
                case TYP_INT:
                    bind->buffer_type = MYSQL_TYPE_LONG;
                    bind->buffer = (char*)&var->raw.i_val;
                    break;
                case TYP_FLOAT:
                    bind->buffer_type = MYSQL_TYPE_DOUBLE;
                    bind->buffer = (char*)&var->raw.f_val;
                    break;
                case TYP_STRING:
                    bind->buffer_type = MYSQL_TYPE_STRING;
                    bind->buffer = var->raw.s_val->ptr;
                    data->param_lengths[param_no] = var->raw.s_val->len;
                    bind->length = &data->param_lengths[param_no];
                    bind->buffer_length = var->raw.s_val->len;
                    break;
                default: break;
            }
            if (!(mysql_ret = mysql_stmt_bind_param(my_stmt, bind)))
                break;
            ++param_no;
            li = li->next;
        } while (li != params->head);
    }
    return mysql_ret;
}

static void get_none (char **in, size_t fldno, char **out, unsigned long out_len) {
    *out[0] = '\0';
}

static void get_tiny (char **in, size_t fldno, char **out, unsigned long out_len) {
    snprintf(out[fldno], out_len, "%hhd", *in[fldno]);
}

static void get_smallint (short int **in, size_t fldno, char **out, unsigned long out_len) {
    snprintf(out[fldno], out_len, "%hd", *in[fldno]);
}

static void get_longint (int **in, size_t fldno, char **out, unsigned long out_len) {
    snprintf(out[fldno], out_len, "%d", *in[fldno]);
}

static void get_longlong (int64_t **in, size_t fldno, char **out, unsigned long out_len) {
    snprintf(out[fldno], out_len, LONG_FMT, *in[fldno]);
}

static void get_double (double **in, size_t fldno, char **out, unsigned long out_len) {
    snprintf(out[fldno], out_len, "%f", *in[fldno]);
}

static void get_float (float **in, size_t fldno, char **out, unsigned long out_len) {
    snprintf(out[fldno], out_len, "%f", *in[fldno]);
}

static void get_string (char **in, size_t fldno, char **out, unsigned long out_len) {
    strncpy(out[fldno], in[fldno], out_len);
    out[fldno][out_len] = '\0';
}

static void get_time (MYSQL_TIME **ts, size_t fldno, char **out, unsigned out_len) {
    MYSQL_TIME *t = ts[fldno];
    snprintf(out[fldno], out_len, "%04d-%02d-%02d %02d:%02d:%02d", t->year, t->month, t->day, t->hour, t->minute, t->second);
}

static int bind_result (xsql_stmt_t *stmt) {
    int mysql_ret;
    mysql_t *data = (mysql_t*)stmt->data;
    stmt->types = malloc(stmt->nflds * sizeof(xsql_type_t));
    for (size_t i = 0; i < stmt->nflds; ++i) {
        MYSQL_FIELD *field = mysql_fetch_field(data->res);
        MYSQL_BIND *column = &data->columnes[i];
        stmt->fldnams[i] = mkstr(field->name, field->name_length, 8);
        column->buffer_type = field->type;
        column->buffer_length = field->length;
        column->buffer = data->result[i] = malloc(column->buffer_length+4);
        data->max_lengths[i] = field->length;
        data->lengths[i] = field->length;
        stmt->result[i] = malloc(field->length + 4);
        column->is_null = &data->nulls[i];
        column->length = &data->lengths[i];
        column->error = &data->errors[i];
        switch (field->type) {
            case MYSQL_TYPE_TINY: data->converts[i] = (convert_h)get_tiny; stmt->types[i] = TYP_INT; break;
            case MYSQL_TYPE_SHORT: data->converts[i] = (convert_h)get_smallint; stmt->types[i] = TYP_INT; break;
            case MYSQL_TYPE_LONG: data->converts[i] = (convert_h)get_longint; stmt->types[i] = TYP_INT; break;
            case MYSQL_TYPE_LONGLONG: data->converts[i] = (convert_h)get_longlong; stmt->types[i] = TYP_INT; break;
            case MYSQL_TYPE_FLOAT: data->converts[i] = (convert_h)get_float; stmt->types[i] = TYP_FLOAT; break;
            case MYSQL_TYPE_DOUBLE: data->converts[i] = (convert_h)get_double; stmt->types[i] = TYP_FLOAT; break;
            case MYSQL_TYPE_DATETIME:
            case MYSQL_TYPE_TIMESTAMP: data->converts[i] = (convert_h)get_time; stmt->types[i] = TYP_STRING; break;
            case MYSQL_TYPE_STRING:
            case MYSQL_TYPE_VAR_STRING: data->converts[i] = get_string; stmt->types[i] = TYP_STRING; break;
            default: data->converts[i] = get_none; stmt->types[i] = TYP_STRING; break; // FIXME
        }
    }
    if (0 != (mysql_ret = mysql_stmt_bind_result((MYSQL_STMT*)stmt->hstmt, data->columnes)))
        return mysql_ret;
    return mysql_stmt_store_result((MYSQL_STMT*)stmt->hstmt);
}

static void get_stmt_result (xsql_stmt_t *stmt) {
    mysql_t *data = (mysql_t*)stmt->data;
    ++stmt->nrecs;
    for (size_t i = 0; i < stmt->nflds; ++i)
        data->converts[i](data->result, i, stmt->result, data->max_lengths[i]);
}

int sqlexec (xsql_stmt_t *stmt, list_t *params) {
    int mysql_ret = 1;
    MYSQL_STMT *my_stmt = (MYSQL_STMT*)stmt->hstmt;
    mysql_t *data = (mysql_t*)stmt->data;
    if (data->res) {
        if (0 == (mysql_ret = set_params(stmt, params)) && 0 == (mysql_ret = mysql_stmt_execute(my_stmt))) {
            if (data->res && 0 == (mysql_ret = bind_result(stmt))) {
                mysql_ret = mysql_stmt_fetch(my_stmt);
                switch (mysql_ret) {
                    case 0: get_stmt_result(stmt); break;
                    case MYSQL_NO_DATA: mysql_stmt_reset(my_stmt); stmt->ret = XSQL_FIN;
                    default: break;
                }
            }
        }
    } else
        mysql_ret = mysql_stmt_execute(my_stmt);
    if (0 != mysql_ret && MYSQL_NO_DATA != mysql_ret) {
        stmt->msg = strdup(mysql_stmt_error(my_stmt));
        stmt->ret = XSQL_ERROR;
    }
    return stmt->ret;
}

int fetch (xsql_stmt_t *stmt) {
    MYSQL_STMT *my_stmt = (MYSQL_STMT*)stmt->hstmt;
    int ret = 0 == mysql_stmt_fetch(my_stmt) ? XSQL_OK : XSQL_FIN;
    if (XSQL_OK == ret)
        get_stmt_result(stmt);
    else
        mysql_stmt_reset(my_stmt);
    return ret;
}

int sqlexec_direct (xsql_conn_t *conn, xsql_stmt_t **curstmt, const char *cmd, size_t cmd_len, list_t *params) {
    xsql_stmt_t *stmt = *curstmt = sqlprepare(conn, *curstmt, cmd, cmd_len, params->len);
    if (!stmt->msg)
        return sqlexec(stmt, params);
    stmt->ret = XSQL_ERROR;
    return XSQL_ERROR;
}

void sqlclose (xsql_stmt_t *stmt) {
    mysql_t *data = (mysql_t*)stmt->data;
    if (data) {
        if (data->res) mysql_free_result(data->res);
        if (data->params) free(data->params);
        if (data->param_lengths) free(data->param_lengths);
        if (data->columnes) free(data->columnes);
        if (data->nulls) free(data->nulls);
        if (data->param_nulls) free(data->param_nulls);
        if (data->max_lengths) free(data->max_lengths);
        if (data->lengths) free(data->lengths);
        if (data->converts) free(data->converts);
        if (data->errors) free(data->errors);
        if (data->result) {
            for (size_t i = 0; i < stmt->nflds; ++i)
                free(data->result[i]);
            free(data->result);
        }
        free(data);
    }
    if (stmt->msg) free(stmt->msg);
    if (stmt->fldnams) {
        for (int i = 0; i < stmt->nflds; ++i)
            free(stmt->fldnams[i]);
        free(stmt->fldnams);
    }
    if (stmt->result) {
        for (int i = 0; i < stmt->nflds; ++i)
            free(stmt->result[i]);
        free(stmt->result);
    }
    if (stmt->types)
        free(stmt->types);
    if (stmt->hstmt) mysql_stmt_close((MYSQL_STMT*)stmt->hstmt);
    free(stmt);
}

void dbclose (xsql_conn_t *conn) {
    free(conn->name);
    if (conn->hdb) {
        mysql_close((MYSQL*)conn->hdb);
        free(conn->hdb);
    }
    if (conn->msg)
        free(conn->msg);
    free(conn);
}
