#!/bin/bash

sum=0
for n in $(dpkg -c $1 | awk '{print $3}')
do
    sum=$(($sum+$n))
done
echo "$1 ->" $((sum/1024)) "Kb"
