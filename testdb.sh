#!/bin/sh

sqlite3 ./test.db "
create table product (
    id          integer not null,
    name        varchar(50) not null,
    stat        varchar(15) not null,
    primary key (id)
);
insert into product values (1, 'hompag', 'active');
insert into product values (2, 'helper', 'active');
insert into product values (3, 'msword', 'stopped');
insert into product values (4, 'msaccess', 'stopped');
"
