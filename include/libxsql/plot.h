#ifndef __XSQL_PLOT_H__
#define __XSQL_PLOT_H__

#include <stdio.h>
#include <libex/str.h>
#include <libex/file.h>
#include "db.h"

typedef enum { PF_PNG = 0, PF_POSTSCRIPT } gnuplot_format_t;
typedef enum { PS_LINES = 0, PS_POINTS, PS_LINES_POINTS, PS_IMPULSES, PS_DOTS, PS_STEPS, PS_ERRORBARS, PS_BOXES, PS_BOXESERRORBARS } gnuplot_style_t;

typedef struct {
    gnuplot_format_t format;
    gnuplot_style_t style;
    xsql_stmt_t *stmt;
    str_t *cmd;
    str_t *xlabel;
    str_t *ylabel;
} gnuplot_t;
void gnuplot_perform (gnuplot_t *plot, str_t **result);

#endif // __XSQL_PLOT_H__
