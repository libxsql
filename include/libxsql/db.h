#ifndef __XSQL_DB_H__
#define __XSQL_DB_H__

#include <stdlib.h>
#include <stdint.h>
#include <dlfcn.h>
#include <libex/str.h>
#include <libex/list.h>
#include "var.h"

#define XSQL_VERSION "0.1"

typedef struct xsql_dbprov xsql_dbprov_t;
typedef struct xsql_conn xsql_conn_t;
typedef struct xsql_stmt xsql_stmt_t;

typedef const char* (*xsql_dbprov_name_h) ();
typedef char* (*xsql_placeholder_h) (int);
typedef xsql_conn_t* (*xsql_dbopen_h) (xsql_dbprov_t*, const char*, size_t, const char*, size_t);
typedef xsql_stmt_t* (*xsql_sqlprepare_h) (xsql_conn_t*, xsql_stmt_t*, const char*, size_t, size_t);
typedef int (*xsql_sqlexec_h) (xsql_stmt_t*, list_t*);
typedef int (*xsql_sqlexec_direct_h) (xsql_conn_t*, xsql_stmt_t**, const char*, size_t, list_t*);
typedef int (*xsql_fetch_h) (xsql_stmt_t*);
typedef void (*xsql_sqlclose_h) (xsql_stmt_t*);
typedef void (*xsql_dbclose_h) (xsql_conn_t*);

struct xsql_dbprov {
    void *hnd;
    str_t *name;
    xsql_placeholder_h get_placeholder;
    xsql_dbopen_h dbopen;
    xsql_sqlprepare_h prepare;
    xsql_sqlexec_h sqlexec;
    xsql_sqlexec_direct_h sqlexec_direct;
    xsql_fetch_h fetch;
    xsql_sqlclose_h sqlclose;
    xsql_dbclose_h dbclose;
};
extern list_t *dbprovs;
xsql_dbprov_t *xsql_find_provider (const char *name, size_t name_len);

str_t *xsql_dbinit (const char *conf_str, size_t conf_str_len);
void xsql_dbdone ();

typedef void* xsql_dbconn_t;
struct xsql_conn {
    xsql_dbprov_t *provider;
    str_t *name;
    xsql_dbconn_t hdb;
    void *data;
    char *msg;
};

typedef void* xsql_dbstmt_t;
typedef void (*get_field_value_h) (xsql_dbstmt_t, int, char**);
struct xsql_stmt {
    xsql_conn_t *conn;
    xsql_dbstmt_t hstmt;
    char *msg;
    int nflds;
    int nrecs;
    str_t **fldnams;
    char **result;
    void *data;
    int ret;
    xsql_type_t *types;
};

int parse_conninfo (char *conninfo, size_t conninfo_len, char **host, char **dbname, char **user, char **pass);

#endif // __XSQL_DB_H__
