#ifndef __XSQL_VAR_H__
#define __XSQL_VAR_H__

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <libex/array.h>
#include <libex/str.h>
#include <libex/tree.h>

#define XSQL_OK 0
#define XSQL_FIN 1
#define XSQL_NEXT 2
#define XSQL_COMMENT 3
#define XSQL_ERROR -1

#if __x86_64 || __ppc64__
#define FMT_INT "%ld"
#else
#define FMT_INT "%lld"
#endif

#define MAX_VAR_NAME_LEN 64
#define NULLSTR "(null)"
#define SYS_STATUS "__status"
#define SYSVAL_RUNNING "running"

//typedef enum { TYP_NONE, TYP_INT, TYP_FLOAT, TYP_STRING, TYP_BOOL } xsql_type_t;
typedef int32_t xsql_type_t;
#define TYP_NONE 0
#define TYP_INT 1
#define TYP_FLOAT 2
#define TYP_STRING 3
#define TYP_BOOL 4
typedef enum { INST_NOP, INST_ADD, INST_SUB, INST_MUL, INST_DIV, INST_EQ, INST_NEQ, INST_GT, INST_LT, INST_GE, INST_LE, INST_AND, INST_OR, INST_NOT, INST_NEG } xsql_inst_t;

typedef struct xsql_var xsql_var_t;
typedef xsql_var_t* xsql_varptr_t;

typedef int32_t xsql_bool_t;
typedef int64_t xsql_int_t;
typedef double xsql_float_t;
#define XSQL_STR_CHUNK_SIZE 8
typedef str_t xsql_str_t;

typedef void (*xsql_var_free_h) (xsql_var_t*);

typedef union {
    xsql_bool_t b_val;
    xsql_int_t i_val;
    xsql_float_t f_val;
    xsql_str_t *s_val;
} xsql_raw_t;

struct xsql_var {
    str_t *name;
    xsql_type_t typ;
    xsql_raw_t raw;
    xsql_var_free_h on_free;
};
xsql_var_t xsql_var_init (xsql_type_t typ, xsql_raw_t *raw, xsql_var_free_h on_free);
const char *xsql_varstr (xsql_var_t *v);
char *xsql_varstrdup (xsql_var_t *v);
const char *xsql_typstr (xsql_type_t typ);
xsql_type_t xsql_max_type (xsql_type_t x, xsql_type_t y);
void xsql_var_free_str (xsql_var_t *v);

DEFINE_SORTED_ARRAY(xsql_var_array_t, xsql_varptr_t);

typedef xsql_var_t (*xsql_inst_h) (xsql_var_t, xsql_var_t);
typedef xsql_var_t (*xsql_uinst_h) (xsql_var_t);

xsql_var_t xsql_var_int_add_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_sub_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_mul_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_div_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_eq_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_neq_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_gt_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_lt_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_ge_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_le_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_add_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_sub_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_mul_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_div_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_eq_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_neq_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_gt_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_lt_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_ge_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_int_le_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_add_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_sub_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_mul_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_div_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_eq_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_neq_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_gt_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_lt_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_ge_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_le_int (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_add_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_sub_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_mul_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_div_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_eq_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_neq_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_gt_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_lt_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_ge_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_float_le_float (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_string_add_string (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_string_eq_string (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_string_neq_string (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_bool_eq_bool (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_bool_neq_bool (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_bool_gt_bool (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_bool_lt_bool (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_bool_ge_bool (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_bool_le_bool (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_bool_and_bool (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_bool_or_bool (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_true (xsql_var_t x, xsql_var_t y);
xsql_var_t xsql_var_false (xsql_var_t x, xsql_var_t y);

xsql_var_t xsql_var_neg_int (xsql_var_t v);
xsql_var_t xsql_var_neg_float (xsql_var_t v);
xsql_var_t xsql_var_not_bool (xsql_var_t v);

int xsql_get_inst (xsql_type_t left, xsql_inst_t inst, xsql_type_t right, xsql_inst_h *ret_inst, xsql_type_t *ret_typ);
int xsql_get_uinst (xsql_type_t typ, xsql_inst_t inst, xsql_uinst_h *ret_inst, xsql_type_t *ret_typ);

#endif // __XSQL_VAR_H__
