#ifndef __XSQL_FMT_H__
#define __XSQL_FMT_H__

#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <libex/str.h>
#include <libex/list.h>
#include <libex/file.h>
#include <libex/msg.h>
#include "var.h"
#include "db.h"

//#define XSQL_READ O_RDONLY
//#define XSQL_WRITE (O_CREAT | O_TRUNC | O_WRONLY)
#define XSQL_READ 0
#define XSQL_WRITE 1

#define XSQL_SQLOUT 0x0001
#define XSQL_TEMPNAM 0x0002

#define IDFMT_NORMAL 0
#define IDFMT_ODD 1
#define IDFMT_EVEN 2
#define IDFMT_FLDNAM 3
#define IDFMT_HEADER 4
#define IDFMT_FOOTER 5
#define IDFMT_MAX IDFMT_FOOTER

#define BG_NORMAL 0xffffff
#define FG_NORMAL 0x000000
#define BG_ODD 0xEEEEEE
#define FG_ODD 0x000000
#define BG_EVEN 0xFFFFFF
#define FG_EVEN 0x000000
#define BG_FLDNAM 0x1A5785
#define FG_FLDNAM 0xFFFFFF
#define BG_HEADER 0x429de3
#define FG_HEADER 0xFFFFFF
#define BG_NEG 0xE64040
#define FG_NEG 0xFFFFFF

#define FMT_BEGIN 1
#define FMT_BEGIN_DATA 2
#define FMT_RECORD 3
#define FMT_END_DATA 4
#define FMT_NODATA 5
#define FMT_END 6

typedef struct xsql_fmtprov xsql_fmtprov_t;
typedef struct xsql_fmt xsql_fmt_t;

typedef const char* (*xsql_fmt_name_h) ();
typedef str_t* (*xsql_fmt_wrtname_h) (const char*, size_t);
typedef xsql_fmt_t* (*xsql_fmt_open_h) (xsql_fmtprov_t*, const char*, size_t, const char*, size_t, int flags);
typedef int (*xsql_fmt_wrtopen_h) (xsql_fmt_t*);
typedef void (*xsql_fmt_set_bg_h) (xsql_fmt_t*, int, int32_t);
typedef void (*xsql_fmt_set_fg_h) (xsql_fmt_t*, int, int32_t);
typedef void (*xsql_fmt_set_style_h) (xsql_fmt_t*, int);
typedef void (*xsql_fmt_start_outrec_h) (xsql_fmt_t*, xsql_stmt_t*);
typedef void (*xsql_fmt_outrec_h) (xsql_fmt_t*, xsql_stmt_t*);
typedef void (*xsql_fmt_end_outrec_h) (xsql_fmt_t*, xsql_stmt_t*);
typedef void (*xsql_fmt_start_outvar_h) (xsql_fmt_t*);
typedef void (*xsql_fmt_outvar_h) (xsql_fmt_t*, xsql_var_t*, size_t);
typedef int (*xsql_fmt_wrtvar_h) (xsql_fmt_t*, xsql_var_t*, size_t);
typedef int (*xsql_fmt_wrtstr_h) (xsql_fmt_t*, const char*, size_t, size_t);
typedef void (*xsql_fmt_end_outvar_h) (xsql_fmt_t*);
typedef int (*xsql_fmt_wrtend_h) (xsql_fmt_t*);
typedef void (*xsql_fmt_close_h) (xsql_fmt_t*);
typedef void (*xsql_fmt_wrtclose_h) (xsql_fmt_t*);
typedef void (*xsql_fmt_end_h) (xsql_fmt_t*);
typedef void (*xsql_fmt_newtab_h) (xsql_fmt_t*, const char*, size_t);
typedef void (*xsql_fmt_plot_h) (xsql_fmt_t*, const char*, size_t);

typedef struct {
    int typ;
    strptr_t *ptr;
    int len;
} xsql_fmt_read_t;
typedef int (*xsql_fmt_read_h) (xsql_fmt_t *fmt, xsql_fmt_read_t *data);

struct xsql_fmtprov {
    void *hnd;
    xsql_fmt_name_h fmtname;
    xsql_fmt_wrtname_h fmtwrtname;
    xsql_fmt_open_h fmtopen;
    xsql_fmt_wrtopen_h fmtwrtopen;
    xsql_fmt_set_bg_h fmtsetbg;
    xsql_fmt_set_fg_h fmtsetfg;
    xsql_fmt_set_style_h fmtsetstyle;
    xsql_fmt_start_outrec_h fmtstart_outrec;
    xsql_fmt_outrec_h fmtoutrec;
    xsql_fmt_end_outrec_h fmtend_outrec;
    xsql_fmt_start_outvar_h fmtstart_outvar;
    xsql_fmt_outvar_h fmtoutvar;
    xsql_fmt_wrtvar_h fmtwrtvar;
    xsql_fmt_wrtstr_h fmtwrtstr;
    xsql_fmt_end_outvar_h fmtend_outvar;
    xsql_fmt_wrtend_h fmtwrtend;
    xsql_fmt_close_h fmtclose;
    xsql_fmt_wrtclose_h fmtwrtclose;
    xsql_fmt_end_h fmtend;
    xsql_fmt_newtab_h fmtnewtab;
    xsql_fmt_plot_h fmtplot;
    xsql_fmt_read_h fmtread;
};
extern list_t *fmtprovs;
str_t *xsql_fmtinit (const char *conf_str, size_t conf_str_len);
void xsql_fmtdone ();
xsql_fmtprov_t *xsql_find_fmtprov (const char *name, size_t name_len);

xsql_fmtprov_t *xsql_create_text_provider ();
xsql_fmtprov_t *xsql_create_json_provider ();
xsql_fmtprov_t *xsql_create_bin_provider ();

struct xsql_fmt {
    str_t *name;
    str_t *fname;
    xsql_fmtprov_t *prov;
    int flags;
};

extern xsql_fmtprov_t *fmtstd_prov;

typedef struct {
    str_t *name;
    str_t *fname;
    xsql_fmtprov_t *prov;
    int flags;
    int fd;
    list_t *result;
    int *fldlen;
} xsql_text_t;

int file_wrtopen (xsql_fmt_t *fmt);
str_t *file_wrtname (const char *fname, size_t fname_len);
int file_wrtvar (xsql_fmt_t *fmt, xsql_var_t *var, size_t index);
int file_wrtstr (xsql_fmt_t *fmt, const char *str, size_t str_len, size_t index);
int file_wrtend (xsql_fmt_t *fmt);
void file_wrtclose (xsql_fmt_t *fmt);

#endif // __XSQL_FMT_H__
