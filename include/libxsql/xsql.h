#ifndef __XSQL_H__
#define __XSQL_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <libex/list.h>
#include <libex/tree.h>
#include "var.h"
#include "db.h"
#include "fmt.h"
#include "plot.h"

#define XSQL_ERR_BUF_SIZE 1024
#define XSQL_CONF "xsql.rc"
#define XSQL_DESCFILE ".desc"

/*
    Evaluate
*/
typedef struct {
    str_t *key;
    str_t *val;
} keyval_t;

typedef struct xsql_op xsql_op_t;
typedef struct xsql_script xsql_script_t;
typedef struct xsql xsql_t;
typedef struct xsql_ev xsql_ev_t;
typedef struct {
    char *text;
    char *text_ptr;
    str_t *fname;
    char *err;
    int line_no;
} xsql_source_t;

// -= evaluation =-
typedef enum {EV_UINST, EV_INST, EV_VAL} xsql_evtype_t;
typedef struct xsql_ev xsql_ev_t;
typedef xsql_type_t (*xsql_retyp_h) (xsql_ev_t*);

struct xsql_ev {
    xsql_evtype_t typ;
    xsql_uinst_h uinst;
    xsql_inst_h inst;
    xsql_var_t var;
    xsql_var_t *var_ptr;
    xsql_ev_t *left;
    xsql_ev_t *right;
};

xsql_ev_t *xsql_eval_add_var (xsql_type_t typ, xsql_raw_t *raw, xsql_var_free_h on_free);
xsql_ev_t *xsql_eval_add_var_link (xsql_var_t *var);
xsql_ev_t *xsql_eval_add_inst (xsql_inst_t inst, xsql_ev_t *left, xsql_ev_t *right);
xsql_ev_t *xsql_eval_add_uinst (xsql_inst_t inst, xsql_ev_t *right);

xsql_var_t xsql_eval_exec (xsql_ev_t *ev, int is_keep_consts);
xsql_ev_t *xsql_eval_clone (xsql_script_t *scr, xsql_ev_t *ev);
void xsql_eval_free (xsql_ev_t *ev);

typedef struct {
    xsql_var_t agg1;
    xsql_var_t agg2;
    xsql_var_t agg3;
} xsql_agg_t;

// -= script =-
struct xsql_script {
    xsql_t *scr_main;
    str_t *name;
    list_t *var_list;
    list_t *param_list;
    xsql_op_t *op;
};

typedef struct {
    xsql_type_t typ;
    str_t *var_name;
    xsql_var_t *var;
    str_t *prompt;
} xsql_param_t;

struct xsql {
    xsql_t *scr_main;
    str_t *name;
    list_t *var_list;
    list_t *param_list;
    xsql_op_t *op;
    xsql_agg_t *aggs;
    int agg_len;
    int agg_idx;
    int agg_size;
    list_t *conn_list;
    xsql_conn_t *conn;
    xsql_dbprov_t *cur_prov;
    list_t *input_param_list;
    char *err;
    xsql_fmt_t *fmt;
    xsql_fmt_t *fmtstd;
    list_t *files;
};

int xsql_gen_error (xsql_source_t *txt, const char *arg, ...);
int xsql_gen_runtime_error (xsql_t *scr, const char *fmt, ...);
void xsql_add_arg (list_t *arg_list, const char *arg);
void xsql_add_arg_ex (xsql_t *scr, list_t *arg_list, const char *arg, xsql_type_t expected_typ);
int xsql_script_exec (xsql_t *scr, list_t *args);

// -= operation =-
typedef xsql_op_t* (*xsql_op_exec_h) (xsql_op_t*);
typedef xsql_op_t* (*xsql_op_clone_h) (xsql_script_t *dst, xsql_op_t*);
typedef void (*xsql_op_destroy_h) (xsql_op_t*);
xsql_op_t *xsql_op_no_clone (xsql_op_t *x);
struct xsql_op {
    xsql_op_t *next;
    xsql_op_exec_h exec;
    xsql_op_destroy_h destroy;
    xsql_op_clone_h clone;
    xsql_script_t *scr;
};
void xsql_op_exec (xsql_op_t *x);
xsql_op_t *xsql_op_clone (xsql_script_t *dst, xsql_op_t *x);
void xsql_op_free (xsql_op_t *x);

// -= assign variables =-
typedef struct xsql_assign xsql_assign_t;
struct xsql_assign {
    xsql_op_t *next;
    xsql_op_exec_h exec;
    xsql_op_destroy_h destroy;
    xsql_op_clone_h clone;
    xsql_script_t *scr;
    xsql_var_t *var;
    xsql_ev_t *ev;
    xsql_var_t vret;
};
xsql_op_t *xsql_assign_exec (xsql_op_t *x);
void xsql_assign_destroy (xsql_op_t *x);

// -= print =-
typedef struct {
    xsql_op_t *next;
    xsql_op_exec_h exec;
    xsql_op_destroy_h destroy;
    xsql_op_clone_h clone;
    xsql_script_t *scr;
    list_t *ev_list;
} xsql_print_t;
xsql_op_t *xsql_print_exec (xsql_op_t *x);
void xsql_print_destroy (xsql_op_t *x);

// -= if =-
typedef struct {
    xsql_op_t *next;
    xsql_op_exec_h exec;
    xsql_op_destroy_h destroy;
    xsql_op_clone_h clone;
    xsql_script_t *scr;
    xsql_ev_t *ev;
    xsql_op_t *op_true;
    xsql_op_t *op_false;
} xsql_if_t;
xsql_op_t *xsql_if_exec (xsql_op_t *x);
void xsql_if_destroy (xsql_op_t *x);

// -= case =-
typedef struct {
    xsql_ev_t *ev;
    xsql_op_t *body;
} xsql_case_item_t;

typedef struct {
    xsql_op_t *next;
    xsql_op_exec_h exec;
    xsql_op_destroy_h destroy;
    xsql_op_clone_h clone;
    xsql_script_t *scr;
    list_t *items;
} xsql_case_t;
xsql_op_t *xsql_case_exec (xsql_op_t *x);
void xsql_case_destroy (xsql_op_t* x);

// -= for =-
typedef struct {
    xsql_op_t *next;
    xsql_op_exec_h exec;
    xsql_op_destroy_h destroy;
    xsql_op_clone_h clone;
    xsql_script_t *scr;
    xsql_var_t *var_iter;
    xsql_ev_t *ev_init;
    xsql_ev_t *ev_end;
    xsql_ev_t *ev_step;
    xsql_op_t *op_body;
} xsql_for_t;
xsql_op_t *xsql_for_exec (xsql_op_t *x);
void xsql_for_destroy (xsql_op_t *x);

// -= while =-
typedef struct {
    xsql_op_t *next;
    xsql_op_exec_h exec;
    xsql_op_destroy_h destroy;
    xsql_op_clone_h clone;
    xsql_script_t *scr;
    xsql_ev_t *ev;
    xsql_op_t *op_body;
} xsql_while_t;
xsql_op_t *xsql_while_exec (xsql_op_t *x);
void xsql_while_destroy (xsql_op_t* x);

// -= connect =-
typedef struct {
    xsql_op_t *next;
    xsql_op_exec_h exec;
    xsql_op_destroy_h destroy;
    xsql_op_clone_h clone;
    xsql_script_t *scr;
    str_t *conninfo;
    //str_t *v_conninfo;
    xsql_var_t *v_conninfo;
    str_t *provider_name;
    xsql_dbprov_t *provider;
    str_t *name;
    xsql_conn_t *conn;
} xsql_connect_t;
xsql_op_t *xsql_connect_exec (xsql_op_t *x);
void xsql_connect_destroy (xsql_op_t *x);

// -= exec sql statements =-
typedef struct {
    xsql_op_t *next;
    xsql_op_exec_h exec;
    xsql_op_destroy_h destroy;
    xsql_op_clone_h clone;
    xsql_script_t *scr;
    str_t *query;
    list_t *params;
    xsql_stmt_t *stmt;
    int recycling;
} xsql_sqlexec_t;
xsql_op_t *xsql_sqlexec_exec (xsql_op_t *x);
void xsql_sqlexec_destroy (xsql_op_t *x);

// -= store =-
typedef str_t *(*xsql_getfname_h) (const char *dir, size_t dir_len, const char *fname, size_t fname_len);
extern xsql_getfname_h xsql_getfname;
str_t *xsql_getpath (const char *s, size_t l);
typedef struct {
    xsql_op_t *next;
    xsql_op_exec_h exec;
    xsql_op_destroy_h destroy;
    xsql_op_clone_h clone;
    xsql_script_t *scr;
    str_t *name;
    str_t *fname;
    xsql_var_t *v_fname;
    int flags;
    xsql_fmtprov_t *fmtprov;
    xsql_op_t *op_body;
} xsql_store_t;
extern xsql_getfname_h xsql_getfname;
xsql_op_t *xsql_store_exec (xsql_op_t *x);
void xsql_store_destroy (xsql_op_t *x);

// -= load =-
typedef struct {
    xsql_op_t *next;
    xsql_op_exec_h exec;
    xsql_op_destroy_h destroy;
    xsql_op_clone_h clone;
    xsql_script_t *scr;
    str_t *fname;
    xsql_var_t *v_fname;
    list_t *var_list;
    xsql_fmtprov_t *fmtprov;
    xsql_op_t *op_body;
} xsql_load_t;
xsql_op_t *xsql_load_exec (xsql_op_t *x);
void xsql_load_destroy (xsql_op_t *x);

// -= loop =-
typedef struct {
    xsql_op_t *next;
    xsql_op_exec_h exec;
    xsql_op_destroy_h destroy;
    xsql_op_clone_h clone;
    xsql_script_t *scr;
    str_t *query;
    list_t *params;
    xsql_stmt_t *stmt;
    int recycling;
    xsql_op_t *op_body;
    list_t *var_list;
    str_t **svals;
    size_t svals_len;
} xsql_loop_t;
xsql_op_t *xsql_loop_exec (xsql_op_t *x);
void xsql_loop_destroy (xsql_op_t *x);

// -= new tab =-
typedef struct {
    xsql_op_t *next;
    xsql_op_exec_h exec;
    xsql_op_destroy_h destroy;
    xsql_op_clone_h clone;
    xsql_script_t *scr;
    str_t *name;
} xsql_newtab_t;
xsql_op_t *xsql_newtab_exec (xsql_op_t *x);
void xsql_newtab_destroy (xsql_op_t* x);

// -= plot =-
typedef struct {
    xsql_op_t *next;
    xsql_op_exec_h exec;
    xsql_op_destroy_h destroy;
    xsql_op_clone_h clone;
    xsql_script_t *scr;
    str_t *query;
    const char *format;
    const char *style;
    list_t *params;
    xsql_stmt_t *stmt;
    int recycling;
} xsql_plot_t;
xsql_op_t *xsql_plot_exec (xsql_op_t *x);
void xsql_plot_destroy (xsql_op_t *x);

// -= use connection
typedef struct {
    xsql_op_t *next;
    xsql_op_exec_h exec;
    xsql_op_destroy_h destroy;
    xsql_op_clone_h clone;
    xsql_script_t *scr;
    str_t *connection_name;
    xsql_dbprov_t *provider;
    xsql_connect_t *op_conn;
} xsql_use_t;
xsql_op_t *xsql_use_exec (xsql_op_t *x);
void xsql_use_destroy (xsql_op_t *x);

/*
    Parser
*/

typedef struct {
    str_t *label;
    xsql_type_t typ;
} xsql_meta_param_t;

typedef struct {
    str_t *desc;
    list_t *params;
} xsql_meta_t;
extern int report_flags;

int xsql_load_file (const char *fname, xsql_source_t *txt);
void xsql_source_destroy (xsql_source_t *txt);
void xsql_script_destroy (xsql_t *scr);
int xsql_load_script (const char *fname, xsql_source_t *txt);

int xsql_get_token (xsql_source_t *txt, strptr_t *ret);
xsql_var_t *xsql_get_var (xsql_script_t *scr, const char *name, size_t name_len);
xsql_var_t *xsql_add_var (xsql_script_t *scr, const char *name, size_t name_len, xsql_type_t typ);
xsql_ev_t *xsql_parse_eval (xsql_source_t *txt, xsql_script_t *scr, strptr_t *token);
int xsql_parse (xsql_source_t *txt, xsql_t *scr);
xsql_meta_t *xsql_load_meta (const char *fname);
void xsql_free_meta (xsql_meta_t *meta);
int xsql_set_script_params (xsql_script_t *scr, list_t *param_list);

#endif // __XSQL_H__
