-module(xsqlconf).
-export([load/1]).

get_dir(dir_home) ->
    case os:getenv("HOME") of
        false -> undefined;
        Dir -> Dir
    end;
get_dir(dir_home_config) ->
    case get_dir(dir_home) of
        undefined -> undefined;
        Dir -> filename:absname_join(Dir, ".config")
    end;
get_dir(dir_config) -> "/etc";
get_dir(dir_current) ->
    case file:get_cwd() of
        {ok, Dir} -> Dir;
        _ -> undefined
    end;
get_dir(_) -> undefined.

load_conf_spec(DirId, FileName, Prefix, D) ->
    case get_dir(DirId) of
        undefined -> D;
        Dir -> load_conf_spec(filename:absname_join(Dir, [Prefix|FileName]), D)
    end.
load_conf_spec(FileName, D) ->
    case file:open(FileName, [read]) of
        {ok, Fd} -> load_conf_file(Fd, D);
        _ -> D
    end.

trim(S) -> re:replace(S, "\\s+", "", [global,{return,list}]).

load_conf_file(Fd, D) ->
    case file:read_line(Fd) of
        {ok, Line} -> load_conf_file(Fd, Line, D);
        _ -> file:close(Fd), D
    end.
load_conf_file(Fd, Line, D) ->
    case list_to_tuple(re:split(Line, "=", [{return, list}])) of
        {Key, Value} ->
            load_conf_file(Fd, dict:store(trim(Key), trim(Value), D));
        _ -> load_conf_file(Fd, D)
    end.

load(FileName) ->
    load([{dir_config,[]}, {dir_home_config,[]}, {dir_home,"."}, {dir_current,"."}], FileName, dict:new()).

load([{DirId,Prefix}|T], FileName, D) ->
    load(T, FileName, load_conf_spec(DirId, FileName, Prefix, D));
load([], _, D) -> D.
