-module(xsql).
-export([parse_json/1, parse/1, to_html/1, test/0, mkid/2, ls/1]).

-record(content, {
    status=fail,
    data
}).

-record(dir, {
    name,
    content
}).
-record(file, {
    name,
    params=[],
    path
}).
-record(param, {
    label,
    type
}).

parse(Data) ->
    {Content} = Data,
    parse(Content, #content{}).

parse_json(Json) ->
    parse(jiffy:decode(Json)).


parse([{<<"status">>,0}|T], R) ->
    parse(T, R#content{status=ok});
parse([{<<"data">>,Data}|T], R) ->
    parse(T, R#content{data=Data});
parse([_H|T], R) ->
    parse(T, R);
parse([], R) ->
    parse_content(R#content.data, []).

parse_content([{Item}|T], R) ->
    case lists:keyfind(<<"is_dir">>, 1, Item) of
        {_, true} -> parse_content(T, [parse_dir(Item, #dir{})|R]);
        {_, false} -> parse_content(T, [parse_file(Item, #file{})|R]);
        _ -> parse_content(T, R)
    end;
parse_content([_H|T], D) -> parse_content(T, D);
parse_content([], R) -> lists:sort(R).

parse_dir([{<<"name">>,Name}|T], #dir{}=R) ->
    parse_dir(T, R#dir{name=binary_to_list(Name)});
parse_dir([{<<"content">>,Content}|T], #dir{}=R) ->
    parse_dir(T, R#dir{content=parse_content(Content, [])});
parse_dir([_|T], R) -> parse_dir(T, R);
parse_dir([], R) -> R.

parse_file([{<<"name">>,Name}|T], #file{}=R) ->
    parse_file(T, R#file{name=binary_to_list(Name)});
parse_file([{<<"path">>,Path}|T], #file{}=R) ->
    parse_file(T, R#file{path=binary_to_list(Path)});
parse_file([{<<"params">>,Params}|T], #file{}=R) ->
    parse_file(T, R#file{params=parse_params(Params, [])});
parse_file([_|T], R) -> parse_file(T, R);
parse_file([], R) -> R.

parse_params([{Param}|T], R) ->
    parse_params(T, [parse_any_param(Param, #param{})|R]);
parse_params([], R) -> lists:reverse(R).

parse_any_param([{<<"label">>,Label}|T], #param{}=R) ->
    parse_any_param(T, R#param{label=binary_to_list(Label)});
parse_any_param([{<<"type">>,Type}|T], #param{}=R) ->
    parse_any_param(T, R#param{type=binary_to_list(Type)});
parse_any_param([_|T], R) ->parse_any_param(T, R);
parse_any_param([], R) -> R.

test() -> ls("/").

err(Status) ->
    [{status, Status}, {header, {connection, "close"}}].

mkurl(Host, Port, Url) ->
    lists:concat([Host, ":", Port, Url]).

ls(Url) ->
    case get("config") of
        undefined ->
            Conf = xsqlconf:load("xsql.rc"),
            put("config", Conf),
            inets:start(),
            ls(Url, Conf);
        Conf ->
            ls(Url, Conf)
    end.
ls(Url, Conf) ->
    Port = dict:find("port", Conf),
    Host = dict:find("host", Conf),
    case (Port /= error) and (Host /= error) of
        true -> ls(Url, Conf, Host, Port);
        false -> err(502)
    end.
ls(Url, _Conf, {ok,Host}, {ok,Port}) ->
    RequestUrl = mkurl(Host, Port, Url),
    case httpc:request(RequestUrl) of
        {ok, {{_Ver, 200, _Phrase}, _Hdrs, Body}} ->
            to_html(Body);
        {error,Reason} ->
            err(502)
    end.

merge([H|T], R) -> merge(T, [H|R]);
merge([], R) -> R.

mrg(N, R) -> mrg(merge(integer_to_list(N), R)).
mrg(R) -> [$-|R].

mkid(N, I) -> mkid(N, I, []).
mkid(_N, 0, R) -> "item" ++ R;
mkid(N, I, R) -> mkid(N, I-1, mrg(N, R)).

to_html(Content) ->
    case parse_json(Content) of
        {fail, _} -> {ehtml, "error"};
        Data -> {ehtml, [{'div', [{class,"css-treeview"}], [to_html(Data, [], 0, 1)]}]}
    end.
to_html([#file{}=X|T], R, N, I) ->
    to_html(T, [{li, [], {a, [{href, X#file.path}], X#file.name}}|R], N+1, I+1);
to_html([#dir{}=X|T], R, N, I) ->
    Id = mkid(N, I),
    to_html(T, [{ul, [], {li, [], [{input, [{type,"checkbox"},{id,Id}]}, {label, [{for,Id}], X#dir.name}, {ul, [], to_html(X#dir.content, [], N, I+1)}]}}|R], N+1, I);
to_html([], R, _N, _I) -> lists:reverse(R).
