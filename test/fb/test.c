#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <ibase.h>

#define MAXLEN 1024
#define ISC_INT64_FORMAT "ll"
typedef PARAMVARY VARY2;

//#ifdef SQL_VERSION_V6
//    #undef SQL_VERSION_V6
//    #define SQL_VERSION_V6 1
//#endif

ISC_STATUS_ARRAY status;
isc_db_handle hdb = 0;
isc_tr_handle htr = 0;
isc_stmt_handle hstmt = 0;
XSQLDA *out_sqlda = NULL, *in_sqlda = NULL;
char **out_data = NULL;
short *out_ind = NULL;
char **in_data = NULL;
short *in_ind = NULL;


#define USER "sysdba"
#define PASS "masterkey"

int db_open () {
    ISC_STATUS ret;
    char *dpb = malloc(1 + strlen(USER) + 2 + strlen(PASS) + 2), *p = dpb;
    short dpb_len = 1;
    *dpb = isc_dpb_version1;
    isc_modify_dpb(&dpb, &dpb_len, isc_dpb_user_name, USER, strlen(USER));
    isc_modify_dpb(&dpb, &dpb_len, isc_dpb_password, PASS, strlen(PASS));
    if (0 != (ret = isc_attach_database(status, 0, "/home/shura/proj/firebird/employee.fdb", &hdb, dpb_len, dpb)))
        isc_print_status(status);
    free(p);
    return ret;
}

void db_close () {
    isc_detach_database(status, &hdb);
}

void set_sqlda_out () {
    out_data = calloc(out_sqlda->sqld, sizeof(char*));
    out_ind = calloc(out_sqlda->sqld, sizeof(short));
    for (int i = 0; i < out_sqlda->sqld; ++i) {
        out_data[i] = malloc(sizeof(short) + out_sqlda->sqlvar[i].sqllen + 1);
        out_sqlda->sqlvar[i].sqldata = (char*)out_data[i];
        out_sqlda->sqlvar[i].sqlind = &out_ind[i];
    }
}

void set_sqlda_in () {
    in_data = calloc(in_sqlda->sqld, sizeof(char*));
    in_ind = calloc(in_sqlda->sqld, sizeof(short));
    for (int i = 0; i < in_sqlda->sqld; ++i) {
        in_data[i] = "1000000";
        in_sqlda->sqlvar[i].sqldata = (char*)in_data[i];
        in_sqlda->sqlvar[i].sqltype = SQL_TEXT;
        in_sqlda->sqlvar[i].sqllen = strlen(in_data[i]);
        in_sqlda->sqlvar[i].sqlind = &in_ind[i];
        in_ind[i] = 0;
    }
}

void free_sqlda () {
    for (int i = 0; i < out_sqlda->sqld; ++i)
        free(out_data[i]);
    free(out_data); out_data = NULL;
    free(out_ind); out_ind = NULL;
    free(out_sqlda);
    out_sqlda = NULL;
    if (in_data) free(in_data);
    if (in_ind) free(in_ind);
    free(in_sqlda);
}

int execute () {
    int ret;
    if (0 == (ret = isc_dsql_allocate_statement(status, &hdb, &hstmt)) &&
        0 == (ret = isc_start_transaction(status, &htr, 1, &hdb, 0, NULL)) &&
        0 == (ret = isc_dsql_prepare(status, &htr, &hstmt, 0, "select department, budget from department where budget > ?", SQL_DIALECT_V6, out_sqlda))) {
        ret = isc_dsql_describe_bind(status, &hstmt, SQL_DIALECT_V6, in_sqlda);
        if (in_sqlda->sqld > 0) {
            if (in_sqlda->sqld > in_sqlda->sqln)
                in_sqlda = realloc(in_sqlda, XSQLDA_LENGTH(in_sqlda->sqld));
            set_sqlda_in();
        }
        if (0 == out_sqlda->sqld)
            ret = isc_dsql_execute(status, &htr, &hstmt, SQL_DIALECT_V6, NULL);
        else {
            if (out_sqlda->sqld > out_sqlda->sqln)
                out_sqlda = realloc(out_sqlda, XSQLDA_LENGTH(out_sqlda->sqld));
            set_sqlda_out();
            if (0 == in_sqlda->sqld)
                ret = isc_dsql_execute(status, &htr, &hstmt, SQL_DIALECT_V6, out_sqlda);
            else
                ret = isc_dsql_execute2(status, &htr, &hstmt, SQL_DIALECT_V6, in_sqlda, NULL);
        }
    }
    if (0 != ret)
        isc_print_status(status);
    return ret;
}

void print_column (XSQLVAR *var)
{
    short       dtype;
    char        data[MAXLEN], *p;
    char        blob_s[20], date_s[25];
    VARY2        *vary2;
    short       len; 
    struct tm   times;
    ISC_QUAD    bid;

    dtype = var->sqltype & ~1;
    p = data;

    // Null handling.  If the column is nullable and null
    if ((var->sqltype & 1) && (*var->sqlind < 0))
    {
        switch (dtype) {
            case SQL_TEXT:
            case SQL_VARYING:
                len = var->sqllen;
                break;
            case SQL_SHORT:
                len = 6;
		if (var->sqlscale > 0) len += var->sqlscale;
                break;
            case SQL_LONG:
                len = 11;
		if (var->sqlscale > 0) len += var->sqlscale;
                break;
	    case SQL_INT64:
		len = 21;
		if (var->sqlscale > 0) len += var->sqlscale;
		break;
            case SQL_FLOAT:
                len = 15;
                break;
            case SQL_DOUBLE:
                len = 24;
                break;
	    case SQL_TIMESTAMP:
		len = 24;
		break;
	    case SQL_TYPE_DATE:
		len = 10;
		break;
	    case SQL_TYPE_TIME:
		len = 13;
		break;
            case SQL_BLOB:
            case SQL_ARRAY:
            default:
                len = 17;
                break;
        }
        if ((dtype == SQL_TEXT) || (dtype == SQL_VARYING))
            sprintf(p, "%-*s ", len, "NULL");
        else
            sprintf(p, "%*s ", len, "NULL");
    }
    else
    {
        switch (dtype)
        {
            case SQL_TEXT:
                sprintf(p, "%.*s ", var->sqllen, var->sqldata);
                break;

            case SQL_VARYING:
                vary2 = (VARY2*) var->sqldata;
                vary2->vary_string[vary2->vary_length] = '\0';
                sprintf(p, "%-*s ", var->sqllen, vary2->vary_string);
                break;

            case SQL_SHORT:
            case SQL_LONG:
	    case SQL_INT64:
		{
		ISC_INT64	value;
		short		field_width;
		short		dscale;
		switch (dtype)
		    {
		    case SQL_SHORT:
			value = (ISC_INT64) *(short *) var->sqldata;
			field_width = 6;
			break;
		    case SQL_LONG:
			value = (ISC_INT64) *(int *) var->sqldata;
			field_width = 11;
			break;
		    case SQL_INT64:
			value = (ISC_INT64) *(ISC_INT64 *) var->sqldata;
			field_width = 21;
			break;
		    }
		dscale = var->sqlscale;
		if (dscale < 0)
		    {
		    ISC_INT64	tens;
		    short	i;

		    tens = 1;
		    for (i = 0; i > dscale; i--)
			tens *= 10;

		    if (value >= 0)
			sprintf (p, "%*" ISC_INT64_FORMAT "d.%0*" ISC_INT64_FORMAT "d",
				field_width - 1 + dscale, 
				(ISC_INT64) value / tens,
				-dscale, 
				(ISC_INT64) value % tens);
		    else if ((value / tens) != 0)
			sprintf (p, "%*" ISC_INT64_FORMAT "d.%0*" ISC_INT64_FORMAT "d",
				field_width - 1 + dscale, 
				(ISC_INT64) (value / tens),
				-dscale, 
				(ISC_INT64) -(value % tens));
		    else
			sprintf (p, "%*s.%0*" ISC_INT64_FORMAT "d",
				field_width - 1 + dscale, 
				"-0",
				-dscale, 
				(ISC_INT64) -(value % tens));
		    }
		else if (dscale)
		    sprintf (p, "%*" ISC_INT64_FORMAT "d%0*d", 
			    field_width, 
			    (ISC_INT64) value,
			    dscale, 0);
		else
		    sprintf (p, "%*" ISC_INT64_FORMAT "d%",
			    field_width, 
			    (ISC_INT64) value);
		}
                break;

            case SQL_FLOAT:
                sprintf(p, "%15g ", *(float *) (var->sqldata));
                break;

            case SQL_DOUBLE:
		sprintf(p, "%24f ", *(double *) (var->sqldata));
                break;

	    case SQL_TIMESTAMP:
		isc_decode_timestamp((ISC_TIMESTAMP *)var->sqldata, &times);
		sprintf(date_s, "%04d-%02d-%02d %02d:%02d:%02d.%04d",
				times.tm_year + 1900,
				times.tm_mon+1,
				times.tm_mday,
				times.tm_hour,
				times.tm_min,
				times.tm_sec,
				((ISC_TIMESTAMP *)var->sqldata)->timestamp_time % 10000);
		sprintf(p, "%*s ", 24, date_s);
		break;
	    case SQL_TYPE_DATE:
		isc_decode_sql_date((ISC_DATE *)var->sqldata, &times);
		sprintf(date_s, "%04d-%02d-%02d",
				times.tm_year + 1900,
				times.tm_mon+1,
				times.tm_mday);
		sprintf(p, "%*s ", 10, date_s);
		break;

	    case SQL_TYPE_TIME:
		isc_decode_sql_time((ISC_TIME *)var->sqldata, &times);
		sprintf(date_s, "%02d:%02d:%02d.%04d",
				times.tm_hour,
				times.tm_min,
				times.tm_sec,
				(*((ISC_TIME *)var->sqldata)) % 10000);
		sprintf(p, "%*s ", 13, date_s);
		break;

            case SQL_BLOB:
            case SQL_ARRAY:
                //Print the blob id on blobs or arrays
                bid = *(ISC_QUAD *) var->sqldata;
                sprintf(blob_s, "%08x:%08x", bid.gds_quad_high, bid.gds_quad_low);
                sprintf(p, "%17s ", blob_s);
                break;

            default:
                break;
        }
    }

    printf("%s", p);
}

void print_data () {
    int fetch_stat;
    short nflds = out_sqlda->sqld;
    while (0 == (fetch_stat = isc_dsql_fetch(status, &hstmt, SQL_DIALECT_V6, out_sqlda))) {
        for (int i = 0; i < nflds; ++i)
            print_column((XSQLVAR*)&out_sqlda->sqlvar[i]);
        printf("\n");
    }
    isc_print_status(status);
}

void fin () {
    isc_commit_transaction(status, &htr);
    isc_dsql_free_statement(status, &hstmt, DSQL_close);
}

int main () {
    if (0 == db_open()) {
        in_sqlda = malloc(XSQLDA_LENGTH(20));
        in_sqlda->sqln = 20;
        in_sqlda->version = 1;
        out_sqlda = malloc(XSQLDA_LENGTH(20));
        out_sqlda->sqln = 20;
        out_sqlda->version = 1;
        if (0 == execute()) {
            print_data();
            fin();
        }
        free_sqlda();
        db_close();
    }
    return 0;
}
