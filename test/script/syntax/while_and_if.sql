.name "while & if";
x = 1;
y = 10;
while x <= y do
    print "x=", x;
    if x == y then
        print "fin";
    else
        print "next";
    end;
    x = x + 1;
end;
