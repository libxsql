.name "store";
store name "tabs" format "excel" to "./_test.xlsx" do
    print "first tab";
    print 1;
    newtab "second";
    print "second tab";
    print 2;
    newtab "third";
    print "third tab";
    print 3;
end;
