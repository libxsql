.name "evals";
x = "aaa" + "bbb";
x = x + "ccc";
y = x + x;
z = x + y;
print "x:", x;
print "t:", y;

x = "";
for i = 0; i < 5; i+1 do
    print i;
    x = x + "1";
end;
print "x:", x;

x = null;
print "x:", x;
if x == null then
    print "x is null";
end;

x = 1;
print "x=", x;
if x == null then
    print "x is null";
else
    print "x is not null";
end;

s="\u0414\u0435\u043d\u0435\u0433 \u043d\u0435\u0442, \u043d\u043e \u0432\u044b \u0434\u0435\u0440\u0436\u0438\u0442\u0435\u0441\u044c, \u0438 \u0445\u043e\u0440\u043e\u0448\u0435\u0433\u043e \u0432\u0430\u043c \u043d\u0430\u0441\u0442\u0440\u043e\u0435\u043d\u0438\u044f";
print s;

x = 1;
y = 10;
while x <= y do
    print "x=", x;
    if x == y then
        print "fin";
    else
        print "next";
    end;
    x = x + 1;
end;

x = 2;
print "x:", x, " ok";
case
    when x == 1 then
        print "first";
    when x == 2 then
        print "second";
    when x == 3 then
        print "third";
end;

x = 2.1 * 6.3 - (12 + 6.54) * (5.2 / (8.0 -12));
print "x:", x;

x = "qwerty";
for i = 0; i < 8; i+1 do
    x = x + "123456";
    print x;
end;

