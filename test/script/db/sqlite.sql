.name "sqlite";
connect "test/chinook/Chinook_Sqlite.sqlite" provider "sqlite";

.input char "Artist Name";

ArtistName = $1;
print "Artist: ", ArtistName;

store name "Artist" format "excel" to "test/result/artist.xlsx" do
    print "Albums";
    select al.[Title] from [Artist] ar, [Album] al where ar.[ArtistId] = al.[ArtistId] and ar.[Name] = :ArtistName;
    print "";
    print "Tracks";
    select al.[Title], tr.[Name], tr.[Milliseconds], mt.[Name]
    from [Artist] ar, [Album] al, [Track] tr, [MediaType] mt
    where
        ar.[ArtistId] = al.[ArtistId] and
        al.[AlbumId] = tr.[AlbumId] and
        tr.[MediaTypeId] = mt.[MediaTypeId] and
        ar.[Name] = :ArtistName;
end;
