.name "firebird";
connect "dbname=127.0.0.1:/home/shura/proj/libxsql/test/chinook/Chinook_Firebird.fb;user=sysdba;pass=masterkey" provider "firebird";

.input char "Artist Name";

ArtistName = $1;
print "Artist: ", ArtistName;

store name "Artist" format "excel" to "test/result/artist.xlsx" do
    print "Albums";
    select al."Title" from "Artist" ar, "Album" al where ar."ArtistId" = al."ArtistId" and ar."Name" = :ArtistName;
    print "";
    print "Tracks";
    select al."Title", tr."Name", tr."Milliseconds", mt."Name"
    from "Artist" ar, "Album" al, "Track" tr, "MediaType" mt
    where
        ar."ArtistId" = al."ArtistId" and
        al."AlbumId" = tr."AlbumId" and
        tr."MediaTypeId" = mt."MediaTypeId" and
        ar."Name" = :ArtistName;
end;
