.name "monetdb";
connect "host=localhost:11211;dbname=chinook;user=monetdb;pass=monetdb" provider "monetdb";

.input char "Artist Name";

ArtistName = $1;
print "Artist: ", ArtistName;

store name "Artist" format "excel" to "test/result/artist.xlsx" do
    print "Albums";
    select al.title from artist ar, album al where ar.artistId = al.artistId and ar.name = :ArtistName;
    print "";
    print "Tracks";
    select al.title, tr.name, tr.milliseconds, mt.name
    from artist ar, album al, track tr, mediatype mt
    where
        ar.artistid = al.artistid and
        al.albumid = tr.albumid and
        tr.mediaTypeid = mt.mediaTypeid and
        ar.name = :ArtistName;
end;
