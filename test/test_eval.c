#include <stdlib.h>
#include "../include/libxsql/xsql.h"

// 2 + 3 * 4 + (6 * 5) / 7
void test000 () {
    xsql_raw_t raw;

    raw.i_val = 2;
    xsql_ev_t *ev_2 = xsql_eval_add_var(TYP_INT, &raw, NULL);
    raw.i_val = 3;
    xsql_ev_t *ev_3 = xsql_eval_add_var(TYP_INT, &raw, NULL);
    raw.i_val = 4;
    xsql_ev_t *ev_4 = xsql_eval_add_var(TYP_INT, &raw, NULL);
    raw.i_val = 5;
    xsql_ev_t *ev_5 = xsql_eval_add_var(TYP_INT, &raw, NULL);
    raw.f_val = 6;
    xsql_ev_t *ev_6 = xsql_eval_add_var(TYP_FLOAT, &raw, NULL);
    raw.i_val = 7;
    xsql_ev_t *ev_7 = xsql_eval_add_var(TYP_INT, &raw, NULL);
    
    xsql_ev_t *ev_mul_1 = xsql_eval_add_inst(INST_MUL, ev_3, ev_4);
    xsql_ev_t *ev_add_1 = xsql_eval_add_inst(INST_ADD, ev_2, ev_mul_1);
    xsql_ev_t *ev_mul_2 = xsql_eval_add_inst(INST_MUL, ev_6, ev_5);
    xsql_ev_t *ev_div_1 = xsql_eval_add_inst(INST_DIV, ev_mul_2, ev_7);
    xsql_ev_t *ev_add_2 = xsql_eval_add_inst(INST_ADD, ev_add_1, ev_div_1);
    
    xsql_var_t var = xsql_eval_exec(ev_add_2, 0);
    xsql_eval_free(ev_add_2);
    char typ [32], val [32];
    strcpy(typ, xsql_typstr(var.typ));
    strcpy(val, xsql_varstr(&var));
    printf("%s: %s\n", typ, val);
}

// "Good" + " " + "luck" + " people"
void test001 () {
    xsql_raw_t raw;
    
    raw.s_val = mkstr(CONST_STR_LEN("Good"), 16);
    xsql_ev_t *ev_1 = xsql_eval_add_var(TYP_STRING, &raw, xsql_var_free_str);
    raw.s_val = mkstr(CONST_STR_LEN(" "), 16);
    xsql_ev_t *ev_2 = xsql_eval_add_var(TYP_STRING, &raw, xsql_var_free_str);
    raw.s_val = mkstr(CONST_STR_LEN("luck,"), 16);
    xsql_ev_t *ev_3 = xsql_eval_add_var(TYP_STRING, &raw, xsql_var_free_str);
    raw.s_val = mkstr(CONST_STR_LEN(" people"), 16);
    xsql_ev_t *ev_4 = xsql_eval_add_var(TYP_STRING, &raw, xsql_var_free_str);
    
    xsql_ev_t *ev_a1 = xsql_eval_add_inst(INST_ADD, ev_3, ev_4);
    xsql_ev_t *ev_a2 = xsql_eval_add_inst(INST_ADD, ev_2, ev_a1);
    xsql_ev_t *ev_a3 = xsql_eval_add_inst(INST_ADD, ev_1, ev_a2);
    
    xsql_var_t var = xsql_eval_exec(ev_a3, 0);
    xsql_eval_free(ev_a3);
    char typ [32], val [32];
    strcpy(typ, xsql_typstr(var.typ));
    strcpy(val, xsql_varstr(&var));
    printf("%s: %s\n", typ, val);
    if (var.on_free) var.on_free(&var);
}

int main () {
    test001();
    return 0;
}
