#!/bin/sh

valgrind --log-file=$1.log --leak-check=full --show-reachable=yes --track-origins=yes --suppressions=./.xsql.supp $1 $2 $3 $4 $5 $6 $7
